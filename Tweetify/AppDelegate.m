//
//  AppDelegate.m
//  Tweetify
//
//  Created by smr on 20.02.13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import "AppDelegate.h"
#import "DCIntrospect.h"
#import "MainViewController.h"
#import <Crashlytics/Crashlytics.h>
#import "iRate.h"
#import "iVersion.h"
#import "Crackify.h"
#import "BlockAlertView.h"

#if LITE_VERSION
#import "AdViewController.h"
#endif

@implementation AppDelegate

@synthesize mainNavi;
@synthesize splashView;

+ (void)initialize
{
	[iRate sharedInstance].onlyPromptIfLatestVersion = NO;
    [iRate sharedInstance].daysUntilPrompt = 10;
    [iRate sharedInstance].usesUntilPrompt = 20;
    [iRate sharedInstance].appStoreID = 632432082;
    
    [iVersion sharedInstance].appStoreID = 632432082;
    [iVersion sharedInstance].showOnFirstLaunch = NO;
    [iVersion sharedInstance].groupNotesByVersion = YES;
}

- (void)dealloc
{
    self.window = nil;
}

- (void)showSplashScreen
{
    if ([self.splashView isDescendantOfView:self.window])
        return;
    
    self.splashView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.splashView.backgroundColor = [UIColor blackColor];
    
	[self.window addSubview:self.splashView];
    [self.window bringSubviewToFront:self.splashView];
}


- (void)removeSplashScreen
{
    [self.splashView removeFromSuperview];
    self.splashView = nil;
    
    if ([Crackify isCracked])
    {
        NSNumber *launchCount = [[NSUserDefaults standardUserDefaults] objectForKey:@"tweetify.app.launch.count"];
        if (launchCount)
            launchCount = @(launchCount.integerValue + 1);
        else
            [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"tweetify.app.launch.count"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        if (launchCount.integerValue >= 5)
        {
            //Crack alert
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Cracked app", @"Cracked app")
                                                            message:NSLocalizedString(@"App is cracked", @"App is cracked")
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"Buy app", @"Buy app")
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }
}

- (void)splashScreenAddActivity
{
    UIImageView *bootLogo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"boot_bird.png"]];
    bootLogo.center = self.splashView.center;
    [self.splashView addSubview:bootLogo];
    
    UIActivityIndicatorView *act = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(CGRectGetMidX([[UIScreen mainScreen] bounds]) - 10,
                                                                                             CGRectGetMidY([[UIScreen mainScreen] bounds]),
                                                                                             20, 20)];
	[act startAnimating];
	[act setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhiteLarge];
    act.alpha = 0.0f;
	[self.splashView addSubview:act];
    
    [UIView animateWithDuration:1.5f animations:^{
        bootLogo.center = CGPointMake(bootLogo.center.x, bootLogo.center.y - 50);
        act.alpha = 1.0f;
    } completion:^(BOOL finished) { }];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Flurry startSession:@"YGH6CKTRS2Z5GMN89CK2"];
    [Crashlytics startWithAPIKey:@"29f780d2dde0eea948e9b1b2693354ef68738d85"];
    
#if LITE_VERSION
    [AdBannerController sharedInstance].adMobId = @"a151fa1b1ceeaeb";
	[AdBannerController sharedInstance].shouldDisplayIAds = NO;
	[AdBannerController sharedInstance].shouldDisplayAdMobAds = YES;
    
#endif
    if (isIOS7orHigher()) {
       [[UIApplication sharedApplication] setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum]; 
    }
    
    if ([DataProxy sharedDataProxy].currentTheme == AppThemeTypeBlack)
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    else
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"black.png"]
                                       forBarMetrics:UIBarMetricsDefault];
    [[UIToolbar appearance] setBackgroundImage:[UIImage imageNamed:@"black.png"] forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    
    MainViewController *main = [[MainViewController alloc] initWithNibName:@"MainViewController" bundle:nil];
    
    self.mainNavi = [[UINavigationController alloc] initWithRootViewController:main];
    [self.mainNavi setNavigationBarHidden:YES];
    self.mainNavi.navigationBar.translucent = NO;

    self.window.rootViewController = self.mainNavi;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *result = [userDefaults objectForKey:@"settings"];
    
    if (![result length])
    {
        [userDefaults setObject:@"SETUP" forKey:@"settings"];
        [userDefaults setObject:@"" forKey:@"mr.user.acc.name"];
        [userDefaults synchronize];
    }
    
    [self.window makeKeyAndVisible];
    
#if TARGET_IPHONE_SIMULATOR
    [[DCIntrospect sharedIntrospector] start];
#endif
    
    [self showSplashScreen];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"tweetify.tutorial.isDisplayed"])
        [self splashScreenAddActivity];
    
    return YES;
}

-(void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    completionHandler(UIBackgroundFetchResultNewData);
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - UIAlertView delegate methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [[UIApplication sharedApplication] openURL:[[NSURL alloc] initWithString:@"http://itunes.apple.com/app/id632432082"]];
}

@end
