//
//  BaseViewController.h
//  Tweetify
//
//  Created by Yuriy Berdnikov on 3/14/13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import <Accounts/Accounts.h>
#import <Twitter/Twitter.h>
#import "TweetComposerView.h"
#import "FDTakeController.h"
#import "FGController.h"
#import "TweetViewCell.h"
#import "TweetInfo.h"
#import "YRDropdownView.h"
#import "JSCoreTextView.h"
#import "ThemeAppearance.h"

#if LITE_VERSION
#import "AdViewController.h"

@interface BaseViewController : AdViewController <TweetViewCellDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

#else

@interface BaseViewController : UIViewController <TweetViewCellDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

#endif

@property (nonatomic, retain) ACAccount* account;

@property (nonatomic, strong) FGController *fgController;

@property (retain, nonatomic) FDTakeController *takeController;
@property (strong, nonatomic) TweetComposerView *tweetComposerView;
@property (nonatomic, assign) NSUInteger tweetImageURLStringLength;
@property (strong, nonatomic) UIImage *tweetImageToUpload;
@property (nonatomic, strong) NSMutableSet *suggestionsDatabase;

- (void)replyToTweet:(TweetInfo *)tweetInfo;

- (void)retweet:(TweetInfo *)tweetInfo tweetType:(TweetType)tweetType response:(NSDictionary *)response;
- (void)unretweet:(TweetInfo *)tweetInfo tweetType:(TweetType)tweetType response:(NSDictionary *)response;

- (void)openTweetComposerViewForReplyID:(NSString *)replyID;
- (void)hideTweetComposerView:(BOOL)isCanceled;

- (void)gotoProfile:(NSString *)profileName;
- (void)updateTweetLengthLabel;

- (NSString *)mrGetTimeStrFromDate:(NSString *)date;

- (BOOL)isResponceContainsError:(NSError *)error;
- (BOOL)isResponceValid:(id)responce error:(NSError *)error;

- (UITableView *)tableViewForDatasource:(NSMutableArray *)datasource;
- (NSMutableArray *)datasourceForTweetType:(TweetType)tweetType;

- (void)animateTweetButton:(BOOL)animate;

@end
