//
//  BaseViewController.m
//  Tweetify
//
//  Created by Yuriy Berdnikov on 3/14/13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import "BaseViewController.h"
#import "ProfileViewController.h"
#import "ConversationViewController.h"
#import "TSMiniWebBrowser.h"
#import "JSONDataParser.h"
#import "BlockActionSheet.h"
#import "ThemeAppearance.h"

@interface BaseViewController () <UITextViewDelegate, TweetComposerViewDelegate, FDTakeDelegate>

@property (nonatomic, assign) NSRange lastRange;

@end

@implementation BaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        self.fgController = [[FGController alloc] initWithDelegate:nil];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationController.navigationBar.hidden = YES;
    
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"TweetComposerView" owner:self options:nil];
    // assuming the view is the only top-level object in the nib file (besides File's Owner and First Responder)
    UIView *nibView = [nibObjects objectAtIndex:0];
    self.tweetComposerView = (TweetComposerView *)nibView;
    self.tweetComposerView.delegate = self;
    
    self.tweetComposerView.tweetComposerTextView.delegate = self;
    
    self.takeController = [[FDTakeController alloc] init];
    self.takeController.viewControllerForPresenting = self;
    self.takeController.delegate = self;
    
    self.lastRange = (NSRange){NSNotFound, 0};
    self.suggestionsDatabase = [[NSMutableSet alloc] init];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)gotoProfile:(NSString *)profileName
{
    if ([self.account.username isEqualToString:profileName])
        return;

    ProfileViewController *profile = [[ProfileViewController alloc] init];
    profile.account = self.account;
    profile.currentUserName = profileName;
    [self.navigationController pushViewController:profile animated:YES];
}

#pragma mark - UITextField delegate methods

- (void)textViewDidChange:(UITextView *)textView
{
    [self updateTweetLengthLabel];
    
    if (!textView.text.length)
        return;
    
    __block NSArray *filteredSuggestions = nil;
    self.lastRange = (NSRange){NSNotFound, 0};
    [textView.text enumerateSubstringsInRange:(NSRange){0, textView.text.length} options:NSStringEnumerationByWords usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
        if (NSLocationInRange(MAX([textView selectedRange].location - 1, 0), substringRange) &&
            [[textView.text substringWithRange:(NSRange){MAX(0, substringRange.location - 1), substringRange.length + 1}] hasPrefix:@"@"])
        {
            
            filteredSuggestions = [self.suggestionsDatabase.allObjects filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"self beginswith[cd] %@", substring]];
            self.lastRange = substringRange;
            *stop = YES;
        }
    }];
    
    self.tweetComposerView.suggestions = filteredSuggestions;
    
    NSRange selectedRange = textView.selectedRange;
    NSMutableAttributedString *lString = [[NSMutableAttributedString alloc] initWithString:textView.text];
    
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue" size:17.0f];
    [lString addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, textView.text.length)];
    
    UIColor *textColor = ([DataProxy sharedDataProxy].currentTheme == AppThemeTypeBlack) ? DARK_TWEET_COMPOSER_TWEET_TEXT_COLOR : LIGHT_TWEET_COMPOSER_TWEET_TEXT_COLOR;
    
    [lString addAttributes:@{NSForegroundColorAttributeName : textColor} range:NSMakeRange(0, textView.text.length)];
    
    if ((textView.text.length + self.tweetComposerView.twitterImageURLLength) > TWEET_MAX_LENGTH)
    {
        NSRange range = NSMakeRange((TWEET_MAX_LENGTH - self.tweetComposerView.twitterImageURLLength),
                                    (textView.text.length + self.tweetComposerView.twitterImageURLLength) - TWEET_MAX_LENGTH);
        
        [lString addAttributes:@{NSForegroundColorAttributeName : ([DataProxy sharedDataProxy].currentTheme == AppThemeTypeBlack) ? DARK_TWEET_COMPOSER_TWEET_EXTRA_TEXT_COLOUR : LIGHT_TWEET_COMPOSER_TWEET_EXTRA_TEXT_COLOUR} range:range];
    }
    
    textView.attributedText = lString;
    textView.selectedRange = (NSRange){MIN(selectedRange.location, textView.text.length), selectedRange.length};
}

- (void)updateTweetLengthLabel
{
    UITextView *regexHighlightView = self.tweetComposerView.tweetComposerTextView;
    
    if ((!regexHighlightView.text.length && !self.tweetImageToUpload) || (regexHighlightView.text.length + self.tweetComposerView.twitterImageURLLength) > TWEET_MAX_LENGTH)
        self.tweetComposerView.postNewTweetButton.enabled = NO;
    else
        self.tweetComposerView.postNewTweetButton.enabled = YES;
    
    self.tweetComposerView.tweetLengthCounterLabel.text = [NSString stringWithFormat:@"%d", TWEET_MAX_LENGTH - regexHighlightView.text.length - self.tweetComposerView.twitterImageURLLength];
}

- (void)openTweetComposerViewForReplyID:(NSString *)replyID
{
    [self.tweetComposerView setFrame:[[UIScreen mainScreen] bounds]];

    CGRect tweetComposerFrame = self.tweetComposerView.tweetComposerView.frame;
    tweetComposerFrame.origin = CGPointMake(0.0f, -tweetComposerFrame.size.height);
    self.tweetComposerView.tweetComposerView.frame = tweetComposerFrame;
    self.tweetComposerView.inReplyToID = replyID;
    
    if (self.tweetImageToUpload)
        self.tweetComposerView.twitterImageURLLength = self.tweetImageURLStringLength;
    else
        self.tweetComposerView.twitterImageURLLength = 0;
    
    [self.view addSubview:self.tweetComposerView];
    [self.view bringSubviewToFront:self.tweetComposerView];
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect tweetComposerFrame = self.tweetComposerView.tweetComposerView.frame;
        tweetComposerFrame.origin = CGPointMake(0.0f, 0.0f);
        self.tweetComposerView.tweetComposerView.frame = tweetComposerFrame;
    } completion:^(BOOL finished) {
        if (finished)
        {
            [self updateTweetLengthLabel];
            [self.tweetComposerView.tweetComposerTextView becomeFirstResponder];
        }
    }];
}

- (void)hideTweetComposerView:(BOOL)isCanceled
{
    [self.tweetComposerView.tweetComposerTextView resignFirstResponder];
    
    [UIView animateWithDuration:0.4f animations:^{
        
        CGRect composerFrame = self.tweetComposerView.tweetComposerView.frame;
        
        if (isCanceled)
            composerFrame.origin = CGPointMake(composerFrame.origin.x, -composerFrame.size.height);
        else
            composerFrame.origin = CGPointMake(composerFrame.size.width, composerFrame.origin.y);
        
        self.tweetComposerView.tweetComposerView.frame = composerFrame;
        
    } completion:^(BOOL finished) {
        if (finished)
            [self.tweetComposerView removeFromSuperview];
    }];
}

#pragma mark - Picture Select

/* take photo from camera & goto next page */
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *cameraImage = [info valueForKey:UIImagePickerControllerOriginalImage];
    UIImage *resultImage = [self mrResizePicture:cameraImage];
    
    [self.tweetComposerView.addImageToTweetButton setImage:resultImage forState:UIControlStateNormal];
    self.tweetImageToUpload = resultImage;
    
    self.tweetComposerView.twitterImageURLLength = self.tweetImageURLStringLength;
    [self.tweetComposerView.tweetComposerTextView setNeedsDisplay];
    
    [self dismissViewControllerAnimated:YES completion:^{
        [self.tweetComposerView.tweetComposerTextView becomeFirstResponder];
    }];
}

/* take photo from gallery & goto next page */
- (void)takeController:(FDTakeController *)controller gotPhoto:(UIImage *)photo withInfo:(NSDictionary *)info
{
    UIImage *resultImage = [self mrResizePicture:photo];
    
    self.tweetImageToUpload = resultImage;
    [self.tweetComposerView.addImageToTweetButton setImage:resultImage forState:UIControlStateNormal];
    
    self.tweetComposerView.twitterImageURLLength = self.tweetImageURLStringLength;
    [self.tweetComposerView.tweetComposerTextView setNeedsDisplay];
    [self.tweetComposerView.tweetComposerTextView becomeFirstResponder];
}

- (UIImage *)mrResizePicture:(UIImage *)inputImage
{    
    int max = 640;
    UIImage *resultImage;
    CGImageRef imgRef = [inputImage CGImage];
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    CGSize boundsNew;
    
    if (width <= max && height <= max) {
        resultImage = inputImage;
        boundsNew = resultImage.size;
    }
    else
    {
        if(width > height)
        {
            CGFloat ratio = width/height;
            boundsNew.width = max;
            boundsNew.height = max / ratio;
        }
        else
        {
            CGFloat ratio = height/width;
            boundsNew.height = max;
            boundsNew.width = max / ratio;
        }
    }
    
    UIGraphicsBeginImageContext(boundsNew);
    
    [inputImage drawInRect:CGRectMake(0.0, 0.0, boundsNew.width, boundsNew.height)];
    resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return resultImage;
}

#pragma mark - TweetViewCellDelegate methods

- (void)openURL:(NSString *)link
{
    TSMiniWebBrowser *webBrowser = [[TSMiniWebBrowser alloc] initWithUrl:[[NSURL alloc] initWithString:link]];
    webBrowser.showPageTitleOnTitleBar = YES;
    webBrowser.showActionButton = YES;
    webBrowser.showReloadButton = YES;
    webBrowser.mode = TSMiniWebBrowserModeNavigation;
    webBrowser.barStyle = UIBarStyleBlack;
    [self.navigationController pushViewController:webBrowser animated:YES];
}

- (void)replyToTweet:(TweetInfo *)tweetInfo
{
    NSMutableString *replyString = [[NSMutableString alloc] initWithString:tweetInfo.userTwitterName];
    NSMutableArray *replyArray = [tweetInfo.replyToUsers mutableCopy];
    
    if (replyArray.count)
    {
        [replyArray removeObject:[[NSString alloc] initWithFormat:@"@%@", self.account.username]];
        
        if (replyArray.count)
            [replyString appendFormat:@" %@", [replyArray componentsJoinedByString:@" "]];
    }
    
    [replyString appendString:@" "];
    
    self.tweetComposerView.tweetComposerTextView.text = [NSString stringWithString:replyString];
    [self.tweetComposerView.tweetComposerTextView setNeedsDisplay];
    
    if (replyArray.count)
        self.tweetComposerView.tweetComposerTextView.selectedRange = [replyString rangeOfString:[replyArray componentsJoinedByString:@" "]];
    
    [self openTweetComposerViewForReplyID:tweetInfo.tweetID];
}

- (void)quoteTweet:(TweetInfo *)tweetInfo
{
    NSString *quouteString = [[NSString alloc] initWithFormat:@"\"%@ %@\" ", tweetInfo.userTwitterName, tweetInfo.tweetText];
    
    self.tweetComposerView.tweetComposerTextView.text = quouteString;
    [self.tweetComposerView.tweetComposerTextView setNeedsDisplay];
    
    [self openTweetComposerViewForReplyID:nil];
}

- (void)showProfileForUser:(NSString *)username
{
    NSString *firstLetter = [[NSString stringWithFormat:@"%@", username] substringToIndex:1];
    
    if([firstLetter isEqualToString:@"@"])
        [self gotoProfile:[[NSString stringWithFormat:@"%@", username] substringFromIndex:1]];
    else
        [self gotoProfile:[NSString stringWithFormat:@"%@", username]];
}

- (void)replyToTweet:(NSString *)tweetID tweetType:(TweetType)type
{
    NSAssert(NO, @"Need to override this method in subclasses");
}

- (void)retweet:(TweetInfo *)tweetInfo tweetType:(TweetType)tweetType response:(NSDictionary *)response
{
    TweetInfo *receivedTweet = [[JSONDataParser sharedJSONParser] parseTweetInfo:response];
    
    tweetInfo.isRetweeted = YES;
    tweetInfo.retweetedTweetID = receivedTweet.originalTweetID;
}

- (void)unretweet:(TweetInfo *)tweetInfo tweetType:(TweetType)tweetType response:(NSDictionary *)response
{
    tweetInfo.isRetweeted = NO;
}

- (void)favoriteStateChangedForTweet:(TweetInfo *)tweetInfo tweetType:(TweetType)type isFavorited:(BOOL)isFavorited completed:(void(^)(void))completedBlock
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    completedBlock();
    
    NSString *tweetID = tweetInfo.isFavorited ? tweetInfo.favouritedTweetID : tweetInfo.originalTweetID;
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.twitter.com/1.1/favorites/%@.json", isFavorited ? @"destroy": @"create"]];
    NSDictionary *postParameters = [[NSDictionary alloc] initWithObjectsAndKeys:tweetID, @"id", nil];
    
    SLRequest *request  = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:TWRequestMethodPOST URL:url parameters:postParameters];
    [request setAccount:self.account];
    
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        if ([self isResponceContainsError:error])
            return;
        
        if (responseData)
        {
            NSError *error;
            NSDictionary *twittFavResponse = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
            if (error)
            {
                NSAssert(NO, error.localizedDescription);
            }
            
            if (![self isResponceValid:twittFavResponse error:error])
                return;
            
            if(twittFavResponse.count)
            {
                tweetInfo.isFavorited = !tweetInfo.isFavorited;
                if (tweetInfo.isFavorited)
                {
                    TweetInfo *receivedTweet = [[JSONDataParser sharedJSONParser] parseTweetInfo:twittFavResponse];
                    tweetInfo.favouritedTweetID = receivedTweet.originalTweetID;
                }
                else
                    tweetInfo.favouritedTweetID = nil;
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            });
        }
    }];
}

- (void)retweetStateChangedForTweet:(TweetInfo *)tweetInfo tweetType:(TweetType)type isRetweeted:(BOOL)isRetweeted completed:(void(^)(void))completedBlock
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;

    NSMutableArray *datasource = [self datasourceForTweetType:type];

    if (![datasource containsObject:tweetInfo])
        return;

    NSUInteger index = [datasource indexOfObject:tweetInfo];
    UITableView *tableView = [self tableViewForDatasource:datasource];

    NSString *tweetID = tweetInfo.isRetweeted ? tweetInfo.retweetedTweetID : tweetInfo.originalTweetID;
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.twitter.com/1.1/statuses/%@/%@.json",
                                       isRetweeted ? @"destroy": @"retweet", tweetID]];
    
    [tableView beginUpdates];
    TweetViewCell *cellToUpdate = (TweetViewCell *)[tableView cellForRowAtIndexPath:
                                                    [NSIndexPath indexPathForRow:index inSection:0]];
    completedBlock();
    
    if (!isRetweeted)
    {
        tweetInfo.isRetweeted = YES;
        cellToUpdate.retweetInfoText.text = @"Retweeted by You";
    }
    else
    {
        tweetInfo.isRetweeted = NO;
        if ([tweetInfo.retwittedByUserTwitterName isEqualToString:self.account.username])
        {
            cellToUpdate.retweetInfoText.text = @"";
        }
        else
            cellToUpdate.retweetInfoText.text = tweetInfo.retwittedByUser.length ? [[NSString alloc] initWithFormat:@"Retweeted by %@", tweetInfo.retwittedByUser] : @"";
    }
    
    [tableView endUpdates];
    
    SLRequest *request  = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:TWRequestMethodPOST URL:url parameters:nil];
    [request setAccount:self.account];
    
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        if ([self isResponceContainsError:error])
            return;
        
        if (responseData)
        {
            NSError *error;
            NSDictionary *twittRtwResponse = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
            if (error)
            {
                NSAssert(NO, error.localizedDescription);
            }
            
            if(twittRtwResponse.count)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (isRetweeted)
                        [self unretweet:tweetInfo tweetType:type response:twittRtwResponse];
                    else
                        [self retweet:tweetInfo tweetType:type response:twittRtwResponse];
                });
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            });
        }
    }];
}

- (void)deleteTweet:(NSString *)tweetID
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.twitter.com/1.1/statuses/destroy/%@.json", tweetID]];
    SLRequest *request  = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:TWRequestMethodPOST URL:url parameters:nil];
    [request setAccount:self.account];
    
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        if ([self isResponceContainsError:error])
            return;
        
        if (responseData)
        {
            NSError *error;
            NSArray *twittRemoveResponse = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
            if (error)
            {
                NSAssert(NO, error.localizedDescription);
            }
            
            if (![self isResponceValid:twittRemoveResponse error:error])
                return;
            
            if(twittRemoveResponse != nil && twittRemoveResponse.count > 0)
            {
                
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            });
        }
    }];
}

- (void)showConversationForTweet:(NSString *)tweetID username:(NSString *)username tweetType:(TweetType)type
{
    if ([tweetID isKindOfClass:[NSNull class]] || !tweetID.length)
        return;
    
    ConversationViewController *conversation = [[ConversationViewController alloc] init];
    conversation.tweetID = tweetID;
    conversation.account = self.account;
    conversation.username = username;
    
    [self.navigationController pushViewController:conversation animated:YES];
}

- (void)showFullscreenImage:(NSURL *)imageURL
{
    [self.fgController showImageWithURL:imageURL];
}

- (void)animateTweetButton:(BOOL)animate
{
    
}

#pragma mark - TweetComposerViewDelegate methods

- (void)postNewTweet:(TweetComposerView *)composerView
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    self.tweetComposerView.postNewTweetButton.enabled = NO;
    
    NSString *tweetText = self.tweetComposerView.tweetComposerTextView.text;
    
    if(tweetText.length > TWEET_MAX_LENGTH)
        tweetText = [[NSString stringWithFormat:@"%@",self.tweetComposerView.tweetComposerTextView.text] substringToIndex:TWEET_MAX_LENGTH];
    
    SLRequest *request;
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] initWithObjectsAndKeys:tweetText, @"status", nil];
    if (self.tweetComposerView.inReplyToID.length)
        [parameters setObject:self.tweetComposerView.inReplyToID forKey:@"in_reply_to_status_id"];

    
    if (self.tweetImageToUpload)
    {
        NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/update_with_media.json"];
        
        request  = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:TWRequestMethodPOST URL:url parameters:parameters];
        [request addMultipartData:UIImageJPEGRepresentation(self.tweetImageToUpload, 0.9) withName:@"media" type:@"image/jpg" filename:@"image.jpeg"];
    }
    else
    {
        NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/statuses/update.json"];
        request  = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:TWRequestMethodPOST URL:url parameters:parameters];
    }
    
    [self hideTweetComposerView:NO];
    self.tweetComposerView.postNewTweetButton.enabled = NO;
    
    [self animateTweetButton:YES];
    
    [request setAccount:self.account];
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
     {
         dispatch_async(dispatch_get_main_queue(), ^{
             self.tweetComposerView.postNewTweetButton.enabled = YES;
         });
         
         if ([self isResponceContainsError:error])
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [self animateTweetButton:NO];
             });
             
             return;
         }
         
         if (responseData)
         {
             NSError *error;
             NSArray *twittAddResponse = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
             if (error)
             {
                 NSAssert(NO, error.localizedDescription);
             }
             
             if (![self isResponceValid:twittAddResponse error:error])
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [self animateTweetButton:NO];
                 });
                 return;
             }
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 if(twittAddResponse && twittAddResponse.count)
                 {
                     [self.tweetComposerView.addImageToTweetButton setImage:[UIImage imageNamed:@"picture_button.png"] forState:UIControlStateNormal];
                     self.tweetImageToUpload = nil;
                 }
                 
                 self.tweetComposerView.tweetComposerTextView.text = @"";
                 [self.tweetComposerView.tweetComposerTextView setNeedsDisplay];
                 
                 [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [self animateTweetButton:NO];
                 });
             });
         }
     }];
}

- (void)deleteNewTweet:(TweetComposerView *)composerView
{
    NSString *tweetText = [self.tweetComposerView.tweetComposerTextView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if(tweetText.length || self.tweetImageToUpload)
    {
        [self.tweetComposerView.addImageToTweetButton setImage:[UIImage imageNamed:@"picture_button.png"] forState:UIControlStateNormal];
        self.tweetImageToUpload = nil;
        self.tweetComposerView.tweetComposerTextView.text = @"";
        [self.tweetComposerView.tweetComposerTextView setNeedsDisplay];
        [self updateTweetLengthLabel];
    }
    else
        [self hideTweetComposerView:YES];
}

- (void)addImageToTweet:(TweetComposerView *)composerView
{
    BlockActionSheet *sheet = [[BlockActionSheet alloc] initWithTitle:@""];

    [sheet addButtonWithTitle:@"Take picture" block:^{
        
        BOOL isCameraAvailable = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
        
        if (isCameraAvailable == NO) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Camera not available"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            return;
        }
        
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        [imagePicker setDelegate:self];
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
        [imagePicker setCameraCaptureMode:UIImagePickerControllerCameraCaptureModePhoto];
        [self presentViewController:imagePicker animated:YES completion:nil];
    }];
    
    [sheet addButtonWithTitle:@"Camera Roll" block:^{
        [self.takeController takePhotoOrChooseFromLibrary];
    }];
    
    if (self.tweetImageToUpload)
    {
        [sheet setDestructiveButtonWithTitle:@"Remove picture" block:^{
            [self.tweetComposerView.addImageToTweetButton setImage:[UIImage imageNamed:@"picture_button.png"]
                                                          forState:UIControlStateNormal];
            self.tweetImageToUpload = nil;
            self.tweetComposerView.twitterImageURLLength = 0;
            [self.tweetComposerView.tweetComposerTextView setNeedsDisplay];
            [self updateTweetLengthLabel];
        }];
    }
    
    [sheet setCancelButtonWithTitle:@"Cancel" block:nil];
    
    [sheet showInView:self.view];
}

- (void)dismissComposer:(TweetComposerView *)composerView
{
    [self hideTweetComposerView:YES];
}

- (void)suggestedTextWasUsed:(NSString *)suggestedText
{
    NSString *newText = [self.tweetComposerView.tweetComposerTextView.text stringByReplacingCharactersInRange:self.lastRange
                                                                                                   withString:[[NSString alloc] initWithFormat:@"%@ ", suggestedText]];
    
    self.tweetComposerView.tweetComposerTextView.text = newText;
    [self.tweetComposerView.tweetComposerTextView setNeedsDisplay];
    self.tweetComposerView.tweetComposerTextView.selectedRange = (NSRange){self.lastRange.location + suggestedText.length + 1, 0};
    
    [self updateTweetLengthLabel];
}

- (NSString *)mrGetTimeStrFromDate:(NSString *)date
{    
    NSString *returnSTR;
    
    NSDate *nowDate = [NSDate date];
    time_t unixNowTime = (time_t) [nowDate timeIntervalSince1970];
    int intNowTime = unixNowTime;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    [dateFormatter setLocale:locale];
    
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [dateFormatter setDateFormat:@"EEE MMM dd HH:mm:ss Z yyyy"];
    NSDate *dateFromString = [dateFormatter dateFromString:[NSString stringWithFormat:@"%@",date]];
    
    time_t unixTime = (time_t) [dateFromString timeIntervalSince1970];
    int intTime = unixTime;
    
    int mrTimeDelta = MAX(0, intNowTime - intTime);
    
    if(mrTimeDelta > (24 * 60 * 60))
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"d MMM"];
        [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
        
        returnSTR = [formatter stringFromDate:dateFromString];
    }
    else
    {
        if(mrTimeDelta > (60 * 60))
        {
            int hours = (int)mrTimeDelta/(60 * 60);
            returnSTR = [NSString stringWithFormat:@"%dh",hours];
        }
        else if(mrTimeDelta > 60)
        {
            int minutes = (int)mrTimeDelta / 60;
            returnSTR = [NSString stringWithFormat:@"%dm",minutes];
        }
        else
        {
            int seconds = mrTimeDelta;
            returnSTR = [NSString stringWithFormat:@"%ds",seconds];
        }
        
    }
    
    return returnSTR;
}

- (BOOL)isResponceValid:(id)responce error:(NSError *)error
{
    if (![responce isKindOfClass:[NSDictionary class]] && !error)
        return YES;
    
    if (error)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            
            [YRDropdownView showDropdownInView:self.view.window
                                         title:NSLocalizedString(@"Error", @"Error")
                                        detail:error.localizedDescription
                                         image:[UIImage imageNamed:@"dropdown-alert"]
                                      animated:YES
                                     hideAfter:2];
        });
        
        return NO;
    }
    
    NSArray *errors = [responce objectForKey:@"errors"];
    if (!errors.count)
        return YES;
    
    NSString *errorDescription = [[errors lastObject] objectForKey:@"message"];
    if (!errorDescription.length)
        return YES;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        [YRDropdownView showDropdownInView:self.view.window
                                     title:NSLocalizedString(@"Error", @"Error")
                                    detail:errorDescription
                                     image:[UIImage imageNamed:@"dropdown-alert"]
                                  animated:YES
                                 hideAfter:2];
    });
    
    return NO;
}

- (BOOL)isResponceContainsError:(NSError *)error
{
    if (!error)
        return NO;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        [YRDropdownView showDropdownInView:self.view.window
                                     title:NSLocalizedString(@"Error", @"Error")
                                    detail:error.localizedDescription
                                     image:[UIImage imageNamed:@"dropdown-alert"]
                                  animated:YES
                                 hideAfter:2];
    });
    
    return YES;
}

- (UITableView *)tableViewForDatasource:(NSMutableArray *)datasource
{
    NSAssert(NO, @"Unknown datasource type");
    return nil;
}

- (NSMutableArray *)datasourceForTweetType:(TweetType)tweetType
{
    NSAssert(NO, @"Unknown datasource type");
    return nil;
}

@end
