//
//  AccountsViewController.h
//  Tweetify
//
//  Created by Yuriy Berdnikov on 4/1/13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import <Accounts/Accounts.h>

@protocol AccountsControllerDelegate <NSObject>
- (void)accountWasChanged:(ACAccount *)account;
@end

@interface AccountsViewController : UITableViewController

@property (nonatomic, strong) NSArray *accounts;
@property (nonatomic, weak) id<AccountsControllerDelegate> delegate;

@end
