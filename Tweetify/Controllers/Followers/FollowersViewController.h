//
//  FollowersViewController.h
//  Tweetify
//
//  Created by Yuriy Berdnikov on 17.04.13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import "BaseViewController.h"

@interface FollowersViewController : BaseViewController

@property(nonatomic, copy) NSString *username;
@property(nonatomic, assign) BOOL showFollowees;

@end
