//
//  FollowersViewController.m
//  Tweetify
//
//  Created by Yuriy Berdnikov on 17.04.13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import "FollowersViewController.h"
#import "UserInfoCell.h"
#import "ProfileViewController.h"
#import "JSONDataParser.h"
#import "AllAroundPullView.h"
#import "DataProxy.h"
#import "ThemeAppearance.h"

@interface FollowersViewController ()
@property (weak, nonatomic) IBOutlet UITableView *contentTable;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *controllerTitle;

@property (nonatomic, strong) AllAroundPullView *bottomPullView;
@property (nonatomic, strong) NSMutableArray *followers;

@property (nonatomic, copy) NSString *nextCursor;
@end

@implementation FollowersViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    UINib *customCellNib = [UINib nibWithNibName:@"UserInfoCell" bundle:nil];
    [self.contentTable registerNib:customCellNib forCellReuseIdentifier:@"userInfoCell"];
    
    self.nextCursor = @"-1";
    self.controllerTitle.text = self.showFollowees ? NSLocalizedString(@"Follow", @"Follow") : NSLocalizedString(@"Followers", @"Followers");
    
    [self.activityIndicator startAnimating];
    
    [self applyThemeAppearance];
    
    [self loadUserFollowersForCursor:self.nextCursor];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
    [self setContentTable:nil];
    [self setCloseButton:nil];
    [self setActivityIndicator:nil];
    [self setControllerTitle:nil];
    [super viewDidUnload];
}

- (AllAroundPullView *)bottomPullView
{
    if (!_bottomPullView)
    {
        _bottomPullView = [[AllAroundPullView alloc] initWithScrollView:self.contentTable
                                                                       position:AllAroundPullViewPositionBottom
                                                                         action:^(AllAroundPullView *view){
                                                                             [self loadUserFollowersForCursor:self.nextCursor];
                                                                         }];
        _bottomPullView.activityView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
    }
    return _bottomPullView;
}

#pragma mark - Load data

- (void)loadUserFollowersForCursor:(NSString *)nextCursor
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
 
    NSString *requestAddress = [[NSString alloc] initWithFormat:@"https://api.twitter.com/1.1/%@/list.json?cursor=%@&screen_name=%@&skip_status=true&include_user_entities=false",
                                self.showFollowees ? @"friends" : @"followers", self.nextCursor, self.username];

    NSURL *url = [[NSURL alloc] initWithString:requestAddress];
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:TWRequestMethodGET URL:url parameters:nil];
    [request setAccount:self.account];
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
     {
         if (responseData)
         {
             NSError *error;
             NSDictionary *usersItems = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
             if (error)
             {
                 NSAssert(NO, error.localizedDescription);
             }
             
             if (![self isResponceValid:usersItems error:error])
                 return;
             
             NSMutableArray *parsedResult = [[JSONDataParser sharedJSONParser] parseUsersInfo:[usersItems objectForKey:@"users"]];
             
             if ([self.nextCursor isEqualToString:@"-1"])
                 self.followers = parsedResult;
             else
                 [self.followers addObjectsFromArray:parsedResult];
             
             self.nextCursor = [[usersItems objectForKey:@"next_cursor"] stringValue];
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 [self.contentTable reloadData];
                 
                 if (![_bottomPullView isDescendantOfView:self.contentTable])
                     [self.contentTable addSubview:self.bottomPullView];
                 
                 [self.activityIndicator stopAnimating];
                 [_bottomPullView finishedLoading];
                 
                 [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
             });
         }
         else
         {
             dispatch_async(dispatch_get_main_queue(), ^{

                 [self.activityIndicator stopAnimating];
                 [_bottomPullView finishedLoading];
                 
                 [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
             });
         }
     }];
}

#pragma mark - UIButton selector

- (IBAction)closeButtonPressed:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table functions

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.followers.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"userInfoCell";
    UserInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
        cell = [[UserInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    UserTwitterInfo *userInfo = [self.followers objectAtIndex:indexPath.row];
    cell.userInfo = userInfo;
    cell.currentTheme = [DataProxy sharedDataProxy].currentTheme;
    
    // background color for follower
    UIView *backgroundView = [[UIView alloc] init];
    backgroundView.backgroundColor = [UIColor colorWithWhite:30.0/255.0 alpha:1.0];
    
    cell.selectedBackgroundView = backgroundView;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 64;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UserTwitterInfo *userInfo = [self.followers objectAtIndex:indexPath.row];
    
    ProfileViewController *profile = [[ProfileViewController alloc] init];
    profile.account = self.account;
    profile.currentUserName = userInfo.userTwitterName;
    [self.navigationController pushViewController:profile animated:YES];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
}

#pragma mark - Theme related

- (void)applyThemeAppearance
{
    if ([DataProxy sharedDataProxy].currentTheme == AppThemeTypeBlack)
    {
        self.controllerTitle.textColor = DARK_VIEWS_TITLE_COLOR;
        self.view.backgroundColor = DARK_VIEWS_BACKGROUND_COLOR;
        
    }
    else
    {
        self.controllerTitle.textColor = LIGHT_VIEWS_TITLE_COLOR;
        self.view.backgroundColor = LIGHT_VIEWS_BACKGROUND_COLOR;
    }
}

@end
