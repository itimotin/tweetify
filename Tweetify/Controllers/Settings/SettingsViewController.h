//
//  SettingsViewController.h
//  Tweetify
//
//  Created by Yuriy Berdnikov on 10/1/13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import "BaseViewController.h"

@interface SettingsViewController : BaseViewController

@property (nonatomic, copy) void (^onDoneWithSettings)(void);

@end
