//
//  SettingsViewController.m
//  Tweetify
//
//  Created by Yuriy Berdnikov on 10/1/13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import "SettingsViewController.h"
#import "DataProxy.h"
#import "TutorialViewController.h"
#import "AboutViewController.h"
#import "iRate.h"

#if LITE_VERSION
#define APP_ID @"686880200"
#else
#define APP_ID @"632432082"
#endif

static NSString *cellIdentifier = @"settingsViewCell";

@interface SettingsViewController ()

@property (weak, nonatomic) IBOutlet UITableView *contentTableView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;

@property (nonatomic, strong) NSArray *contentDataSource;
@property (nonatomic, strong) NSArray *iconsDataSource;

@property (weak, nonatomic) IBOutlet UILabel *aboutText;
@end

@implementation SettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.contentDataSource = @[NSLocalizedString(@"Theme", nil),
                               NSLocalizedString(@"Write us", nil),
                               NSLocalizedString(@"How to use", nil),
                               NSLocalizedString(@"Rate us", nil)];
    
    self.iconsDataSource = @[[UIImage imageNamed:@"icon_placeholder"],
                             [UIImage imageNamed:@"icon_placeholder"],
                             [UIImage imageNamed:@"icon_placeholder"],
                             [UIImage imageNamed:@"icon_placeholder"]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(themeWasChanged) name:TWTAppThemeChangedNotification object:nil];
    
    [self.contentTableView registerNib:[UINib nibWithNibName:@"ThemeSwitchViewCell" bundle:nil] forCellReuseIdentifier:@"themeSwitchViewCell"];
    
    self.contentTableView.tableFooterView = [[UIView alloc] init];
    
    [self themeWasChanged];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (IBAction)closeButtonPressed:(UIButton *)sender
{
    if (self.onDoneWithSettings)
        self.onDoneWithSettings();
}

#pragma mark - Table functions

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.contentDataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    cell.textLabel.textColor = ([DataProxy sharedDataProxy].currentTheme == AppThemeTypeBlack) ? DARK_SETTINGS_TABLE_TEXT_COLOR : LIGHT_SETTINGS_TABLE_TEXT_COLOR;
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:28.0f];
    cell.imageView.image = [self.iconsDataSource objectAtIndex:indexPath.row];
    
    if (!indexPath.row)
        cell.textLabel.text = ([DataProxy sharedDataProxy].currentTheme == AppThemeTypeWhite) ? NSLocalizedString(@"Dark theme", nil) : NSLocalizedString(@"Light theme", nil);
    else
        cell.textLabel.text = [self.contentDataSource objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.row)
    {
        case 0:
            if ([DataProxy sharedDataProxy].currentTheme == AppThemeTypeBlack)
                [DataProxy sharedDataProxy].currentTheme = AppThemeTypeWhite;
            else
                [DataProxy sharedDataProxy].currentTheme = AppThemeTypeBlack;
            
            [tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
            return;
        case 1:
        {
            [self openTweetComposerViewForReplyID:@"tweetio_app"];
            
            self.tweetComposerView.tweetComposerTextView.text = @"@tweetio_app ";
        }
            return;
        case 2:
        {
            TutorialViewController *tutorialController = [[TutorialViewController alloc] initWithNibName:@"TutorialViewController" bundle:nil];
            [self.navigationController pushViewController:tutorialController animated:YES];
        }
            return;
        case 3:
            //rate
        {
            [iRate sharedInstance].appStoreID = APP_ID;
            [iRate sharedInstance].displayAppUsingStorekitIfAvailable = NO;
            [[iRate sharedInstance] openRatingsPageInAppStore];
        }
            return;
        case 4:
        {
            AboutViewController *aboutController = [[AboutViewController alloc] initWithNibName:@"AboutViewController" bundle:nil];
            [self.navigationController pushViewController:aboutController animated:YES];
        }
            return;
            
        default:
            break;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
}

- (void)themeWasChanged
{
    if ([DataProxy sharedDataProxy].currentTheme == AppThemeTypeBlack)
    {
        self.view.backgroundColor = DARK_VIEWS_BACKGROUND_COLOR;
        self.titleLabel.textColor = DARK_VIEWS_TITLE_COLOR;
        self.aboutText.textColor = DARK_SETTINGS_ABOUT_TEXT_COLOR;
    }
    else
    {
        self.view.backgroundColor = LIGHT_VIEWS_BACKGROUND_COLOR;
        self.titleLabel.textColor = LIGHT_VIEWS_TITLE_COLOR;
        self.aboutText.textColor = LIGHT_SETTINGS_ABOUT_TEXT_COLOR;
    }
    
    [self.tweetComposerView applyThemeAppearance];
    [self.contentTableView reloadData];
}

@end
