//
//  ConversationViewController.h
//  Tweetify
//
//  Created by Yuriy Berdnikov on 3/18/13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import "BaseViewController.h"

@interface ConversationViewController : BaseViewController

@property(nonatomic, strong) ACAccount *account;
@property(nonatomic, copy) NSString *tweetID;
@property(nonatomic, copy) NSString *username;

@end
