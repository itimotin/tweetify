//
//  ConversationViewController.m
//  Tweetify
//
//  Created by Yuriy Berdnikov on 3/18/13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import "UIImageView+WebCache.h"
#import "TweetInfo+Additions.h"
#import "ConversationViewController.h"
#import "JSONDataParser.h"
#import "TweetMediaInfo.h"
#import "TSMiniWebBrowser.h"
#import "SVPullToRefresh.h"
#import "ThemeAppearance.h"

@interface ConversationViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tweetsTable;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (nonatomic, strong) NSMutableOrderedSet *tweets;
@property (nonatomic, strong) NSMutableArray *childTweets;
@property (nonatomic, strong) TweetInfo *mainTweet;
@end

@implementation ConversationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _tweets = [[NSMutableOrderedSet alloc] init];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.activityIndicator startAnimating];
    
    UINib *customCellNib = [UINib nibWithNibName:@"TweetCellView" bundle:nil];
    [self.tweetsTable registerNib:customCellNib forCellReuseIdentifier:@"tweetCell"];
    
    self.mainTweet = [[TweetInfo alloc] init];
    self.mainTweet.tweetID = self.tweetID;
    
    [self applyThemeAppearance];
    
    [self loadInfoForTweet:self.tweetID];
    
    // Set the pull to refresh handler block
    [self.tweetsTable addPullToRefreshWithActionHandler:^{
        [self loadChildTweets];
    }];
    

    [self.tweetsTable.pullToRefreshView setTitle:NSLocalizedString(@"Show replies", @"Show replies") forState:SVPullToRefreshStateStopped];
    [self.tweetsTable.pullToRefreshView setTitle:NSLocalizedString(@"Release to show replies", @"Release to show replies") forState:SVPullToRefreshStateTriggered];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadConversationHistoryForUser:(NSString *)username sinceTweetID:(NSString *)tweetID
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.twitter.com/1.1/search/tweets.json?q=%@&since_id=%@", username, tweetID]];
    SLRequest *request  = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:TWRequestMethodGET URL:url parameters:nil];
    [request setAccount:self.account];
    
    __weak ConversationViewController *weakSelf = self;
    
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        if (responseData)
        {
            NSError *error;
            NSDictionary *tweetResponse = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
            if (error)
            {
                NSAssert(NO, error.localizedDescription);
            }
            
            if (![self isResponceValid:tweetResponse error:error])
                return;

            if(tweetResponse.count)
            {
                [self.childTweets addObjectsFromArray:[[JSONDataParser sharedJSONParser] parseTweetsFeed:[tweetResponse objectForKey:@"statuses"]]];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakSelf createConversationFrom:self.childTweets inReplyTo:weakSelf.tweetID];
                    
                    [weakSelf.tweets sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                        TweetInfo *tweet1 = (TweetInfo *)obj1;
                        TweetInfo *tweet2 = (TweetInfo *)obj2;
                        
                        if (tweet1.tweetID.longLongValue > tweet2.tweetID.longLongValue)
                            return (NSComparisonResult)NSOrderedAscending;
                        
                        if (tweet1.tweetID.longLongValue < tweet2.tweetID.longLongValue)
                            return (NSComparisonResult)NSOrderedDescending;
                        
                        return (NSComparisonResult)NSOrderedSame;
                        
                    }];
                    
                    [weakSelf.tweetsTable reloadData];
                    [weakSelf.tweetsTable.pullToRefreshView stopAnimating];
                    weakSelf.tweetsTable.showsPullToRefresh = NO;
                });
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            });
        }
    }];
}

- (void)createConversationFrom:(NSMutableArray *)tweets inReplyTo:(NSString *)tweetID
{
    if ([tweetID isKindOfClass:[NSNull class]] || !tweetID.length)
        return;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"repliedToTweetID like %@", tweetID];
    NSArray *childTweets = [tweets filteredArrayUsingPredicate:predicate];
    if (childTweets.count)
    {
        [self.tweets addObjectsFromArray:childTweets];
        TweetInfo *tweet = [childTweets lastObject];
        [self createConversationFrom:tweets inReplyTo:tweet.tweetID];
    }
    else
    {
        [self.tweetsTable.pullToRefreshView stopAnimating];
        self.tweetsTable.showsPullToRefresh = NO;
    }
}

- (void)buildConversationForChildTweets:(NSMutableArray *)tweets inReplyTo:(NSString *)tweetID
{
    if ([tweetID isKindOfClass:[NSNull class]] || !tweetID.length)
        return;
        
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"repliedToTweetID like %@", tweetID];
    NSArray *childTweets = [tweets filteredArrayUsingPredicate:predicate];
    if (childTweets.count)
    {
        [self.tweets addObjectsFromArray:childTweets];
        TweetInfo *tweet = [childTweets lastObject];
        [self loadConversationHistoryForUser:tweet.userTwitterName sinceTweetID:tweet.tweetID];
    }
    {
        [self.tweetsTable.pullToRefreshView stopAnimating];
        self.tweetsTable.showsPullToRefresh = NO;
    }
}

- (void)loadChildTweets
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.twitter.com/1.1/search/tweets.json?q=%@&since_id=%@&count=100", self.username, self.tweetID]];
    SLRequest *request  = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:TWRequestMethodGET URL:url parameters:nil];
    [request setAccount:self.account];
    
    __weak ConversationViewController *weakSelf = self;
    
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        if (responseData)
        {
            NSError *error;
            NSDictionary *tweetResponse = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
            if (error)
            {
                NSAssert(NO, error.localizedDescription);
            }
            
            if (![self isResponceValid:tweetResponse error:error])
                return;

            if(tweetResponse.count)
            {
                self.childTweets = [[JSONDataParser sharedJSONParser] parseTweetsFeed:[tweetResponse objectForKey:@"statuses"]];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [weakSelf buildConversationForChildTweets:self.childTweets inReplyTo:weakSelf.tweetID];
                });
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            });
        }
    }];
}

- (void)loadInfoForTweet:(NSString *)tweetID
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.twitter.com/1.1/statuses/show.json?id=%@", tweetID]];
    SLRequest *request  = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:TWRequestMethodGET URL:url parameters:nil];
    [request setAccount:self.account];
    
    __weak ConversationViewController *weakSelf = self;
    
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        if (responseData)
        {
            NSError *error;
            NSDictionary *tweetResponse = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
            if (error)
            {
                NSAssert(NO, error.localizedDescription);
            }
            
            if (![self isResponceValid:tweetResponse error:error])
                return;
            
            if(tweetResponse.count)
            {
                TweetInfo *tweet = [[JSONDataParser sharedJSONParser] parseTweetInfo:tweetResponse];
                [weakSelf.tweets addObject:tweet];
                
                if (![tweet.repliedToTweetID isKindOfClass:[NSNull class]] && tweet.repliedToTweetID.length)
                    [weakSelf loadInfoForTweet:tweet.repliedToTweetID];
                else
                {
                    [weakSelf.tweets sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                        TweetInfo *tweet1 = (TweetInfo *)obj1;
                        TweetInfo *tweet2 = (TweetInfo *)obj2;
                        
                        if (tweet1.tweetID.longLongValue > tweet2.tweetID.longLongValue)
                            return (NSComparisonResult)NSOrderedAscending;
                        
                        if (tweet1.tweetID.longLongValue < tweet2.tweetID.longLongValue)
                            return (NSComparisonResult)NSOrderedDescending;
                        
                        return (NSComparisonResult)NSOrderedSame;
                    }];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [weakSelf.tweetsTable reloadData];
                        [weakSelf.tweetsTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.tweets indexOfObject:self.mainTweet] inSection:0]
                                                    atScrollPosition:UITableViewScrollPositionMiddle animated:NO];
                        [self.activityIndicator stopAnimating];
                    });
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            });
        }
    }];
}

- (void)showConversationForTweet:(NSString *)tweetID username:(NSString *)username tweetType:(TweetType)type
{
    // prevent opening conversation view again
}

#pragma mark - Table functions

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.tweets.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int rowIndex = indexPath.row;
    
    static NSString *CellIdentifier = @"tweetCell";
    TweetViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
        cell = [[TweetViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    cell.delegate = self;
    cell.currentTheme = [DataProxy sharedDataProxy].currentTheme;
    
    TweetInfo *tweet = [self.tweets objectAtIndex:rowIndex];
    
    if ([tweet.userTwitterName isEqualToString:[[NSString alloc] initWithFormat:@"@%@", self.account.username]])
        cell.showDeleteButton = YES;
    else
        cell.showDeleteButton = NO;
    
    cell.tweetInfo = tweet;
    cell.tweetType = kTweetTypeUsers;
    [cell.userIcon setImageWithURL:tweet.userAvaURL];
    cell.tweetTime.text = [self mrGetTimeStrFromDate:tweet.createdDate];
    cell.username.text = tweet.userName;
    cell.userTwitterName.text = tweet.userTwitterName;
    cell.isFavorited = tweet.isFavorited;
    cell.repliedTweetID = tweet.repliedToTweetID;
    cell.links = tweet.links;
    cell.hideConversationIndicator = YES;
    
    if ([tweet.retwittedByUserTwitterName isEqualToString:self.account.username] || tweet.isRetweeted)
    {
        cell.isRetweeted = YES;
        cell.retweetInfoText.text = NSLocalizedString(@"Retweeted by You", @"Retweeted by You");
    }
    else
    {
        cell.isRetweeted = NO;
        cell.retweetInfoText.text = tweet.retwittedByUser.length ? [[NSString alloc] initWithFormat:@"Retweeted by %@", tweet.retwittedByUser] : @"";
    }
    
    [cell.tweetImage cancelCurrentImageLoad];
    cell.originalImageURL = nil;
    if (tweet.media.count)
    {
        //TODO: place only 1 image - add support more
        TweetMediaInfo *mediaInfo = (TweetMediaInfo *)[tweet.media lastObject];
        
        NSString *thumbAddress;
        if ([mediaInfo.mediaType isEqualToString:@"photo"])
        {
            thumbAddress = [[NSString alloc] initWithFormat:@"%@:thumb", mediaInfo.mediaURL];
            
            [cell.tweetImage setImageWithURL:[[NSURL alloc] initWithString:thumbAddress]
                            placeholderImage:[UIImage imageNamed:@"tweet_image_placeholder.png"]];
            cell.originalImageURL = [[NSURL alloc] initWithString:mediaInfo.mediaURL];
        }
        else if ([mediaInfo.mediaType isEqualToString:@"instagram"])
        {
            cell.tweetImage.image = [UIImage imageNamed:@"tweet_image_placeholder.png"];
            NSURLRequest *request = [NSURLRequest requestWithURL:[[NSURL alloc] initWithString:mediaInfo.mediaURL]
                                                     cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                 timeoutInterval:10.0];
            
            __weak UITableView *weakTableView = tableView;
            __weak TweetMediaInfo *weakMediaInfo = mediaInfo;
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
             {
                 if (error)
                 {
                     NSAssert(NO, error.localizedDescription);
                     return;
                 }
                 
                 NSDictionary *imageData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                 if (error)
                 {
                     NSAssert(NO, error.localizedDescription);
                     return;
                 }
                 
                 if ([[imageData objectForKey:@"url"] length])
                 {
                     weakMediaInfo.mediaURL = [imageData objectForKey:@"url"];
                     weakMediaInfo.mediaType = @"";
                     
                     TweetViewCell *cellToUpdate = (TweetViewCell *)[weakTableView cellForRowAtIndexPath:indexPath];
                     if (cellToUpdate)
                     {
                         [cellToUpdate.tweetImage setImageWithURL:[[NSURL alloc] initWithString:[imageData objectForKey:@"url"]]
                                                 placeholderImage:[UIImage imageNamed:@"tweet_image_placeholder.png"]];
                         cellToUpdate.originalImageURL = [[NSURL alloc] initWithString:[imageData objectForKey:@"url"]];
                     }
                 }
             }];
        }
        else
        {
            if (mediaInfo.mediaURL.length)
            {
                [cell.tweetImage setImageWithURL:[[NSURL alloc] initWithString:mediaInfo.mediaURL]
                                placeholderImage:[UIImage imageNamed:@"tweet_image_placeholder.png"]];
                cell.originalImageURL = [[NSURL alloc] initWithString:mediaInfo.mediaURL];
            }
        }
    }
    
    CGFloat height = [JSCoreTextView measureFrameHeightForText:tweet.tweetText
                                                      fontName:CELL_TWEET_TEXT_FONT.fontName
                                                      fontSize:CELL_TWEET_TEXT_FONT.pointSize
                                            constrainedToWidth:303
                                                    paddingTop:0
                                                   paddingLeft:0];
    
    CGRect tweetTextFrame = cell.tweetText.frame;
    tweetTextFrame.size.height = height;
    cell.tweetText.frame = tweetTextFrame;
    
    cell.tweetText.text = tweet.tweetText;

    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TweetInfo *tweet = [self.tweets objectAtIndex:indexPath.row];
    
    return [tweet optimalCellSizeForTweet];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
}

- (void)viewDidUnload
{
    [self setTweetsTable:nil];
    [self setCloseButton:nil];
    [self setActivityIndicator:nil];
    [super viewDidUnload];
}

- (IBAction)closeButtonPressed:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - TweetViewCellDelegate methods

- (void)replyToTweet:(NSString *)tweetID tweetType:(TweetType)type
{
    TweetInfo *tweetInfo = [[TweetInfo alloc] init];
    tweetInfo.tweetID = tweetID;
    
    if (![self.tweets containsObject:tweetInfo])
    {
        NSAssert(NO, @"Somethings wrong with this tweet");
        return;
    }
    
    TweetInfo *tweetToUpdate = [self.tweets objectAtIndex:[self.tweets indexOfObject:tweetInfo]];

    [self replyToTweet:tweetToUpdate];
}

- (void)retweet:(NSString *)tweetID tweetType:(TweetType)tweetType
{
    TweetInfo *tweet = [[TweetInfo alloc] init];
    tweet.tweetID = tweetID;
    
    NSUInteger index = [self.tweets indexOfObject:tweet];
    TweetInfo *tweetToUpdate = [self.tweets objectAtIndex:index];
    
    [self.tweetsTable beginUpdates];
    TweetViewCell *cellToUpdate = (TweetViewCell *)[self.tweetsTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
    
    tweetToUpdate.isRetweeted = YES;
    cellToUpdate.retweetInfoText.text = @"Retweeted by You";
    
    [self.tweetsTable endUpdates];
}

- (void)unretweet:(NSString *)tweetID tweetType:(TweetType)tweetType
{
    TweetInfo *tweet = [[TweetInfo alloc] init];
    tweet.tweetID = tweetID;
    
    NSUInteger index = [self.tweets indexOfObject:tweet];
    TweetInfo *tweetToUpdate = [self.tweets objectAtIndex:index];
    
    [self.tweetsTable beginUpdates];
    TweetViewCell *cellToUpdate = (TweetViewCell *)[self.tweetsTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
    
    tweetToUpdate.isRetweeted = NO;
    cellToUpdate.retweetInfoText.text = tweetToUpdate.retwittedByUser.length ? [[NSString alloc] initWithFormat:@"Retweeted by %@", tweetToUpdate.retwittedByUser] : @"";
    
    [self.tweetsTable endUpdates];
}

- (void)retweetStateChangedForTweet:(TweetInfo *)tweetInfo
                          tweetType:(TweetType)type
                        isRetweeted:(BOOL)isRetweeted
                          completed:(void(^)(void))completedBlock
{
    void (^localBlock)(void); // declaration
    
//    NSUInteger index = [self.tweets indexOfObject:tweetInfo];
//    
//    [self.tweetsTable beginUpdates];
//    TweetViewCell *cellToUpdate = (TweetViewCell *)[self.tweetsTable cellForRowAtIndexPath:
//                                                    [NSIndexPath indexPathForRow:index inSection:0]];
//    if (!isRetweeted)
//    {
//        tweetInfo.isRetweeted = YES;
//        cellToUpdate.retweetInfoText.text = @"Retweeted by You";
//    }
//    else
//    {
//        tweetInfo.isRetweeted = NO;
//        cellToUpdate.retweetInfoText.text = tweetInfo.retwittedByUser.length ? [[NSString alloc] initWithFormat:@"Retweeted by %@", tweetInfo.retwittedByUser] : @"";
//    }
//    
//    completedBlock();
//    [self.tweetsTable endUpdates];
    
    __weak ConversationViewController *weakSelf = self;
    localBlock = ^(void) {
        
        NSUInteger index = [weakSelf.tweets indexOfObject:tweetInfo];
        
        [weakSelf.tweetsTable beginUpdates];
        TweetViewCell *cellToUpdate = (TweetViewCell *)[weakSelf.tweetsTable cellForRowAtIndexPath:
                                                        [NSIndexPath indexPathForRow:index inSection:0]];
        if (!isRetweeted)
        {
            tweetInfo.isRetweeted = YES;
            cellToUpdate.retweetInfoText.text = @"Retweeted by You";
        }
        else
        {
            tweetInfo.isRetweeted = NO;
            cellToUpdate.retweetInfoText.text = tweetInfo.retwittedByUser.length ? [[NSString alloc] initWithFormat:@"Retweeted by %@", tweetInfo.retwittedByUser] : @"";
        }
        
        [weakSelf.tweetsTable endUpdates];
    };
    
    [super retweetStateChangedForTweet:tweetInfo tweetType:type isRetweeted:isRetweeted completed:localBlock];
}

- (UITableView *)tableViewForDatasource:(NSMutableArray *)datasource
{
    return self.tweetsTable;
}

- (NSMutableArray *)datasourceForTweetType:(TweetType)tweetType
{
    return [[NSMutableArray alloc] initWithArray:self.tweets.array];
}

- (void)applyThemeAppearance
{
    if ([DataProxy sharedDataProxy].currentTheme == AppThemeTypeBlack)
    {
        self.view.backgroundColor = DARK_VIEWS_BACKGROUND_COLOR;
        self.titleLabel.textColor = DARK_VIEWS_TITLE_COLOR;
        self.tweetsTable.pullToRefreshView.textColor = [UIColor whiteColor];
        self.tweetsTable.pullToRefreshView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
        self.tweetsTable.pullToRefreshView.arrowColor = [UIColor whiteColor];
    }
    else
    {
        self.view.backgroundColor = LIGHT_VIEWS_BACKGROUND_COLOR;
        self.titleLabel.textColor = LIGHT_VIEWS_TITLE_COLOR;
        self.tweetsTable.pullToRefreshView.textColor = [UIColor darkGrayColor];
        self.tweetsTable.pullToRefreshView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        self.tweetsTable.pullToRefreshView.arrowColor = [UIColor darkGrayColor];
    }
}

@end
