//
//  MainViewController.h
//  Tweetify
//
//  Created by smr on 20.02.13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <MobileCoreServices/UTCoreTypes.h>
#import "BaseViewController.h"
#import "TSUserStream.h"
#import "TSFilterStream.h"
#import "TSModelParser.h"
#import "NSArray+Enumerable.h"



@interface MainViewController : BaseViewController <UIScrollViewDelegate, TSStreamDelegate>

@property int mrCurrentPageIndex;

@property (weak, nonatomic) IBOutlet UIScrollView *scroll;


@property (weak, nonatomic) IBOutlet UIView *mrView1;
@property (weak, nonatomic) IBOutlet UIView *mrView2;
@property (weak, nonatomic) IBOutlet UITableView *mrTimeLineTable;
@property (weak, nonatomic) IBOutlet UITableView *mrMentionsTable;

@property (nonatomic, retain) NSArray* accounts;
@property (nonatomic, retain) ACAccountStore* accountStore;
@property (nonatomic, retain) TSStream* stream;

// tweet composer related
@property (weak, nonatomic) IBOutlet UIButton *createNewTweetButton;
- (IBAction)createNewTweetButtonPressed:(UIButton *)sender;

@end
