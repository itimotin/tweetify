//
//  MainViewController.m
//  Tweetify
//
//  Created by smr on 20.02.13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import "UIImageView+WebCache.h"
#import "TweetInfo+Additions.h"
#import "MainViewController.h"
#import "ProfileViewController.h"
#import "JSONDataParser.h"
#import "ProfileView.h"
#import "TweetMediaInfo.h"
#import "Reachability.h"
#import "BlockActionSheet.h"
#import "AllAroundPullView.h"
#import "AccountsViewController.h"
#import "FPPopoverController.h"
#import "MYIntroductionView.h"
#import "SSCheckBoxView.h"
#import "FollowersViewController.h"
#import "LKBadgeView.h"
#import "BlockAlertView.h"
#import "LoadMoreTweetsCell.h"
#import "SettingsViewController.h"
#import "DataProxy.h"

#define kTweetifyLatestTweet @"tweetify.latest.tweet"
#define kTweetifyTweetmarkerURL @"http://tweetmarker.net"
#define kTweetifyTweetmarkerAccessToken @"TW-A69F455221C0"
#define kTweetifyRequestTimeout 10.0f

@interface MainViewController () <UIScrollViewDelegate, ProfileViewDelegate,
UITableViewDataSource, UITableViewDelegate, FPPopoverControllerDelegate, AccountsControllerDelegate,
MYIntroductionDelegate>

@property (nonatomic, weak) IBOutlet ProfileView *profileView;

@property (atomic, retain) NSMutableArray *mrTimelineItems;
@property (atomic, retain) NSMutableArray *mrMentionsItems;
@property (atomic, retain) NSMutableArray *mrUserTweets;

@property (weak, nonatomic) IBOutlet UILabel *timelineTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *mentionsTitleLabel;

@property (nonatomic, strong) AllAroundPullView *timelineBottomPullView;
@property (nonatomic, strong) AllAroundPullView *mentionsBottomPullView;
@property (nonatomic, strong) AllAroundPullView *profileBottomPullView;
@property (nonatomic, strong) FPPopoverController *accountsPopover;

@property (nonatomic, assign) BOOL isUserDataLoaded;
@property (nonatomic, assign) BOOL isTimelineDataLoaded;

@property (nonatomic, strong) SSCheckBoxView *checkbox;

@property (weak, nonatomic) IBOutlet LKBadgeView *badgeView;
@property (weak, nonatomic) IBOutlet LKBadgeView *mentionsBadgeView;
@property (nonatomic, strong) NSNumberFormatter *numberFormatter;

@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
    }
    return self;
}

#pragma mark - View funcs

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.numberFormatter = [[NSNumberFormatter alloc] init];
    [self.numberFormatter  setNumberStyle:NSNumberFormatterDecimalStyle];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadLatestData)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(saveLastTweet)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(saveLastTweet)
                                                 name:UIApplicationWillTerminateNotification
                                               object:nil];
    
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"ProfileView" owner:self options:nil];
    
    // assuming the view is the only top-level object in the nib file (besides File's Owner and First Responder)
    UIView *nibView = [nibObjects objectAtIndex:0];
    self.profileView = (ProfileView *)nibView;
    self.profileView.delegate = self;
    [self.profileView.leftButton setImage:[UIImage imageNamed:@"Profile.png"] forState:UIControlStateNormal];
    self.profileView.rightButton.hidden = YES;
    self.profileView.showSettingsButton = YES;
    [self.profileView setOnSettingsButtonPressed:^{
        //open settings screen
        
        SettingsViewController *settingsController = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil];
        [settingsController setOnDoneWithSettings:^{
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        
        UINavigationController *settingsNavigationController = [[UINavigationController alloc] initWithRootViewController:settingsController];
        settingsNavigationController.navigationBarHidden = YES;
        
        [self presentViewController:settingsNavigationController animated:YES completion:^{
            
        }];
    }];
    
    UINib *customCellNib = [UINib nibWithNibName:@"TweetCellView" bundle:nil];
    
    [self.mrTimeLineTable registerNib:customCellNib forCellReuseIdentifier:@"tweetCell"];
    [self.mrTimeLineTable registerNib:[UINib nibWithNibName:@"LoadMoreTweetsCell" bundle:nil] forCellReuseIdentifier:@"loadMoreTweetsCell"];
    [self.mrMentionsTable registerNib:customCellNib forCellReuseIdentifier:@"tweetCell"];
    [self.profileView.tweetsTable registerNib:customCellNib forCellReuseIdentifier:@"tweetCell"];
    self.profileView.tweetsTable.delegate = self;
    self.profileView.tweetsTable.dataSource = self;
    
    /* this is temp array */
    NSArray *pages = [NSArray arrayWithObjects:self.mrView1, self.mrView2, self.profileView, nil];
    
    /* scroll functionality init */
    self.scroll.contentSize = CGSizeMake(pages.count * CGRectGetWidth(self.scroll.frame), CGRectGetHeight(self.scroll.frame));
    [self.scroll setScrollEnabled:YES];
    self.scroll.scrollsToTop = NO;
    [self.scroll setDelegate:self];
    
    for (int i = 0; i < pages.count; i++)
    {
        CGRect frame;
        frame.origin.x = CGRectGetWidth(self.scroll.frame) * i;
        frame.origin.y = 0;
        frame.size = self.scroll.bounds.size;
        
        [[pages objectAtIndex:i] setFrame:frame];
        
        [self.scroll addSubview:[pages objectAtIndex:i]];
        
    }
    
    //self.scroll.scrollsToTop = NO;
    
    Reachability * reach = [Reachability reachabilityWithHostname:@"www.google.com"];
    
    reach.reachableBlock = ^(Reachability * reachability)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self checkIfUserHasTwitterAccount:^{
                [self mrGetAccessToTwitterAccounts];
            }];
        });
    };
    
    reach.unreachableBlock = ^(Reachability * reachability)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [YRDropdownView showDropdownInView:self.view.window
                                         title:NSLocalizedString(@"Error", @"Error")
                                        detail:NSLocalizedString(@"Please check your network connection or try again", @"Please check your network connection or try again")
                                         image:nil
                                      animated:YES
                                     hideAfter:2];
        });
    };
    
    [reach startNotifier];
    
    //timeline badge
    self.badgeView.widthMode = LKBadgeViewWidthModeSmall;
    self.badgeView.heightMode = LKBadgeViewHeightModeStandard;
    self.badgeView.horizontalAlignment =  LKBadgeViewHorizontalAlignmentLeft;
    self.badgeView.textColor = [UIColor whiteColor];
    self.badgeView.badgeColor = [UIColor colorWithRed:0 / 255.0 green:153 / 255.0 blue:203 / 255.0 alpha:1.0];
    
    //mentions badge
    self.mentionsBadgeView.widthMode = LKBadgeViewWidthModeSmall;
    self.mentionsBadgeView.heightMode = LKBadgeViewHeightModeStandard;
    self.mentionsBadgeView.horizontalAlignment =  LKBadgeViewHorizontalAlignmentLeft;
    self.mentionsBadgeView.textColor = [UIColor whiteColor];
    self.mentionsBadgeView.badgeColor = [UIColor colorWithRed:0 / 255.0 green:153 / 255.0 blue:203 / 255.0 alpha:1.0];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(themeWasChanged) name:TWTAppThemeChangedNotification object:nil];
    [self themeWasChanged];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    self.accounts = nil;
    self.accountStore = nil;
    self.account = nil;
    [self.stream stop];
    self.stream = nil;
    self.profileView = nil;
    
    [self setScroll:nil];
    [self setMrView1:nil];
    [self setMrView2:nil];
    [self setMrTimeLineTable:nil];
    [self setMrMentionsTable:nil];
    [self setCreateNewTweetButton:nil];
    [self setTweetComposerView:nil];
}

- (void)viewDidUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    self.profileView = nil;
    [self setScroll:nil];
    [self setMrView1:nil];
    [self setMrView2:nil];
    [self setMrTimeLineTable:nil];
    [self setMrMentionsTable:nil];
    [self setCreateNewTweetButton:nil];
    [self setTweetComposerView:nil];
    [self setBadgeView:nil];
    [self setMentionsBadgeView:nil];
    [super viewDidUnload];
}

- (void)showTutorialScreen
{
    MYIntroductionPanel *panel = [[MYIntroductionPanel alloc] initWithimage:[UIImage imageNamed:@"slide1.png"] description:@""];
    MYIntroductionPanel *panel2 = [[MYIntroductionPanel alloc] initWithimage:[UIImage imageNamed:@"slide2.png"] description:@""];
    MYIntroductionPanel *panel3 = [[MYIntroductionPanel alloc] initWithimage:[UIImage imageNamed:@"slide3.png"] description:@""];
    MYIntroductionPanel *panel4 = [[MYIntroductionPanel alloc] initWithimage:[UIImage imageNamed:@"slide4.png"] description:@""];
    
    self.checkbox = [[SSCheckBoxView alloc] initWithFrame:CGRectMake(90, 275, 200, 50)
                                                    style:kSSCheckBoxViewStyleMono
                                                  checked:YES];
    [self.checkbox setText:NSLocalizedString(@"Follow us", @"Follow us")];
    
    panel4.accessoryView = self.checkbox;
    
    MYIntroductionView *introductionView = [[MYIntroductionView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)
                                                                              panels:@[panel, panel2, panel3, panel4]
                                                                   languageDirection:MYLanguageDirectionLeftToRight];
    
    introductionView.delegate = self;
    [introductionView showInView:self.view];
    
    [[NSUserDefaults standardUserDefaults] setObject:@(YES) forKey:@"tweetify.tutorial.isDisplayed"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)introductionDidFinishWithType:(MYFinishType)finishType
{
    if (!self.isUserDataLoaded || !self.isTimelineDataLoaded)
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate showSplashScreen];
        [appDelegate splashScreenAddActivity];
    }
    
    if (self.checkbox.checked)
    {
        NSURL *url = [[NSURL alloc] initWithString:@"https://api.twitter.com/1.1/friendships/create.json"];
        
        SLRequest *request  = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                                 requestMethod:SLRequestMethodPOST
                                                           URL:url
                                                    parameters:@{@"screen_name" : @"tweetio_app"}];
        [request setAccount:self.account];
        [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
         {
             
         }];
    }
}

- (AllAroundPullView *)timelineBottomPullView
{
    if (!_timelineBottomPullView)
    {
        _timelineBottomPullView = [[AllAroundPullView alloc] initWithScrollView:self.mrTimeLineTable
                                                                       position:AllAroundPullViewPositionBottom
                                                                         action:^(AllAroundPullView *view){
                                                                             [self mrGetTwDataTimeLine:self.account usePaging:YES];
                                                                         }];
        _timelineBottomPullView.activityView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
    }
    return _timelineBottomPullView;
}

- (AllAroundPullView *)mentionsBottomPullView
{
    if (!_mentionsBottomPullView)
    {
        _mentionsBottomPullView = [[AllAroundPullView alloc] initWithScrollView:self.mrMentionsTable
                                                                       position:AllAroundPullViewPositionBottom
                                                                         action:^(AllAroundPullView *view){
                                                                             [self mrGetTwDataMentions:self.account usePaging:YES];
                                                                         }];
        _mentionsBottomPullView.activityView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
    }
    return _mentionsBottomPullView;
}

- (AllAroundPullView *)profileBottomPullView
{
    if (!_profileBottomPullView)
    {
        _profileBottomPullView = [[AllAroundPullView alloc] initWithScrollView:self.profileView.tweetsTable
                                                                      position:AllAroundPullViewPositionBottom
                                                                        action:^(AllAroundPullView *view){
                                                                            [self mrGetTwDataUserTweets:self.account usePaging:YES];
                                                                        }];
        _profileBottomPullView.activityView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
    }
    return _profileBottomPullView;
}

#pragma mark - UIScrollView delegate methods

- (NSUInteger)activePage
{
    CGFloat pageWidth = self.scroll.frame.size.width;
    NSUInteger page = floor((self.scroll.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    
    return page;
}

- (IBAction)scrollToTop:(UITapGestureRecognizer *)sender
{
    switch ([self activePage])
    {
        case 0:
            if (self.mrTimelineItems.count)
                [self.mrTimeLineTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                            atScrollPosition:UITableViewScrollPositionTop
                                                    animated:YES];
            return;
        case 1:
            if (self.mrMentionsItems.count)
                [self.mrMentionsTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                            atScrollPosition:UITableViewScrollPositionTop
                                                    animated:YES];
            return;
        case 2:
            if (self.mrUserTweets.count)
                [self.profileView.tweetsTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
                                                    atScrollPosition:UITableViewScrollPositionTop
                                                            animated:YES];
            return;
        default:
            break;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    int page = [self activePage];
    
    //TODO: add helper method for this
    UITableView *activeTable;
    if (!page)
        activeTable = self.mrTimeLineTable;
    else if (page == 1)
        activeTable = self.mrMentionsTable;
    else
        activeTable = self.profileView.tweetsTable;
    
    if (activeTable.contentOffset.y <= 0 && !((NSUInteger)sender.contentOffset.x % 320))
    {
        if (!page)
            self.badgeView.text = @"";
        else if (page == 1)
            self.mentionsBadgeView.text = @"";
    }
    
    NSIndexPath *index = [activeTable indexPathForSelectedRow];
    if (index)
        [activeTable deselectRowAtIndexPath:index animated:YES];
    
    if(page == 2)
        self.createNewTweetButton.hidden = YES;
    else
        self.createNewTweetButton.hidden = NO;
    
    if (sender == self.profileView.tweetsTable)
    {
        if (sender.contentOffset.y < 0) {
            [sender setContentOffset:CGPointMake(sender.contentOffset.x, 0)];
        }
    }
}

#pragma mark - Table functions

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self dataSourceForTabelView:tableView] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int rowIndex = indexPath.row;

    TweetInfo *tweet = [[self dataSourceForTabelView:tableView] objectAtIndex:rowIndex];
    
    if ([tweet.tweetID isEqualToString:@"STUB"])
    {
        //display button
        LoadMoreTweetsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"loadMoreTweetsCell"];
        
        if (!cell)
            cell = [[LoadMoreTweetsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"loadMoreTweetsCell"];
        
        __weak LoadMoreTweetsCell *weakCell = cell;
        [cell setOnLoadMorePressed:^{
            NSIndexPath *cellIndex = [self.mrTimeLineTable indexPathForCell:weakCell];

            [self loadMoreTweetsSinceID:[[self.mrTimelineItems objectAtIndex:cellIndex.row + 1] originalTweetID] maxTweetID:[[self.mrTimelineItems objectAtIndex:cellIndex.row - 1] originalTweetID] completionHandler:^(NSArray *tweets) {
                NSIndexPath *cellIndex = [self.mrTimeLineTable indexPathForCell:weakCell];
                
                [self.mrTimelineItems removeObjectAtIndex:cellIndex.row];
                
                if (tweets.count)
                    [self.mrTimelineItems insertObjects:tweets atIndexes:[NSIndexSet indexSetWithIndexesInRange:(NSRange){cellIndex.row, tweets.count}]];
                
                [self.mrTimeLineTable reloadData];
            }];
        }];
        
        return cell;
    }
    
    static NSString *CellIdentifier = @"tweetCell";
    TweetViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
        cell = [[TweetViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    cell.currentTheme = [DataProxy sharedDataProxy].currentTheme;
    
    cell.delegate = self;
    
    if ([tweet.userTwitterName isEqualToString:[[NSString alloc] initWithFormat:@"@%@", self.account.username]])
        cell.showDeleteButton = YES;
    else
        cell.showDeleteButton = NO;
    
    cell.tweetInfo = tweet;
    cell.tweetType = [self tweetTypeForDatasource:[self dataSourceForTabelView:tableView]];
    [cell.userIcon setImageWithURL:tweet.userAvaURL];
    cell.tweetTime.text = [self mrGetTimeStrFromDate:tweet.createdDate];
    cell.username.text = tweet.userName;
    cell.userTwitterName.text = tweet.userTwitterName;
    cell.isFavorited = tweet.isFavorited;
    cell.repliedTweetID = tweet.repliedToTweetID;
    cell.links = tweet.links;
    
    if ([tweet.retwittedByUserTwitterName isEqualToString:self.account.username] || tweet.isRetweeted)
    {
        cell.isRetweeted = YES;
        cell.retweetInfoText.text = NSLocalizedString(@"Retweeted by You", @"Retweeted by You");
    }
    else
    {
        cell.isRetweeted = NO;
        cell.retweetInfoText.text = tweet.retwittedByUser.length ? [[NSString alloc] initWithFormat:@"Retweeted by %@", tweet.retwittedByUser] : @"";
    }
    
    [cell.tweetImage cancelCurrentImageLoad];
    if (tweet.media.count)
    {
        //TODO: place only 1 image - add support more
        TweetMediaInfo *mediaInfo = (TweetMediaInfo *)[tweet.media lastObject];
        
        NSString *thumbAddress;
        if ([mediaInfo.mediaType isEqualToString:@"photo"])
        {
            thumbAddress = [[NSString alloc] initWithFormat:@"%@:thumb", mediaInfo.mediaURL];
            
            [cell.tweetImage setImageWithURL:[[NSURL alloc] initWithString:thumbAddress]
                            placeholderImage:[UIImage imageNamed:@"tweet_image_placeholder.png"]];
            cell.originalImageURL = [[NSURL alloc] initWithString:mediaInfo.mediaURL];
        }
        else if ([mediaInfo.mediaType isEqualToString:@"instagram"])
        {
            cell.tweetImage.image = [UIImage imageNamed:@"tweet_image_placeholder.png"];
            NSURLRequest *request = [NSURLRequest requestWithURL:[[NSURL alloc] initWithString:mediaInfo.mediaURL]
                                                     cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                 timeoutInterval:10.0];
            
            __weak UITableView *weakTableView = tableView;
            __weak TweetMediaInfo *weakMediaInfo = mediaInfo;
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
             {
                 if (error)
                 {
                     NSAssert(NO, error.localizedDescription);
                     return;
                 }
                 
                 NSDictionary *imageData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                 if (error)
                 {
                     NSLog(@"%@", request);
                     //NSAssert(NO, error.localizedDescription);
                     return;
                 }
                 
                 if ([[imageData objectForKey:@"url"] length])
                 {
                     weakMediaInfo.mediaURL = [imageData objectForKey:@"url"];
                     weakMediaInfo.mediaType = @"";
                     
                     TweetViewCell *cellToUpdate = (TweetViewCell *)[weakTableView cellForRowAtIndexPath:indexPath];
                     if (cellToUpdate)
                     {
                         [cellToUpdate.tweetImage setImageWithURL:[[NSURL alloc] initWithString:[imageData objectForKey:@"url"]] placeholderImage:[UIImage imageNamed:@"tweet_image_placeholder.png"]];
                         cellToUpdate.originalImageURL = [[NSURL alloc] initWithString:[imageData objectForKey:@"url"]];
                         [cellToUpdate setNeedsLayout];
                     }
                 }
             }];
        }
        else
        {
            if (mediaInfo.mediaURL.length)
            {
                [cell.tweetImage setImageWithURL:[[NSURL alloc] initWithString:mediaInfo.mediaURL]
                                placeholderImage:[UIImage imageNamed:@"tweet_image_placeholder.png"]];
                cell.originalImageURL = [[NSURL alloc] initWithString:mediaInfo.mediaURL];
            }
        }
    }
    
    CGFloat height = [JSCoreTextView measureFrameHeightForText:tweet.tweetText
                                                      fontName:CELL_TWEET_TEXT_FONT.fontName
                                                      fontSize:CELL_TWEET_TEXT_FONT.pointSize
                                            constrainedToWidth:303
                                                    paddingTop:0
                                                   paddingLeft:0];
    
    CGRect tweetTextFrame = cell.tweetText.frame;
    tweetTextFrame.size.height = ceil(height);
    cell.tweetText.frame = tweetTextFrame;
    
    cell.tweetText.text = tweet.tweetText;
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TweetInfo *tweet = [[self dataSourceForTabelView:tableView] objectAtIndex:indexPath.row];
    
    return [tweet optimalCellSizeForTweet];
}

#pragma mark - UITableView delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView == self.profileView.tweetsTable)
        return 55.0f;
    
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView == self.profileView.tweetsTable)
        return self.profileView.tweetsTitleView;
    
    return [[UIView alloc] init];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
}

#pragma mark - TweetViewCellDelegate methods

- (void)replyToTweet:(NSString *)tweetID tweetType:(TweetType)type
{
    NSMutableArray *tweetsDatasource = [self datasourceForTweetType:type];
    
    NSPredicate *pred = [NSPredicate predicateWithFormat:[[NSString alloc] initWithFormat:@"tweetID like '%@'", tweetID]];
    NSArray *tweets = [tweetsDatasource filteredArrayUsingPredicate:pred];
    NSAssert(tweets.count == 1, @"Somethings wrong with this tweet");
    
    TweetInfo *tweetInfo = [tweets lastObject];
    
    [self replyToTweet:tweetInfo];
}

- (void)checkIfUserHasTwitterAccount:(void(^)(void))successBlock
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        successBlock();
        return;
    }
    
//    SLComposeViewController *mySLComposerSheet = [[SLComposeViewController alloc] init];
//    
//    mySLComposerSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
//    
//    [mySLComposerSheet setInitialText:@""];
//    
//    [mySLComposerSheet addImage:[UIImage imageNamed:@""]];
//    mySLComposerSheet.view.hidden = YES;
//    
//    [self presentViewController:mySLComposerSheet animated:NO completion:^{ }];
}

- (void)loadLatestData
{
    if (!self.account)
        return;
    
    [self mrGetTwDataTimeLine:self.account usePaging:NO];
    [self mrGetTwDataMentions:self.account usePaging:NO];
    [self mrGetTwDataUserTweets:self.account usePaging:NO];
}

- (void)saveLastTweet
{
    NSArray *visibleRows = [self.mrTimeLineTable indexPathsForVisibleRows];
    if (!visibleRows.count || !self.mrTimelineItems.count)
        return;
    
    TweetInfo *tweet = [self.mrTimelineItems objectAtIndex:[[visibleRows objectAtIndex:0] row]];
    
    NSLog(@"tweet.originalTweetID - %@", tweet.originalTweetID);
    
    [[NSUserDefaults standardUserDefaults] setObject:tweet.originalTweetID forKey:kTweetifyLatestTweet];
    [[NSUserDefaults standardUserDefaults] synchronize];

    //save data to tweetmarker
    NSURL *spURL = [NSURL URLWithString:@"https://api.twitter.com/1.1/account/verify_credentials.json"];
    SLRequest *twRequest  = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:TWRequestMethodGET URL:spURL parameters:nil];
    [twRequest setAccount:self.account];
    NSURLRequest *signedURLRequest = [twRequest preparedURLRequest];
    
    // Create API request with OAuth Echo headers.
    NSDictionary *lastTweetData = [[NSDictionary alloc] initWithObjectsAndKeys:tweet.originalTweetID, @"id", nil];
    
    NSString *requestString = [[[NSString alloc] initWithFormat:@"%@/v2/lastread?api_key=%@&username=%@", kTweetifyTweetmarkerURL, kTweetifyTweetmarkerAccessToken, self.account.username]
                               stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[[NSURL alloc] initWithString:requestString]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                       timeoutInterval:kTweetifyRequestTimeout];
    
    NSString *serviceProvider = [[signedURLRequest URL] absoluteString];
    [request setValue:serviceProvider forHTTPHeaderField:@"X-Auth-Service-Provider"];
    
    NSString *authorization = [signedURLRequest valueForHTTPHeaderField:@"Authorization"];
    [request setValue:authorization forHTTPHeaderField:@"X-Verify-Credentials-Authorization"];

    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:[NSJSONSerialization dataWithJSONObject:[[NSDictionary alloc] initWithObjectsAndKeys:lastTweetData, @"timeline", nil] options:NSJSONWritingPrettyPrinted error:nil]];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
             NSLog(@"TweetMarker error - %@", [error localizedDescription]);
     }];
}

-(void)mrGetAccessToTwitterAccounts
{
    // Get access to their accounts
    self.accountStore = [[ACAccountStore alloc] init];
    ACAccountType *accountTypeTwitter = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];

    ACAccountStoreRequestAccessCompletionHandler completionHandler = ^(BOOL granted, NSError *error)
    {
        if (granted && !error)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.accounts = [self.accountStore accountsWithAccountType:accountTypeTwitter];
                
                if (self.accounts.count == 0)
                {
                    [[[UIAlertView alloc] initWithTitle:nil
                                                message:@"Please add a Twitter account in the Settings app"
                                               delegate:nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil] show];
                }
                else
                {
                    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                    BOOL found = NO;
                    
                    if(![[userDefaults objectForKey:@"mr.user.acc.name"] isEqualToString:@""])
                    {
                        for(int i=0; i < self.accounts.count; i++)
                        {
                            ACAccount *oneAccount = [self.accounts objectAtIndex:i];
                            
                            if([oneAccount.username isEqualToString:[userDefaults objectForKey:@"mr.user.acc.name"]])
                            {
                                self.account = [self.accounts objectAtIndex:i];
                                found = YES;
                            }
                        }
                    }
                    
                    if(found)
                    {
                        [self loadUserInformation];
                    }
                    else
                    {
                        // Let them select the account they want to use
                        BlockActionSheet *sheet = [[BlockActionSheet alloc] initWithTitle:@"Select your Twitter account:"];
                        
                        for (ACAccount* account in self.accounts)
                        {
                            [sheet addButtonWithTitle:account.accountDescription block:^{
                                
                                if (![[NSUserDefaults standardUserDefaults] objectForKey:@"tweetify.tutorial.isDisplayed"])
                                    [self showTutorialScreen];
                                else
                                {
                                    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                    [appDelegate showSplashScreen];
                                }
                                
                                [self.mrTimelineItems removeAllObjects];
                                [self.mrMentionsItems removeAllObjects];
                                [self.mrUserTweets removeAllObjects];
                                
                                self.account = account;
                                
                                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                                [userDefaults setObject:self.account.username forKey:@"mr.user.acc.name"];
                                [userDefaults synchronize];
                                
                                [self loadUserInformation];
                            }];
                        }
                        
                        [sheet showInView:self.view];
                    }
                }
            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString* message = [NSString stringWithFormat:@"Error getting access to accounts : %@", [error localizedDescription]];
                [[[UIAlertView alloc] initWithTitle:nil message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            });
        }
    };
    
    [self.accountStore requestAccessToAccountsWithType:accountTypeTwitter options:nil completion:completionHandler];
}

-(void)loadUserInformation
{
    [self.stream stop];
    self.stream = nil;
    
    NSURL *spURL = [NSURL URLWithString:@"https://api.twitter.com/1.1/account/verify_credentials.json"];
    SLRequest *twRequest  = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:TWRequestMethodGET URL:spURL parameters:nil];
    [twRequest setAccount:self.account];
    NSURLRequest *signedURLRequest = [twRequest preparedURLRequest];
    
    // Create API request with OAuth Echo headers.
    NSString *requestString = [[NSString alloc] initWithFormat:@"%@/v2/lastread?api_key=%@&username=%@&collection=timeline", kTweetifyTweetmarkerURL, kTweetifyTweetmarkerAccessToken, self.account.username];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[[NSURL alloc] initWithString:requestString]
                                                           cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
                                                       timeoutInterval:kTweetifyRequestTimeout];
    
    NSString *serviceProvider = [[signedURLRequest URL] absoluteString];
    [request setValue:serviceProvider forHTTPHeaderField:@"X-Auth-Service-Provider"];
    
    NSString *authorization = [signedURLRequest valueForHTTPHeaderField:@"Authorization"];
    [request setValue:authorization forHTTPHeaderField:@"X-Verify-Credentials-Authorization"];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error)
             [self loadTweetsData:[[NSUserDefaults standardUserDefaults] objectForKey:kTweetifyLatestTweet]];
         else
         {
             NSError *parsingError = nil;
             NSDictionary *markerData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&parsingError];
             if (!markerData.count || parsingError)
                 [self loadTweetsData:[[NSUserDefaults standardUserDefaults] objectForKey:kTweetifyLatestTweet]];
             else
             {
                 NSString *latestTweet = [[markerData objectForKey:@"timeline"] objectForKey:@"id"];
                 
                 [[NSUserDefaults standardUserDefaults] setObject:latestTweet forKey:kTweetifyLatestTweet];
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 
                 [self loadTweetsData:latestTweet];
             }
         }
     }];
}

- (void)loadTweetsData:(NSString *)lastTweetID
{
    if (lastTweetID.length)
    {
        [self requestTimelineData:self.account completionHandler:^{
            self.stream = [[TSUserStream alloc] initWithAccount:self.account
                                                    andDelegate:self
                                                  andAllReplies:NO
                                               andAllFollowings:YES];
            if (self.stream)
                [self.stream start];
        }];
    }
    else
        [self mrGetTwDataTimeLine:self.account usePaging:NO];
    
    [self mrGetTwDataMentions:self.account usePaging:NO];
    [self mrGetTwDataUserTweets:self.account usePaging:NO];
    
    [self mrGetUserDataStreem:self.account onSuccess:^{
        if (self.isTimelineDataLoaded)
        {
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            [appDelegate performSelector:@selector(removeSplashScreen) withObject:nil afterDelay:0.5f];
        }
        
        [self loadSuggestionsData];
        
    } onFailure:^{
        [self mrGetAccessToTwitterAccounts];
    }];
    
    [self mrTwitLen:self.account];
    
    if (!lastTweetID.length)
    {
        self.stream = [[TSUserStream alloc] initWithAccount:self.account
                                                andDelegate:self
                                              andAllReplies:NO
                                           andAllFollowings:YES];
        if (self.stream)
            [self.stream start];
    }
}

- (void)loadSuggestionsData
{
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:TWRequestMethodGET URL:[[NSURL alloc] initWithString:@"https://api.twitter.com/1.1/friends/ids.json?cursor=-1&stringify_ids=true"] parameters:nil];
    [request setAccount:self.account];
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
     {
         if (responseData)
         {
             NSError *error;
             NSDictionary *friends = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
             if (error)
             {
                 NSAssert(NO, error.localizedDescription);
             }
             
             if (![self isResponceValid:friends error:error])
                 return;
             
             NSArray *userIDs = [friends objectForKey:@"ids"];
             
             for (NSUInteger i = 0; i <= userIDs.count / 100; i++)
             {
                 NSArray *requestBlock = [userIDs subarrayWithRange:(NSRange){i * 100, MIN(100, userIDs.count - i * 100)}];
                 
                 NSString *ids = [[requestBlock componentsJoinedByString:@","] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                 
                 NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:ids, @"user_id", nil];
                 SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:TWRequestMethodPOST URL:[[NSURL alloc] initWithString:@"https://api.twitter.com/1.1/users/lookup.json"] parameters:params];
                 [request setAccount:self.account];
                 
                 [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
                  {
                      if (responseData)
                      {
                          NSError *error;
                          NSArray *users = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
                          if (error)
                          {
                              NSAssert(NO, error.localizedDescription);
                          }
                          
                          if (![self isResponceValid:users error:error])
                              return;
                          
                          [users enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                              NSDictionary *userInfo = (NSDictionary *)obj;
                              
                              [self.suggestionsDatabase addObject:[userInfo objectForKey:@"screen_name"]];
                          }];
                          
                          NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"description" ascending:YES];
                          NSArray *sortedArray = [self.suggestionsDatabase sortedArrayUsingDescriptors:[NSArray arrayWithObject:sort]];
                          
                          self.suggestionsDatabase = [[NSMutableSet alloc] initWithArray:sortedArray];
                      }
                  }];
             }
         }
     }];
}

#pragma mark - Base methods override

- (void)animateTweetButton:(BOOL)animate
{
    if (animate)
    {
        UIActivityIndicatorViewStyle activityStyle = ([DataProxy sharedDataProxy].currentTheme == AppThemeTypeBlack) ? DARK_NEW_TWEET_BUTTON_LOADING_TYPE : UIActivityIndicatorViewStyleGray;
        UIActivityIndicatorView *progressView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:activityStyle];
        progressView.tag = 1010;
        progressView.hidesWhenStopped = YES;
        progressView.center = CGPointMake(CGRectGetWidth(self.createNewTweetButton.frame) / 2, CGRectGetHeight(self.createNewTweetButton.frame) / 2);
        [progressView startAnimating];
        
        [self.createNewTweetButton setImage:nil forState:UIControlStateNormal];
        [self.createNewTweetButton addSubview:progressView];
    }
    else
    {
        UIImage *composerImage = ([DataProxy sharedDataProxy].currentTheme == AppThemeTypeBlack) ? DARK_NEW_TWEET_BUTTON_COMPOSE_IMAGE : LIGHT_NEW_TWEET_BUTTON_COMPOSE_IMAGE;
        [[self.createNewTweetButton viewWithTag:1010] removeFromSuperview];
        [self.createNewTweetButton setImage:composerImage forState:UIControlStateNormal];
    }
}

- (void)retweet:(TweetInfo *)tweetInfo tweetType:(TweetType)tweetType response:(NSDictionary *)response
{
    //    NSMutableArray *datasource = [self datasourceForTweetType:tweetType];
    //
    //    if (![datasource containsObject:tweetInfo])
    //        return;
    //
    //    NSUInteger index = [datasource indexOfObject:tweetInfo];
    //    UITableView *tableView = [self tableViewForDatasource:datasource];
    //    TweetInfo *receivedTweet = [[JSONDataParser sharedJSONParser] parseTweetInfo:response];
    //
    //    [tableView beginUpdates];
    //    TweetViewCell *cellToUpdate = (TweetViewCell *)[tableView cellForRowAtIndexPath:
    //                                                    [NSIndexPath indexPathForRow:index inSection:0]];
    //
    //    tweetInfo.isRetweeted = YES;
    //    tweetInfo.retweetedTweetID = receivedTweet.originalTweetID;
    //
    //    cellToUpdate.retweetInfoText.text = @"Retweeted by You";
    //
    //    [tableView endUpdates];
}

- (void)unretweet:(TweetInfo *)tweetInfo tweetType:(TweetType)tweetType response:(NSDictionary *)response
{
    NSMutableArray *datasource = [self datasourceForTweetType:tweetType];
    
    if (![datasource containsObject:tweetInfo])
        return;
    
    NSUInteger index = [datasource indexOfObject:tweetInfo];
    UITableView *tableView = [self tableViewForDatasource:datasource];
    
    [tableView beginUpdates];
    TweetViewCell *cellToUpdate = (TweetViewCell *)[tableView cellForRowAtIndexPath:
                                                    [NSIndexPath indexPathForRow:index inSection:0]];
    
    tweetInfo.isRetweeted = NO;
    tweetInfo.retweetedTweetID = nil;
    
    cellToUpdate.retweetInfoText.text = tweetInfo.retwittedByUser.length ? [[NSString alloc] initWithFormat:@"Retweeted by %@", tweetInfo.retwittedByUser] : @"";
    
    [tableView endUpdates];
}

#pragma mark - REST API

-(void)mrTwitLen:(ACAccount*)account{
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.twitter.com/1/help/configuration.json"]];
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:TWRequestMethodGET URL:url parameters:nil];
    [request setAccount:account];
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
     {
         if (responseData)
         {
             NSError *error;
             NSDictionary *mrConfig = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
             
             if (![self isResponceValid:mrConfig error:error])
                 return;
             
             self.tweetImageURLStringLength = [[mrConfig objectForKey:@"short_url_length_https"] integerValue];
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
             });
         }
     }];
}

- (void)mrGetUserDataStreem:(ACAccount *)account
                  onSuccess:(void(^)(void))successBlock
                  onFailure:(void(^)(void))failureBlock
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    self.isUserDataLoaded = NO;
    NSURL *accountInfoURL = [[NSURL alloc] initWithString:[[NSString alloc] initWithFormat:@"https://api.twitter.com/1.1/users/show.json?screen_name=%@", self.account.username]];
    
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:TWRequestMethodGET URL:accountInfoURL parameters:nil];
    [request setAccount:account];
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
     {
         if (responseData)
         {
             NSError *error;
             NSDictionary *mrUserInfo = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
             if (error)
             {
                 NSAssert(NO, error.localizedDescription);
             }
             
             if (![self isResponceValid:mrUserInfo error:error])
                 return;
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 self.profileView.username.text = [mrUserInfo objectForKey:@"name"];
                 self.profileView.userTwitterName = [mrUserInfo objectForKey:@"screen_name"];
                 self.profileView.followCount.text = [NSString stringWithFormat:@"%@", [mrUserInfo objectForKey:@"friends_count"]];
                 self.profileView.followersCount.text = [NSString stringWithFormat:@"%@", [mrUserInfo objectForKey:@"followers_count"]];
                 self.profileView.tweetsCount.text = [NSString stringWithFormat:@"%@", [mrUserInfo objectForKey:@"statuses_count"]];
                 [self.profileView.userIcon setImageWithURL:[NSURL URLWithString:[[mrUserInfo objectForKey:@"profile_image_url"] stringByReplacingOccurrencesOfString:@"_normal" withString:@""]]
                                           placeholderImage:[UIImage imageNamed:@"profile_icon_placeholder.png"]];
                 
                 [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                 
                 if (successBlock)
                     successBlock();
             });
         }
         else
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 if (failureBlock)
                     failureBlock();
             });
         }
         self.isUserDataLoaded = YES;
     }];
    
    
    NSURL *profileBannerURL = [[NSURL alloc] initWithString:[[NSString alloc] initWithFormat:@"https://api.twitter.com/1.1/users/profile_banner.json?screen_name=%@", self.account.username]];
    SLRequest *request2 = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:TWRequestMethodGET URL:profileBannerURL parameters:nil];
    [request2 setAccount:account];
    [request2 performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
     {
         if (responseData)
         {
             NSError *error;
             NSDictionary *mrUserInfo = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
             if (error)
             {
                 NSAssert(NO, error.localizedDescription);
                 return;
             }
             
             NSString *backgroundImageAddress = [[[mrUserInfo objectForKey:@"sizes"] objectForKey:@"mobile_retina"] objectForKey:@"url"];
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 if (backgroundImageAddress)
                 {
                     [self.profileView.profileBackground setImageWithURL:[[NSURL alloc] initWithString:backgroundImageAddress]
                                                        placeholderImage:[UIImage imageNamed:@"profile_banner_placeholder.png"]];
                 }
                 else
                 {
                     self.profileView.profileBackground.image = [UIImage imageNamed:@"profile_banner_placeholder.png"];
                 }
             });
         }
     }];
}

- (void)requestTimelineData:(ACAccount *)account completionHandler:(void(^)(void))finished
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    self.isTimelineDataLoaded = NO;
    
    NSString *requestAddress = [[NSString alloc] initWithFormat:@"https://api.twitter.com/1.1/statuses/home_timeline.json?count=20&max_id=%@",
                                [[NSUserDefaults standardUserDefaults] objectForKey:kTweetifyLatestTweet]];
    
    NSURL *url = [[NSURL alloc] initWithString:requestAddress];
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:TWRequestMethodGET URL:url parameters:nil];
    [request setAccount:account];
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
     {
         if (responseData)
         {
             NSError *error;
             NSArray *timelineItems = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
             if (error)
             {
                 NSAssert(NO, error.localizedDescription);
             }
             
             if (![self isResponceValid:timelineItems error:error])
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                     
                     finished();
                     
                     if (self.isUserDataLoaded)
                     {
                         AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                         [appDelegate performSelector:@selector(removeSplashScreen) withObject:nil afterDelay:0.5f];
                     }
                     
                     self.isTimelineDataLoaded = YES;
                 });
                 
                 return;
             }
             
             NSMutableArray *parsedResult = [[JSONDataParser sharedJSONParser] parseTweetsFeed:timelineItems];
             NSLog(@"parsedResult  - %d", parsedResult.count);
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 if (parsedResult.count)
                 {
                     self.mrTimelineItems = parsedResult;
                     
                     [self.mrTimeLineTable reloadData];
                     
                     if (![self.timelineBottomPullView isDescendantOfView:self.mrTimeLineTable])
                         [self.mrTimeLineTable addSubview:self.timelineBottomPullView];
                 }
                 
                 NSUInteger storedObjectsCount = self.mrTimelineItems.count;
                 
                 //load timeline
                 NSString *requestAddress = nil;
//                 if (self.mrTimelineItems.count)
//                     requestAddress = [[NSString alloc] initWithFormat:@"https://api.twitter.com/1.1/statuses/home_timeline.json?count=190&since_id=%@", [[self.mrTimelineItems objectAtIndex:0] originalTweetID]];
//                 else
                     requestAddress = @"https://api.twitter.com/1.1/statuses/home_timeline.json?count=190";
                 
                 NSURL *url = [[NSURL alloc] initWithString:requestAddress];
                 SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:TWRequestMethodGET URL:url parameters:nil];
                 [request setAccount:account];
                 [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
                  {
                      if (responseData)
                      {
                          NSError *error;
                          NSArray *timelineItems = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
                          if (error)
                          {
                              NSAssert(NO, error.localizedDescription);
                          }
                          
                          if (![self isResponceValid:timelineItems error:error])
                          {
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                                  
                                  finished();
                                  
                                  if (self.isUserDataLoaded)
                                  {
                                      AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                      [appDelegate performSelector:@selector(removeSplashScreen) withObject:nil afterDelay:0.5f];
                                  }
                                  
                                  self.isTimelineDataLoaded = YES;
                              });
                              
                              return;
                          }
                          
                          NSMutableArray *parsedResult = [[JSONDataParser sharedJSONParser] parseTweetsFeed:timelineItems];
                          
                          dispatch_async(dispatch_get_main_queue(), ^{
                              
                              if (parsedResult.count)
                              {
                                  BOOL shouldAddMoreButton = YES;
                                  
                                  if ([parsedResult containsObject:[self.mrTimelineItems objectAtIndex:0]])
                                  {
                                      shouldAddMoreButton = NO;
                                      NSUInteger index = [parsedResult indexOfObject:[self.mrTimelineItems objectAtIndex:0]];
                                      [parsedResult removeObjectsInRange:(NSRange){index, parsedResult.count - index}];
                                  }
                                  
                                  if (shouldAddMoreButton)
                                  {
                                      TweetInfo *stub = [[TweetInfo alloc] init];
                                      stub.tweetID = @"STUB";
                                      [self.mrTimelineItems insertObject:stub atIndex:0];
                                  }
                                  
//                                  NSMutableSet *receivedTweetsSet = [[NSMutableSet alloc] initWithArray:self.mrTimelineItems];
//                                  NSSet *storedTweetsSet = [[NSSet alloc] initWithArray:parsedResult];
//                                  
//                                  [receivedTweetsSet intersectSet:storedTweetsSet];
//                                  if (receivedTweetsSet.count)
//                                      [parsedResult removeObjectsInArray:receivedTweetsSet.allObjects];
//                                  else
//                                  {
//                                      //add button
//
//                                  }
//                                  
                                  [parsedResult addObjectsFromArray:self.mrTimelineItems];
                                  self.mrTimelineItems = parsedResult;

                                  [self.mrTimeLineTable reloadData];
                                  
                                  NSUInteger rowIndex = MAX(0, self.mrTimelineItems.count - (storedObjectsCount ? storedObjectsCount : self.mrTimelineItems.count));
                                  [self.mrTimeLineTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:rowIndex inSection:0]
                                                              atScrollPosition:UITableViewScrollPositionTop animated:NO];
                                  
                                  if (rowIndex)
                                      self.badgeView.text = [[NSString alloc] initWithFormat:@"%d", rowIndex];
                                  
                                  if (![self.timelineBottomPullView isDescendantOfView:self.mrTimeLineTable])
                                      [self.mrTimeLineTable addSubview:self.timelineBottomPullView];
                              }
                              
                              [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                              
                              finished();
                              
                              if (self.isUserDataLoaded)
                              {
                                  AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                                  [appDelegate performSelector:@selector(removeSplashScreen) withObject:nil afterDelay:0.5f];
                              }
                              
                              self.isTimelineDataLoaded = YES;
                          });
                      }
                  }];
             });
         }
     }];
}

- (void)loadMoreTweetsSinceID:(NSString *)tweetID maxTweetID:(NSString *)maxID completionHandler:(void(^)(NSArray *tweets))finished
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSString *requestAddress = [[NSString alloc] initWithFormat:@"https://api.twitter.com/1.1/statuses/home_timeline.json?count=190&since_id=%@&max_id=%@", tweetID, maxID];
    
    NSURL *url = [[NSURL alloc] initWithString:requestAddress];
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:TWRequestMethodGET URL:url parameters:nil];
    [request setAccount:self.account];
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
     {
         if (responseData)
         {
             NSError *error;
             NSArray *timelineItems = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
             if (error)
             {
                 NSAssert(NO, error.localizedDescription);
             }
             
             if (![self isResponceValid:timelineItems error:error])
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     finished(nil);
                 });
                 return;
             }
             
             NSMutableArray *parsedResult = [[JSONDataParser sharedJSONParser] parseTweetsFeed:timelineItems];
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 NSMutableSet *localTweetsSet = [[NSMutableSet alloc] initWithArray:self.mrTimelineItems];
                 NSSet *receivedTweetsSet = [[NSSet alloc] initWithArray:parsedResult];
                 
                 [localTweetsSet intersectSet:receivedTweetsSet];
                 
                 if (localTweetsSet.count)
                     [parsedResult removeObjectsInArray:localTweetsSet.allObjects];
                 
                 finished(parsedResult);
                 
                 [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
             });
         }
     }];
}

-(void) mrGetTwDataTimeLine:(ACAccount *)account usePaging:(BOOL)paging
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    self.isTimelineDataLoaded = NO;
    
    NSString *requestAddress;
    if (paging)
    {
        TweetInfo *oldestTweet = [self.mrTimelineItems lastObject];
        requestAddress = [[NSString alloc] initWithFormat:@"https://api.twitter.com/1.1/statuses/home_timeline.json?count=50&max_id=%@", oldestTweet.originalTweetID];
    }
    else if (self.mrTimelineItems.count)
    {
        //try to get new data
        TweetInfo *latestTweet = [self.mrTimelineItems objectAtIndex:0];
        requestAddress = [[NSString alloc] initWithFormat:@"https://api.twitter.com/1.1/statuses/home_timeline.json?count=190&since_id=%@", latestTweet.originalTweetID];
    }
    else
        requestAddress = @"https://api.twitter.com/1.1/statuses/home_timeline.json?count=100";
    
    NSURL *url = [[NSURL alloc] initWithString:requestAddress];
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:TWRequestMethodGET URL:url parameters:nil];
    [request setAccount:account];
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
     {
         if (responseData)
         {
             NSError *error;
             NSArray *timelineItems = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
             if (error)
             {
                 NSAssert(NO, error.localizedDescription);
             }
             
             if (![self isResponceValid:timelineItems error:error])
                 return;
             
             NSMutableArray *parsedResult = [[JSONDataParser sharedJSONParser] parseTweetsFeed:timelineItems];
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 TweetInfo *latestTweet;
                 
                 if (paging)
                     latestTweet = self.mrTimelineItems.count ? [self.mrTimelineItems lastObject] : nil;
                 else
                     latestTweet = self.mrTimelineItems.count ? [self.mrTimelineItems objectAtIndex:0] : nil;
                 
                 if (latestTweet)
                     [parsedResult removeObject:latestTweet];
                 
                 BOOL needTableReload = YES;
                 if (parsedResult.count)
                 {
                     if (paging)
                     {
                         [self.mrTimelineItems addObjectsFromArray:parsedResult];
                         [self.mrTimeLineTable reloadData];
                         [self.timelineBottomPullView finishedLoading];
                         return;
                     }
                     
                     if (self.mrTimelineItems.count)
                     {
                         __block CGFloat totalHeight = 0;
                         [parsedResult enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                             totalHeight += [obj optimalCellSizeForTweet];
                         }];
                         
                         [parsedResult addObjectsFromArray:self.mrTimelineItems];
                         self.mrTimelineItems = parsedResult;
                         
                         [self safelyAddNewTweets:parsedResult.count tweetsHeight:totalHeight toTable:self.mrTimeLineTable];
                         needTableReload = NO;
                     }
                     else
                         self.mrTimelineItems = parsedResult;
                     
                     if (needTableReload)
                         [self.mrTimeLineTable reloadData];
                     
                     if (![self.timelineBottomPullView isDescendantOfView:self.mrTimeLineTable])
                         [self.mrTimeLineTable addSubview:self.timelineBottomPullView];
                 }
                 
                 [self.timelineBottomPullView finishedLoading];
                 [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                 
                 if (self.isUserDataLoaded)
                 {
                     AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
                     [appDelegate performSelector:@selector(removeSplashScreen) withObject:nil afterDelay:0.5f];
                 }
             });
             self.isTimelineDataLoaded = YES;
         }
     }];
}

- (void)mrGetTwDataMentions:(ACAccount *)account usePaging:(BOOL)paging
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSString *requestAddress;
    if (paging)
    {
        TweetInfo *oldestTweet = [self.mrMentionsItems lastObject];
        requestAddress = [[NSString alloc] initWithFormat:@"https://api.twitter.com/1.1/statuses/mentions_timeline.json?count=50&max_id=%@", oldestTweet.originalTweetID];
    }
    else if (self.mrMentionsItems.count)
    {
        //try to get new data
        TweetInfo *latestTweet = [self.mrMentionsItems objectAtIndex:0];
        requestAddress = [[NSString alloc] initWithFormat:@"https://api.twitter.com/1.1/statuses/mentions_timeline.json?count=190&since_id=%@", latestTweet.originalTweetID];
    }
    else
        requestAddress = @"https://api.twitter.com/1.1/statuses/mentions_timeline.json?count=100";
    
    NSURL *url = [[NSURL alloc] initWithString:requestAddress];
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:TWRequestMethodGET URL:url parameters:nil];
    [request setAccount:account];
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
     {
         if (responseData)
         {
             NSError *error;
             NSArray *mentionsItems = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
             if (error)
             {
                 NSAssert(NO, error.localizedDescription);
             }
             
             if (![self isResponceValid:mentionsItems error:error])
                 return;
             
             NSMutableArray *parsedResult = [[JSONDataParser sharedJSONParser] parseTweetsFeed:mentionsItems];
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 TweetInfo *latestTweet;
                 
                 if (paging)
                     latestTweet = self.mrMentionsItems.count ? [self.mrMentionsItems lastObject] : nil;
                 else
                     latestTweet = self.mrMentionsItems.count ? [self.mrMentionsItems objectAtIndex:0] : nil;
                 
                 if (latestTweet)
                     [parsedResult removeObject:latestTweet];
                 
                 BOOL needTableReload = YES;
                 if (parsedResult.count)
                 {
                     if (paging)
                     {
                         [self.mrMentionsItems addObjectsFromArray:parsedResult];
                         [self.mrMentionsTable reloadData];
                         [self.mentionsBottomPullView finishedLoading];
                         return;
                     }
                     
                     if (self.mrMentionsItems.count)
                     {
                         __block CGFloat totalHeight = 0;
                         [parsedResult enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                             totalHeight += [obj optimalCellSizeForTweet];
                         }];
                         
                         [parsedResult addObjectsFromArray:self.mrMentionsItems];
                         self.mrMentionsItems = parsedResult;
                         
                         [self safelyAddNewTweets:parsedResult.count tweetsHeight:totalHeight toTable:self.mrMentionsTable];
                         needTableReload = NO;
                     }
                     else
                         self.mrMentionsItems = parsedResult;
                     
                     if (needTableReload)
                         [self.mrMentionsTable reloadData];
                     
                     if (![self.mentionsBottomPullView isDescendantOfView:self.mrMentionsTable])
                         [self.mrMentionsTable addSubview:self.mentionsBottomPullView];
                 }
                 
                 [self.mentionsBottomPullView finishedLoading];
                 [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
             });
         }
     }];
}

- (void)mrGetTwDataUserTweets:(ACAccount *)account usePaging:(BOOL)paging
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSString *requestAddress;
    if (paging)
    {
        TweetInfo *oldestTweet = [self.mrUserTweets lastObject];
        requestAddress = [[NSString alloc] initWithFormat:@"https://api.twitter.com/1.1/statuses/user_timeline.json?count=50&max_id=%@", oldestTweet.originalTweetID];
    }
    else if (self.mrUserTweets.count)
    {
        //try to get new data
        TweetInfo *latestTweet = [self.mrUserTweets objectAtIndex:0];
        requestAddress = [[NSString alloc] initWithFormat:@"https://api.twitter.com/1.1/statuses/user_timeline.json?count=190&since_id=%@", latestTweet.originalTweetID];
    }
    else
        requestAddress = @"https://api.twitter.com/1.1/statuses/user_timeline.json?count=100";
    
    NSURL *url = [[NSURL alloc] initWithString:requestAddress];
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:TWRequestMethodGET URL:url parameters:nil];
    [request setAccount:account];
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
     {
         if (responseData)
         {
             NSError *error;
             NSArray *userTweetsItems = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
             if (error)
             {
                 NSAssert(NO, error.localizedDescription);
             }
             
             if (![self isResponceValid:userTweetsItems error:error])
                 return;
             
             NSMutableArray *parsedResult = [[JSONDataParser sharedJSONParser] parseTweetsFeed:userTweetsItems];
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 TweetInfo *latestTweet;
                 
                 if (paging)
                     latestTweet = self.mrUserTweets.count ? [self.mrUserTweets lastObject] : nil;
                 else
                     latestTweet = self.mrUserTweets.count ? [self.mrUserTweets objectAtIndex:0] : nil;
                 
                 if (latestTweet)
                     [parsedResult removeObject:latestTweet];
                 
                 BOOL needTableReload = YES;
                 if (parsedResult.count)
                 {
                     if (paging)
                     {
                         [self.mrUserTweets addObjectsFromArray:parsedResult];
                         [self.profileView.tweetsTable reloadData];
                         [self.profileBottomPullView finishedLoading];
                         return;
                     }
                     
                     if (self.mrUserTweets.count)
                     {
                         __block CGFloat totalHeight = 0;
                         [parsedResult enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
                             totalHeight += [obj optimalCellSizeForTweet];
                         }];
                         
                         [parsedResult addObjectsFromArray:self.mrUserTweets];
                         self.mrUserTweets = parsedResult;
                         
                         [self safelyAddNewTweets:parsedResult.count tweetsHeight:totalHeight toTable:self.profileView.tweetsTable];
                         needTableReload = NO;
                     }
                     else
                         self.mrUserTweets = parsedResult;
                     
                     if (needTableReload)
                         [self.profileView.tweetsTable reloadData];
                     
                     if (![self.profileBottomPullView isDescendantOfView:self.profileView.tweetsTable])
                         [self.profileView.tweetsTable addSubview:self.profileBottomPullView];
                 }
                 
                 [self.profileBottomPullView finishedLoading];
                 [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
             });
         }
     }];
}

- (void)updateTweetFavoriteStatus:(TweetInfo *)tweet
{
    if ([self.mrTimelineItems containsObject:tweet])
    {
        //update local tweet
        [[self.mrTimelineItems objectAtIndex:[self.mrTimelineItems indexOfObject:tweet]] setIsFavorited:tweet.isFavorited];
        dispatch_async(dispatch_get_main_queue(), ^{
            NSUInteger index = [self.mrTimelineItems indexOfObject:tweet];
            TweetInfo *tweetToUpdate = [self.mrTimelineItems objectAtIndex:index];
            
            [self.mrTimeLineTable beginUpdates];
            TweetViewCell *cellToUpdate = (TweetViewCell *)[self.mrTimeLineTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
            
            tweetToUpdate.isFavorited = tweet.isFavorited;
            tweetToUpdate.favouritedTweetID = tweet.originalTweetID;
            
            cellToUpdate.isFavorited = tweet.isFavorited;
            
            [self.mrTimeLineTable endUpdates];
        });
    }
    
    if ([self.mrMentionsItems containsObject:tweet])
    {
        //update local tweet
        [[self.mrMentionsItems objectAtIndex:[self.mrMentionsItems indexOfObject:tweet]] setIsFavorited:tweet.isFavorited];
        dispatch_async(dispatch_get_main_queue(), ^{
            NSUInteger index = [self.mrMentionsItems indexOfObject:tweet];
            TweetInfo *tweetToUpdate = [self.mrMentionsItems objectAtIndex:index];
            
            [self.mrMentionsTable beginUpdates];
            TweetViewCell *cellToUpdate = (TweetViewCell *)[self.mrMentionsTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
            
            tweetToUpdate.isFavorited = tweet.isFavorited;
            tweetToUpdate.favouritedTweetID = tweet.originalTweetID;
            
            cellToUpdate.isFavorited = tweet.isFavorited;
            
            [self.mrMentionsTable endUpdates];
        });
    }
    
    if ([self.mrUserTweets containsObject:tweet])
    {
        //update local tweet
        [[self.mrUserTweets objectAtIndex:[self.mrUserTweets indexOfObject:tweet]] setIsFavorited:tweet.isFavorited];
        dispatch_async(dispatch_get_main_queue(), ^{
            NSUInteger index = [self.mrUserTweets indexOfObject:tweet];
            TweetInfo *tweetToUpdate = [self.mrUserTweets objectAtIndex:index];
            
            [self.profileView.tweetsTable beginUpdates];
            TweetViewCell *cellToUpdate = (TweetViewCell *)[self.profileView.tweetsTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
            
            tweetToUpdate.isFavorited = tweet.isFavorited;
            tweetToUpdate.favouritedTweetID = tweet.originalTweetID;
            
            cellToUpdate.isFavorited = tweet.isFavorited;
            
            [self.profileView.tweetsTable endUpdates];
        });
    }
}

- (void)safelyAddNewTweets:(NSUInteger)tweetsCount tweetsHeight:(CGFloat)height toTable:(UITableView *)table
{
    if (!table.contentOffset.y)
    {
        [table reloadData];
    }
    else
    {
        if (table == self.mrTimeLineTable)
        {
            if (self.badgeView.text.length)
                self.badgeView.text = [[NSString alloc] initWithFormat:@"%d", [self.numberFormatter numberFromString:self.badgeView.text].integerValue + tweetsCount];
            else
                self.badgeView.text = [[NSString alloc] initWithFormat:@"%d", tweetsCount];
        }
        else if (table == self.mrMentionsTable)
        {
            if (self.mentionsBadgeView.text.length)
                self.mentionsBadgeView.text = [[NSString alloc] initWithFormat:@"%d", [self.numberFormatter numberFromString:self.mentionsBadgeView.text].integerValue + tweetsCount];
            else
                self.mentionsBadgeView.text = [[NSString alloc] initWithFormat:@"%d", tweetsCount];
        }
        
        CGPoint offset = table.contentOffset;
        
        NSIndexPath *selectedRow = [table indexPathForSelectedRow];
        
        if (selectedRow)
        {
            TweetViewCell *selectedCell = (TweetViewCell *)[table cellForRowAtIndexPath:selectedRow];
            selectedCell.silenceUpdate = YES;
        }
        
        [table reloadData];
        
        offset.y += height;
        
        if (offset.y > table.contentSize.height)
            offset.y = 0;
        
        table.contentOffset = offset;
        
        if (selectedRow)
        {
            NSIndexPath *updatedIndex = [NSIndexPath indexPathForRow:selectedRow.row + tweetsCount inSection:selectedRow.section];
            TweetViewCell *selectedCell = (TweetViewCell *)[table cellForRowAtIndexPath:updatedIndex];
            selectedCell.silenceUpdate = YES;
            [table selectRowAtIndexPath:updatedIndex animated:NO scrollPosition:UITableViewScrollPositionNone];
        }
    }
}

- (void)safelyAddingNewTweetToTable:(UITableView *)table
{
    if (!table.contentOffset.y)
    {
        [table beginUpdates];
        [table insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:0 inSection:0]]
                     withRowAnimation:UITableViewRowAnimationTop];
        [table endUpdates];
    }
    else
    {
        if (table == self.mrTimeLineTable)
        {
            if (self.badgeView.text.length)
                self.badgeView.text = [[NSString alloc] initWithFormat:@"%d", [self.numberFormatter numberFromString:self.badgeView.text].integerValue + 1];
            else
                self.badgeView.text = @"1";
        }
        else if (table == self.mrMentionsTable)
        {
            if (self.mentionsBadgeView.text.length)
                self.mentionsBadgeView.text = [[NSString alloc] initWithFormat:@"%d", [self.numberFormatter numberFromString:self.mentionsBadgeView.text].integerValue + 1];
            else
                self.mentionsBadgeView.text = @"1";
        }
        
        CGPoint offset = table.contentOffset;
        
        NSIndexPath *selectedRow = [table indexPathForSelectedRow];
        
        if (selectedRow)
        {
            TweetViewCell *selectedCell = (TweetViewCell *)[table cellForRowAtIndexPath:selectedRow];
            selectedCell.silenceUpdate = YES;
        }
        
        [table reloadData];
        
        offset.y += [self tableView:table heightForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        if (offset.y > table.contentSize.height)
            offset.y = 0;
        
        table.contentOffset = offset;
        
        if (selectedRow)
        {
            NSIndexPath *updatedIndex = [NSIndexPath indexPathForRow:selectedRow.row + 1 inSection:selectedRow.section];
            TweetViewCell *selectedCell = (TweetViewCell *)[table cellForRowAtIndexPath:updatedIndex];
            selectedCell.silenceUpdate = YES;
            [table selectRowAtIndexPath:updatedIndex animated:NO scrollPosition:UITableViewScrollPositionNone];
        }
    }
}

#pragma mark - TSStreamDelegate

- (void)streamDidReceiveMessage:(TSStream*)stream json:(id)json {
    [TSModelParser parseJson:json
                     friends:^(TSFriendsList *model) {
                         //NSLog(@"friends --- %@",[model friendsIds]);
                     } tweet:^(TSTweet *model) {
                         
                         dispatch_async(dispatch_get_main_queue(), ^{
                             
                             if (!self.mrTimelineItems)
                                 return;
                             
                             TweetInfo *tweet = [[TweetInfo alloc] initWithTweet:model];
                             
                             if ([tweet.retwittedByUserTwitterName isEqualToString:self.account.username])
                             {
                                 if (![self.mrUserTweets containsObject:tweet])
                                 {
                                     [self.mrUserTweets insertObject:tweet atIndex:0];
                                     [self safelyAddingNewTweetToTable:self.profileView.tweetsTable];
                                     
                                 }
                                 
                                 self.profileView.tweetsCount.text = [[NSString alloc] initWithFormat:@"%d",
                                                                      self.profileView.tweetsCount.text.integerValue + 1];
                                 
                                 //this is user retweet
                                 if ([self.mrTimelineItems containsObject:tweet])
                                 {
                                     NSUInteger index = [self.mrTimelineItems indexOfObject:tweet];
                                     TweetInfo *tweetToUpdate = [self.mrTimelineItems objectAtIndex:index];
                                     
                                     [self.mrTimeLineTable beginUpdates];
                                     TweetViewCell *cellToUpdate = (TweetViewCell *)[self.mrTimeLineTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
                                     
                                     tweetToUpdate.isRetweeted = YES;
                                     tweetToUpdate.retweetedTweetID = tweet.originalTweetID;
                                     
                                     cellToUpdate.isRetweeted = YES;
                                     cellToUpdate.retweetInfoText.text = @"Retweeted by You";
                                     
                                     [self.mrTimeLineTable endUpdates];
                                     
                                     return;
                                 }
                                 
                                 if ([self.mrMentionsItems containsObject:tweet])
                                 {
                                     NSUInteger index = [self.mrMentionsItems indexOfObject:tweet];
                                     TweetInfo *tweetToUpdate = [self.mrMentionsItems objectAtIndex:index];
                                     
                                     [self.mrMentionsTable beginUpdates];
                                     TweetViewCell *cellToUpdate = (TweetViewCell *)[self.mrMentionsTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
                                     
                                     tweetToUpdate.isRetweeted = YES;
                                     tweetToUpdate.retweetedTweetID = tweet.originalTweetID;
                                     
                                     cellToUpdate.isRetweeted = YES;
                                     cellToUpdate.retweetInfoText.text = @"Retweeted by You";
                                     
                                     [self.mrMentionsTable endUpdates];
                                     
                                     return;
                                 }
                                 
                                 if ([self.mrUserTweets containsObject:tweet])
                                 {
                                     
                                 }
                                 return;
                             }
                             
                             if (![self.mrTimelineItems containsObject:tweet])
                             {
                                 [self.mrTimelineItems insertObject:tweet atIndex:0];
                                 [self safelyAddingNewTweetToTable:self.mrTimeLineTable];
                             }
                             
                             if([tweet.userTwitterName isEqualToString:[[NSString alloc] initWithFormat:@"@%@", self.account.username]])
                             {
                                 if (![self.mrUserTweets containsObject:tweet])
                                 {
                                     [self.mrUserTweets insertObject:tweet atIndex:0];
                                     [self safelyAddingNewTweetToTable:self.profileView.tweetsTable];
                                     
                                 }
                                 
                                 self.profileView.tweetsCount.text = [[NSString alloc] initWithFormat:@"%d",
                                                                      self.profileView.tweetsCount.text.integerValue + 1];
                             }
                             
                             NSArray *tweetMentionArray = [model userMentions];
                             
                             if(tweetMentionArray.count)
                             {
                                 BOOL findMention = NO;
                                 
                                 for (int i=0; i<tweetMentionArray.count; i++)
                                 {
                                     TSUser *mentionUser = [[model userMentions] objectAtIndex:i];
                                     
                                     if ([[mentionUser screenName] isEqualToString:self.account.username])
                                         findMention = YES;
                                 }
                                 
                                 if (findMention)
                                 {
                                     if (![self.mrMentionsItems containsObject:tweet])
                                     {
                                         [self.mrMentionsItems insertObject:tweet atIndex:0];
                                         [self safelyAddingNewTweetToTable:self.mrMentionsTable];
                                     }
                                 }
                             }
                         });
                         
                     } deleteTweet:^(TSTweet *model) {
                         
                         CGFloat pageWidth = self.scroll.frame.size.width;
                         int page = floor((self.scroll.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
                         
                         for(int i=0; i<self.mrTimelineItems.count; i++)
                         {
                             if([[[self.mrTimelineItems objectAtIndex:i] originalTweetID] isEqualToString:model.deleteTwID] ||
                                [[[self.mrTimelineItems objectAtIndex:i] retweetedTweetID] isEqualToString:model.deleteTwID])
                             {
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     
                                     TweetInfo *tweet = [self.mrTimelineItems objectAtIndex:i];
                                     if ([tweet.userTwitterName isEqualToString:[[NSString alloc] initWithFormat:@"@%@", self.account.username]])
                                     {
                                         //this is user tweet - remove it
                                         [self.mrTimelineItems removeObject:tweet];
                                         
                                         if (page == 0)
                                         {
                                             [self.mrTimeLineTable beginUpdates];
                                             [self.mrTimeLineTable deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
                                             [self.mrTimeLineTable endUpdates];
                                         }
                                     }
                                     else
                                     {
                                         tweet.isRetweeted = NO;
                                         if (page == 0)
                                         {
                                             [self.mrTimeLineTable beginUpdates];
                                             [self.mrTimeLineTable reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
                                             [self.mrTimeLineTable endUpdates];
                                         }
                                         else
                                             [self.mrTimeLineTable reloadData];
                                     }
                                 });
                             }
                         }
                         
                         for(int i=0; i<self.mrMentionsItems.count; i++)
                         {
                             if ([[[self.mrMentionsItems objectAtIndex:i] originalTweetID] isEqualToString:model.deleteTwID] ||
                                 [[[self.mrMentionsItems objectAtIndex:i] retweetedTweetID] isEqualToString:model.deleteTwID])
                             {
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     
                                     TweetInfo *tweet = [self.mrMentionsItems objectAtIndex:i];
                                     if ([tweet.userTwitterName isEqualToString:[[NSString alloc] initWithFormat:@"@%@", self.account.username]])
                                     {
                                         //this is user tweet - remove it
                                         [self.mrMentionsItems removeObject:tweet];
                                         
                                         if (page == 1)
                                         {
                                             [self.mrMentionsTable beginUpdates];
                                             [self.mrMentionsTable deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
                                             [self.mrMentionsTable endUpdates];
                                         }
                                         else
                                             [self.mrMentionsTable reloadData];
                                     }
                                     else
                                     {
                                         tweet.isRetweeted = NO;
                                         if (page == 1)
                                         {
                                             [self.mrMentionsTable beginUpdates];
                                             [self.mrMentionsTable reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
                                             [self.mrMentionsTable endUpdates];
                                         }
                                         else
                                             [self.mrMentionsTable reloadData];
                                     }
                                 });
                             }
                         }
                         
                         for(int i=0; i<self.mrUserTweets.count; i++)
                         {
                             if ([[[self.mrUserTweets objectAtIndex:i] originalTweetID] isEqualToString:model.deleteTwID] ||
                                 [[[self.mrUserTweets objectAtIndex:i] retweetedTweetID] isEqualToString:model.deleteTwID])
                             {
                                 dispatch_async(dispatch_get_main_queue(), ^{
                                     
                                     TweetInfo *tweet = [self.mrUserTweets objectAtIndex:i];
                                     
                                     [self.mrUserTweets removeObject:tweet];
                                     
                                     if (page == 2)
                                     {
                                         [self.profileView.tweetsTable beginUpdates];
                                         [self.profileView.tweetsTable deleteRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:i inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
                                         [self.profileView.tweetsTable endUpdates];
                                     }
                                     else
                                         [self.profileView.tweetsTable reloadData];
                                     
                                     self.profileView.tweetsCount.text = [[NSString alloc] initWithFormat:@"%d",
                                                                          self.profileView.tweetsCount.text.integerValue - 1];
                                 });
                             }
                         }
                         
                     } follow:^(TSFollow *model) {
                         
                         dispatch_async(dispatch_get_main_queue(), ^{
                             
                             if([model.source.screenName isEqualToString:self.account.username])
                             {
                                 self.profileView.followCount.text = [[NSString alloc] initWithFormat:@"%d",
                                                                      self.profileView.followCount.text.integerValue + 1];
                                 
                                 [self mrGetTwDataTimeLine:self.account usePaging:NO];
                             }
                             
                             if([model.target.screenName isEqualToString:self.account.username])
                             {
                                 self.profileView.followersCount.text = [[NSString alloc] initWithFormat:@"%d",
                                                                         self.profileView.followersCount.text.integerValue + 1];
                             }
                         });
                         
                         
                         //NSLog(@"@%@ Followed @%@", model.source.screenName, model.target.screenName);
                         
                     } favorite:^(TSFavorite *model) {
                         
                         TweetInfo *tweet = [[TweetInfo alloc] initWithTweet:model.tweet];
                         [self updateTweetFavoriteStatus:tweet];
                         
                         //NSLog(@"@%@ favorited tweet by @%@", model.source.screenName, model.tweet.user.screenName);
                     } unfavorite:^(TSFavorite *model) {
                         
                         TweetInfo *tweet = [[TweetInfo alloc] initWithTweet:model.tweet];
                         [self updateTweetFavoriteStatus:tweet];
                         
                         //NSLog(@"@%@ unfavorited tweet by @%@", model.source.screenName, model.tweet.user.screenName);
                     }userupdate:^(TSUser *model){
                         
                         [self mrGetUserDataStreem:self.account onSuccess:nil onFailure:nil];
                         
                         [self mrGetTwDataTimeLine:self.account usePaging:NO];
                         [self mrGetTwDataMentions:self.account usePaging:NO];
                         [self mrGetTwDataUserTweets:self.account usePaging:NO];
                         
                     } unsupported:^(id json) {
                         
                         dispatch_async(dispatch_get_main_queue(), ^{
                             
                             self.profileView.followCount.text = [[NSString alloc] initWithFormat:@"%d",
                                                                  self.profileView.followCount.text.integerValue + 1];
                         });
                         
                         [self mrGetTwDataTimeLine:self.account usePaging:NO];
                         //NSLog(@"Unsupported : %@", json);
                     }];
}

- (void)streamDidReceiveInvalidJson:(TSStream*)stream message:(NSString*)message
{
    [YRDropdownView showDropdownInView:self.view.window
                                 title:NSLocalizedString(@"Error", @"Error")
                                detail:message
                                 image:nil
                              animated:YES
                             hideAfter:2];
}

- (void)streamDidTimeout:(TSStream*)stream
{
    
}

- (void)streamDidFailConnection:(TSStream *)stream
{
    // Hack to just restart it, you'll want to handle this nicer :)
    [self.stream performSelector:@selector(start) withObject:nil afterDelay:10];
}

#pragma mark - Send / Remove tweet functionality

- (IBAction)createNewTweetButtonPressed:(UIButton *)sender
{
    [self openTweetComposerViewForReplyID:nil];
}

#pragma mark - ProfileViewDelegate methods

- (void)showFollowersForUser:(NSString *)username
{
    FollowersViewController *followersController = [[FollowersViewController alloc] initWithNibName:@"FollowersViewController" bundle:nil];
    followersController.account = self.account;
    followersController.username = username;
    
    [self.navigationController pushViewController:followersController animated:YES];
}

- (void)showFollowedUsersForUser:(NSString *)username
{
    FollowersViewController *followersController = [[FollowersViewController alloc] initWithNibName:@"FollowersViewController" bundle:nil];
    followersController.account = self.account;
    followersController.username = username;
    followersController.showFollowees = YES;
    
    [self.navigationController pushViewController:followersController animated:YES];
}

- (void)leftButtonPressed:(UIButton *)sender
{
    ACAccountType *accountTypeTwitter = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    ACAccountStoreRequestAccessCompletionHandler completionHandler = ^(BOOL granted, NSError *error)
    {
        if (granted && !error)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.accounts = [self.accountStore accountsWithAccountType:accountTypeTwitter];
                
                if (self.accounts.count == 0)
                {
                    [[[UIAlertView alloc] initWithTitle:nil
                                                message:@"Please add a Twitter account in the Settings app"
                                               delegate:nil
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil] show];
                }
                else
                {
                    [self dislayAccountsPopupFromView:sender];
                }
            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSString* message = [NSString stringWithFormat:@"Error getting access to accounts : %@", [error localizedDescription]];
                [[[UIAlertView alloc] initWithTitle:nil message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            });
        }
    };
    
    [self.accountStore requestAccessToAccountsWithType:accountTypeTwitter options:nil completion:completionHandler];
}

- (void)rightButtonPressed
{
    
}

#pragma mark - Show account popup

- (void)dislayAccountsPopupFromView:(UIButton *)sender
{
    self.accountsPopover = nil;
    
    AccountsViewController *accountsController = [[AccountsViewController alloc] initWithNibName:@"AccountsViewController" bundle:nil];
    accountsController.accounts = self.accounts;
    accountsController.delegate = self;
    
    self.accountsPopover = [[FPPopoverController alloc] initWithViewController:accountsController];
    self.accountsPopover.tint = FPPopoverWhiteTint;
    self.accountsPopover.border = NO;
    self.accountsPopover.contentSize = CGSizeMake(200, self.accounts.count * 44 + 36);
    
    self.accountsPopover.arrowDirection = FPPopoverArrowDirectionAny;
    [self.accountsPopover presentPopoverFromView:sender];
}

#pragma mark - FPPopoverController methods

- (void)presentedNewPopoverController:(FPPopoverController *)newPopoverController
          shouldDismissVisiblePopover:(FPPopoverController *)visiblePopoverController
{
    [visiblePopoverController dismissPopoverAnimated:YES];
}

#pragma mark - AccountsControllerDelegate methods

- (void)accountWasChanged:(ACAccount *)account
{
    [self.accountsPopover dismissPopoverAnimated:YES];
    
    if (account == self.account)
        return;
    
    [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:kTweetifyLatestTweet];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate showSplashScreen];
    
    [self.mrTimelineItems removeAllObjects];
    [self.mrTimeLineTable reloadData];
    [self.mrMentionsItems removeAllObjects];
    [self.mrMentionsTable reloadData];
    [self.mrUserTweets removeAllObjects];
    [self.profileView.tweetsTable reloadData];
    
    self.account = account;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:self.account.username forKey:@"mr.user.acc.name"];
    [userDefaults synchronize];
    
    [self loadUserInformation];
}

#pragma mark - Helpers

- (UITableView *)tableViewForDatasource:(NSMutableArray *)datasource
{
    if (datasource == self.mrTimelineItems)
        return self.mrTimeLineTable;
    
    if (datasource == self.mrMentionsItems)
        return self.mrMentionsTable;
    
    if (datasource == self.mrUserTweets)
        return self.profileView.tweetsTable;
    
    NSAssert(NO, @"Unknown datasource type");
    return nil;
}

- (NSMutableArray *)dataSourceForTabelView:(UITableView *)tableView
{
    if (tableView == self.mrTimeLineTable)
        return self.mrTimelineItems;
    if (tableView == self.mrMentionsTable)
        return self.mrMentionsItems;
    if (tableView == self.profileView.tweetsTable)
        return self.mrUserTweets;
    
    NSAssert(NO, @"Unknown tableview received");
    return nil;
}

- (TweetType)tweetTypeForDatasource:(NSArray *)datasource
{
    if (datasource == self.mrTimelineItems)
        return kTweetTypeTimeline;
    
    if (datasource == self.mrMentionsItems)
        return kTweetTypeMentions;
    
    if (datasource == self.mrUserTweets)
        return kTweetTypeUsers;
    
    NSAssert(NO, @"Unknown datasource type");
    return kTweetTypeTimeline;
}

- (NSMutableArray *)datasourceForTweetType:(TweetType)tweetType
{
    switch (tweetType)
    {
        case kTweetTypeTimeline:
            return self.mrTimelineItems;
            
        case kTweetTypeMentions:
            return self.mrMentionsItems;
            
        case kTweetTypeUsers:
            return self.mrUserTweets;
        default:
            NSAssert(NO, @"Unknown datasource type");
            break;
    }
}

#pragma mark - Theme related

- (void)themeWasChanged
{
    if ([DataProxy sharedDataProxy].currentTheme == AppThemeTypeBlack)
    {
        self.view.backgroundColor = DARK_VIEWS_BACKGROUND_COLOR;
        self.scroll.backgroundColor = DARK_VIEWS_BACKGROUND_COLOR;
        self.mrView1.backgroundColor = DARK_VIEWS_BACKGROUND_COLOR;
        self.mrView2.backgroundColor = DARK_VIEWS_BACKGROUND_COLOR;
        self.profileView.backgroundColor = DARK_VIEWS_BACKGROUND_COLOR;
        
        self.timelineTitleLabel.textColor = DARK_VIEWS_TITLE_COLOR;
        self.mentionsTitleLabel.textColor = DARK_VIEWS_TITLE_COLOR;
        
        [self.createNewTweetButton setImage:DARK_NEW_TWEET_BUTTON_COMPOSE_IMAGE forState:UIControlStateNormal];
        [self.createNewTweetButton setBackgroundImage:DARK_NEW_TWEET_BUTTON_BG_IMAGE forState:UIControlStateNormal];
        
        self.badgeView.textColor = DARK_NEW_TWEETS_BADGE_TEXT_COLOR;
        self.badgeView.badgeColor = DARK_NEW_TWEETS_BADGE_BG_COLOR;
        
        self.mentionsBadgeView.textColor = DARK_NEW_TWEETS_BADGE_TEXT_COLOR;
        self.mentionsBadgeView.badgeColor = DARK_NEW_TWEETS_BADGE_BG_COLOR;
    }
    else
    {
        self.view.backgroundColor = LIGHT_VIEWS_BACKGROUND_COLOR;
        self.scroll.backgroundColor = LIGHT_VIEWS_BACKGROUND_COLOR;
        self.mrView1.backgroundColor = LIGHT_VIEWS_BACKGROUND_COLOR;
        self.mrView2.backgroundColor = LIGHT_VIEWS_BACKGROUND_COLOR;
        self.profileView.backgroundColor = LIGHT_VIEWS_BACKGROUND_COLOR;
        
        self.timelineTitleLabel.textColor = LIGHT_VIEWS_TITLE_COLOR;
        self.mentionsTitleLabel.textColor = LIGHT_VIEWS_TITLE_COLOR;
        
        [self.createNewTweetButton setImage:LIGHT_NEW_TWEET_BUTTON_COMPOSE_IMAGE forState:UIControlStateNormal];
        [self.createNewTweetButton setBackgroundImage:LIGHT_NEW_TWEET_BUTTON_BG_IMAGE forState:UIControlStateNormal];
        
        self.badgeView.textColor = LIGHT_NEW_TWEETS_BADGE_TEXT_COLOR;
        self.badgeView.badgeColor = LIGHT_NEW_TWEETS_BADGE_BG_COLOR;
        
        self.mentionsBadgeView.textColor = LIGHT_NEW_TWEETS_BADGE_TEXT_COLOR;
        self.mentionsBadgeView.badgeColor = LIGHT_NEW_TWEETS_BADGE_BG_COLOR;
    }
    
    [self.tweetComposerView applyThemeAppearance];
    
    self.profileView.theme = [DataProxy sharedDataProxy].currentTheme;
    
    [self.mrTimeLineTable reloadData];
    [self.mrMentionsTable reloadData];
}

@end
