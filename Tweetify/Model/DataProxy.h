//
//  DataProxy.h
//  Tweetify
//
//  Created by Yuriy Berdnikov on 10/3/13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const TWTAppThemeChangedNotification;

typedef NS_ENUM(NSInteger, AppThemeType)
{
    AppThemeTypeBlack,
    AppThemeTypeWhite,
};

@interface DataProxy : NSObject

+ (DataProxy *)sharedDataProxy;

@property (nonatomic, assign) AppThemeType currentTheme;

@end
