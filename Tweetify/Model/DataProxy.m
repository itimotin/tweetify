//
//  DataProxy.m
//  Tweetify
//
//  Created by Yuriy Berdnikov on 10/3/13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import "DataProxy.h"

NSString * const TWTAppThemeChangedNotification = @"TWTAppThemeChangedNotificationEvent";

#define kAppCurrentTheme @"app.current.theme"

@implementation DataProxy

#pragma mark - Singleton mehtods

+ (DataProxy *)sharedDataProxy
{
    static DataProxy *sharedDataProxy = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedDataProxy = [[self alloc] init];
    });
    
    return sharedDataProxy;
}

- (id)init
{
    if (self = [super init])
    {
        
    }
    return self;
}

- (AppThemeType)currentTheme
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:kAppCurrentTheme] integerValue];
}

- (void)setCurrentTheme:(AppThemeType)currentTheme
{
    [[NSUserDefaults standardUserDefaults] setObject:@(currentTheme) forKey:kAppCurrentTheme];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:TWTAppThemeChangedNotification object:nil];
    
    if (currentTheme == AppThemeTypeBlack)
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    else
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
}

@end
