//
//  TweetInfo+Additions.h
//  Tweetify
//
//  Created by Yuriy Berdnikov on 08.03.13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import "TweetInfo.h"

@interface TweetInfo (Additions)

- (CGFloat)optimalCellSizeForTweet;

@end
