//
//  TweetInfo+Additions.m
//  Tweetify
//
//  Created by Yuriy Berdnikov on 08.03.13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import "TweetInfo+Additions.h"
#import "TweetViewCell.h"

@implementation TweetInfo (Additions)

- (CGFloat)optimalCellSizeForTweet
{
    //top - 50 + retweet text + text + time text = cell size
    //TODO: add tweet image if exists
    CGFloat tweetCellHeight = 50.0f;
    
    if (self.retwittedByUser.length || self.isRetweeted)
    {
        CGSize retweetTextSize = [@"Retweeted by" sizeWithFont:CELL_RETWEET_INFO_TEXT_FONT
                                                  constrainedToSize:CGSizeMake(300, 9999)];
        tweetCellHeight += retweetTextSize.height;
    }
    
    if (self.tweetText.length)
    {
        CGFloat height = [JSCoreTextView measureFrameHeightForText:self.tweetText
                                                          fontName:CELL_TWEET_TEXT_FONT.fontName
                                                          fontSize:CELL_TWEET_TEXT_FONT.pointSize
                                                constrainedToWidth:303
                                                        paddingTop:0
                                                       paddingLeft:0];
        tweetCellHeight += height + 8;
    }
    
    if (self.createdDate.length)
    {
        CGSize tweetTimeSize = [self.createdDate sizeWithFont:CELL_TWEET_TIME_TEXT_FONT
                                            constrainedToSize:CGSizeMake(300, 9999)];
        tweetCellHeight += tweetTimeSize.height + 8;
    }
    
    if (self.media.count)
    {
        tweetCellHeight += 88.0f;
    }
    
    return tweetCellHeight + 8;
}

@end
