//
//  TweetInfo.h
//  Tweetify
//
//  Created by Yuriy Berdnikov on 08.03.13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import "TSTweet.h"

@interface TweetInfo : NSObject

@property (nonatomic, copy) NSString *tweetID; //id of original tweet
@property (nonatomic, copy) NSString *tweetText;
@property (nonatomic, copy) NSString *userTwitterName; //original author twitter
@property (nonatomic, copy) NSString *userName; // original author name
@property (nonatomic, copy) NSString *createdDate; // date of new tweet
@property (nonatomic, copy) NSURL *userAvaURL; // original author avatar
@property (nonatomic, strong) NSArray *media;
@property (nonatomic, strong) NSDictionary *links;
@property (nonatomic, copy) NSMutableArray *replyToUsers;
@property (nonatomic, copy) NSString *repliedToTweetID;

@property (nonatomic, copy) NSString *retwittedByUser; // tweet author
@property (nonatomic, copy) NSString *retwittedByUserTwitterName; // tweet author twitter_name
@property (nonatomic, copy) NSString *originalTweetID; // tweet ID

//logged user related
@property (nonatomic, assign) BOOL isFavorited;
@property (nonatomic, copy) NSString *favouritedTweetID;
@property (nonatomic, assign) BOOL isRetweeted;
@property (nonatomic, copy) NSString *retweetedTweetID;

- (id)initWithTweet:(TSTweet *)tweet;

@end
