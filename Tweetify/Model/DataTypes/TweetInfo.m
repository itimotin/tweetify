//
//  TweetInfo.m
//  Tweetify
//
//  Created by Yuriy Berdnikov on 08.03.13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import <objc/runtime.h>
#import "TweetInfo.h"
#import "JSONDataParser.h"
#import "NSString+HTML.h"

@implementation TweetInfo

- (id)initWithTweet:(TSTweet *)tweet
{
    self = [super init];
    if (self)
    {
        if (tweet.rtwStatus)
        {
            self.tweetID = tweet.rtwTweetID;
            self.tweetText = [[JSONDataParser sharedJSONParser] replaceURLsInText:[[tweet.rtwStatus objectForKey:@"text"] stringByDecodingHTMLEntities] withURLs:tweet.urlsInfo];
            self.userTwitterName = [[NSString alloc] initWithFormat:@"@%@", [[tweet.rtwStatus objectForKey:@"user"] objectForKey:@"screen_name"]];
            self.userName = [[tweet.rtwStatus objectForKey:@"user"] objectForKey:@"name"];
            self.userAvaURL = [[NSURL alloc] initWithString:[[[tweet.rtwStatus objectForKey:@"user"] objectForKey:@"profile_image_url"] stringByReplacingOccurrencesOfString:@"_normal" withString:@""]];
            
            self.retwittedByUser = tweet.user.name;
            self.retwittedByUserTwitterName = tweet.user.screenName;
        }
        else
        {
            self.tweetID = tweet.tweetID;
            self.tweetText = [[JSONDataParser sharedJSONParser] replaceURLsInText:[tweet.text stringByDecodingHTMLEntities] withURLs:tweet.urlsInfo];
            self.userTwitterName = [[NSString alloc] initWithFormat:@"@%@", tweet.user.screenName];
            self.userName = tweet.user.name;
            self.userAvaURL = [[NSURL alloc] initWithString:tweet.user.ava];
        }
        
        self.originalTweetID = tweet.tweetID;
        self.createdDate = tweet.created_at;
        self.replyToUsers = [[JSONDataParser sharedJSONParser] parseReplyUsersInfoInText:self.tweetText forUser:self.userName];
        self.isFavorited = tweet.isFavorited;

        self.repliedToTweetID = tweet.repliedToID;
        self.links = [[JSONDataParser sharedJSONParser] linksMapping:tweet.urlsInfo];
        
        NSArray *mediaInfo = tweet.mediaInfo;
        if (mediaInfo.count)
            self.media = [[JSONDataParser sharedJSONParser] parseMediaInfo:mediaInfo];
        
        if (!self.media && tweet.urlsInfo)
            self.media = [[JSONDataParser sharedJSONParser] parseImagesURL:tweet.urlsInfo];
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    TweetInfo *tweet = [[[self class] allocWithZone:zone] init];
    
    tweet.tweetID = self.tweetID;
    tweet.tweetText = self.tweetText;
    tweet.userTwitterName = self.userTwitterName;
    tweet.userName = self.userName;
    tweet.createdDate = self.createdDate;
    tweet.userAvaURL = self.userAvaURL;
    tweet.media = self.media;
    tweet.links = self.links;
    tweet.replyToUsers = self.replyToUsers;
    tweet.repliedToTweetID = self.repliedToTweetID;
    tweet.retwittedByUser = self.retwittedByUser;
    tweet.originalTweetID = self.originalTweetID;
    tweet.isFavorited = self.isFavorited;
    tweet.favouritedTweetID = self.favouritedTweetID;
    tweet.isRetweeted = self.isRetweeted;
    tweet.retweetedTweetID = self.retweetedTweetID;
    tweet.retwittedByUserTwitterName = self.retwittedByUserTwitterName;
    
    return tweet;
}

- (BOOL)isEqual:(id)object
{
    return ([self.tweetID isEqualToString:[object tweetID]]);
}

- (NSUInteger)hash
{
    return (self.tweetID.hash);
}

- (NSString *)description
{
    NSMutableString *result = [NSMutableString stringWithFormat: @"<%@:%p", NSStringFromClass([self class]), self];
    
    unsigned ivarCount = 0;
    Ivar *ivarList = class_copyIvarList([self class], &ivarCount);
    
    for (unsigned i = 0; i < ivarCount; i++) {
        NSString *varName = [NSString stringWithUTF8String: ivar_getName(ivarList[i])];
        [result appendFormat: @" %@=%@", varName, [self valueForKey: varName]];
    }
    
    [result appendString: @">"];
    
    free(ivarList);
    
    return result;
}

@end
