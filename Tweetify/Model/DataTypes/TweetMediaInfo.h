//
//  TweetMediaInfo.h
//  Tweetify
//
//  Created by Yuriy Berdnikov on 09.03.13.
//  Copyright (c) 2013 smr. All rights reserved.
//

@interface TweetMediaInfo : NSObject

@property (nonatomic, copy) NSString *mediaID;
@property (nonatomic, copy) NSString *mediaType;
@property (nonatomic, copy) NSString *mediaURL;

@end
