//
//  TweetMediaInfo.m
//  Tweetify
//
//  Created by Yuriy Berdnikov on 09.03.13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import <objc/runtime.h>
#import "TweetMediaInfo.h"

@implementation TweetMediaInfo

- (id)copyWithZone:(NSZone *)zone
{
    TweetMediaInfo *mediaInfo = [[[self class] allocWithZone:zone] init];
    mediaInfo.mediaID = self.mediaID;
    mediaInfo.mediaType = self.mediaType;
    mediaInfo.mediaURL = self.mediaURL;
    
    return mediaInfo;
}

- (BOOL)isEqual:(id)object
{
    return (self.mediaID == [object mediaID] && self.mediaType == [object mediaType] &&
            self.mediaURL == [object mediaURL]);
}

- (NSUInteger)hash
{
    return (self.mediaID.hash + self.mediaType.hash + self.mediaURL.hash);
}

- (NSString *)description
{
    NSMutableString *result = [NSMutableString stringWithFormat: @"<%@:%p", NSStringFromClass([self class]), self];
    
    unsigned ivarCount = 0;
    Ivar *ivarList = class_copyIvarList([self class], &ivarCount);
    
    for (unsigned i = 0; i < ivarCount; i++) {
        NSString *varName = [NSString stringWithUTF8String: ivar_getName(ivarList[i])];
        [result appendFormat: @" %@=%@", varName, [self valueForKey: varName]];
    }
    
    [result appendString: @">"];
    
    free(ivarList);
    
    return result;
}

@end
