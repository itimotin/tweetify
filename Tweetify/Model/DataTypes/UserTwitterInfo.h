//
//  UserTwitterInfo.h
//  Tweetify
//
//  Created by Yuriy Berdnikov on 17.04.13.
//  Copyright (c) 2013 smr. All rights reserved.
//

@interface UserTwitterInfo : NSObject

@property (nonatomic, copy) NSString *userTwitterName; //original author twitter
@property (nonatomic, copy) NSString *userName; // original author name
@property (nonatomic, copy) NSURL *userAvaURL; // original author avatar

@end
