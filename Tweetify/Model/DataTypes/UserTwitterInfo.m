//
//  UserTwitterInfo.m
//  Tweetify
//
//  Created by Yuriy Berdnikov on 17.04.13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import <objc/runtime.h>
#import "UserTwitterInfo.h"

@implementation UserTwitterInfo

- (id)copyWithZone:(NSZone *)zone
{
    UserTwitterInfo *userInfo = [[[self class] allocWithZone:zone] init];
    
    userInfo.userTwitterName = self.userTwitterName;
    userInfo.userName = self.userName;
    userInfo.userAvaURL = self.userAvaURL;
    
    return userInfo;
}

- (BOOL)isEqual:(id)object
{
    return ([self.userTwitterName isEqualToString:[object userTwitterName]] &&
            [self.userName isEqualToString:[object userName]] &&
            self.userAvaURL == [object userAvaURL]);
}

- (NSUInteger)hash
{
    return (self.userTwitterName.hash + self.userName.hash + self.userAvaURL.hash);
}

- (NSString *)description
{
    NSMutableString *result = [NSMutableString stringWithFormat: @"<%@:%p", NSStringFromClass([self class]), self];
    
    unsigned ivarCount = 0;
    Ivar *ivarList = class_copyIvarList([self class], &ivarCount);
    
    for (unsigned i = 0; i < ivarCount; i++) {
        NSString *varName = [NSString stringWithUTF8String: ivar_getName(ivarList[i])];
        [result appendFormat: @" %@=%@", varName, [self valueForKey: varName]];
    }
    
    [result appendString: @">"];
    
    free(ivarList);
    
    return result;
}


@end
