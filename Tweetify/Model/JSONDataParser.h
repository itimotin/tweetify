//
//  JSONDataParser.h
//  Tweetify
//
//  Created by Yuriy Berdnikov on 08.03.13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import "TweetInfo.h"

@interface JSONDataParser : NSObject

+ (JSONDataParser *)sharedJSONParser;

- (NSArray *)parseImagesURL:(NSArray *)imagesURLS;
- (NSArray *)parseMediaInfo:(NSArray *)mediaInfo;
- (NSDictionary *)linksMapping:(NSArray *)urls;
- (NSString *)replaceURLsInText:(NSString *)text withURLs:(NSArray *)urls;
- (NSMutableArray *)parseUsersInfo:(NSArray *)usersInfo;

- (NSMutableArray *)parseReplyUsersInfoInText:(NSString *)text forUser:(NSString *)userTwitterName;
- (TweetInfo *)parseTweetInfo:(NSDictionary *)tweetData;
- (NSMutableArray *)parseTweetsFeed:(NSArray *)tweetsJSONData;

@end
