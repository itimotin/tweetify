//
//  JSONDataParser.m
//  Tweetify
//
//  Created by Yuriy Berdnikov on 08.03.13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import "JSONDataParser.h"
#import "TweetMediaInfo.h"
#import "NSString+HTML.h"
#import "UserTwitterInfo.h"

@implementation JSONDataParser

#pragma mark - Singleton Methods

static JSONDataParser *sharedJSONParser = nil;

// Get the shared instance and create it if necessary.
+ (JSONDataParser *)sharedJSONParser
{
    if (sharedJSONParser == nil)
        sharedJSONParser = [[super allocWithZone:NULL] init];
    
    return sharedJSONParser;
}

// We can still have a regular init method, that will get called the first time the Singleton is used.
- (id)init
{
    self = [super init];
    
    if (self)
    {
        
    }
    
    return self;
}

// Your dealloc method will never be called, as the singleton survives for the duration of your app.
// However, I like to include it so I know what memory I'm using (and incase, one day, I convert away from Singleton).
-(void)dealloc
{
    // I'm never called!
}

// We don't want to allocate a new instance, so return the current one.
+ (id)allocWithZone:(NSZone*)zone
{
    return [self sharedJSONParser];
}

// Equally, we don't want to generate multiple copies of the singleton.
- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

#pragma mark - Object methods

- (NSMutableArray *)parseUsersInfo:(NSArray *)usersInfo
{
    __block NSMutableArray *followers = [[NSMutableArray alloc] init];
    
    [usersInfo enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        UserTwitterInfo *userInfo = [[UserTwitterInfo alloc] init];
        userInfo.userName = [obj objectForKey:@"name"];
        userInfo.userTwitterName = [obj objectForKey:@"screen_name"];
        
        NSURL *imageURL = [[NSURL alloc] initWithString:[[obj objectForKey:@"profile_image_url"]
                                                         stringByReplacingOccurrencesOfString:@"_normal" withString:@"_bigger"]];
        userInfo.userAvaURL = imageURL;
        [followers addObject:userInfo];
    }];
    
    return followers;
}

- (NSArray *)parseEntities:(NSDictionary *)entities
{
    if (!entities)
        return nil;
    
    NSArray *mediaInfo = [entities objectForKey:@"media"];
    if (mediaInfo)
        return [self parseMediaInfo:mediaInfo];
    
    //check other services
    NSArray *urls = [entities objectForKey:@"urls"];
    if (urls.count)
        return [self parseImagesURL:urls];
    
    return nil;
}

- (NSArray *)parseImagesURL:(NSArray *)imagesURLS
{
    __block NSMutableArray *parsedURLs = [[NSMutableArray alloc] init];
    
    [imagesURLS enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *expandedURL = [obj objectForKey:@"expanded_url"];
        if (!expandedURL.length)
            return;
        
        if ([expandedURL rangeOfString:@"http://instagram.com"].location != NSNotFound)
        {
            //found instagram photo - dump it
            NSString *dumpPhotoString = [[NSString alloc] initWithFormat:@"http://api.instagram.com/oembed?url=%@", expandedURL];
            
            TweetMediaInfo *tweetMedia = [[TweetMediaInfo alloc] init];
            tweetMedia.mediaURL = dumpPhotoString;
            tweetMedia.mediaType = @"instagram";
            
            [parsedURLs addObject:tweetMedia];
        }
        else if ([expandedURL rangeOfString:@"twitpic.com"].location != NSNotFound)
        {
            NSURL *twitpicURL = [[NSURL alloc] initWithString:expandedURL];
            
            TweetMediaInfo *tweetMedia = [[TweetMediaInfo alloc] init];
            tweetMedia.mediaURL = [[NSString alloc] initWithFormat:@"http://twitpic.com/show/full/%@", twitpicURL.lastPathComponent];
            tweetMedia.mediaType = @"twitpic";
            
            [parsedURLs addObject:tweetMedia];
        }
        else if ([expandedURL rangeOfString:@"yfrog.com"].location != NSNotFound)
        {     
            TweetMediaInfo *tweetMedia = [[TweetMediaInfo alloc] init];
            tweetMedia.mediaURL = [[NSString alloc] initWithFormat:@"%@:iphone", expandedURL];
            tweetMedia.mediaType = @"yfrog";
            
            [parsedURLs addObject:tweetMedia];
        }
    }];
    
    return parsedURLs.count ? parsedURLs : nil;
}

- (NSArray *)parseMediaInfo:(NSArray *)mediaInfo
{
    if (!mediaInfo.count)
        return nil;
    
    __block NSMutableArray *info = [[NSMutableArray alloc] init];
    
    [mediaInfo enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSDictionary *media = (NSDictionary *)obj;
        
        //TODO: currently support only images
        if (![[media objectForKey:@"type"] isEqualToString:@"photo"])
            return;
        
        TweetMediaInfo *tweetMedia = [[TweetMediaInfo alloc] init];
        tweetMedia.mediaID = [media objectForKey:@"id_str"];
        tweetMedia.mediaURL = [media objectForKey:@"media_url"];
        tweetMedia.mediaType = [media objectForKey:@"type"];
        
        [info addObject:tweetMedia];
    }];
    
    return info.count ? info : nil;
}

- (NSMutableArray *)parseReplyUsersInfoInText:(NSString *)text forUser:(NSString *)userTwitterName
{
    NSString *tweetText = [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSRange rangeUser = [tweetText rangeOfString:@"@"];
    if (rangeUser.location == NSNotFound)
        return nil;
    
    NSMutableArray *replyArray = [[NSMutableArray alloc] init];
    NSString *bufText = tweetText;
    
    while (rangeUser.location != NSNotFound)
    {
        bufText = [bufText substringFromIndex:rangeUser.location];
        
        NSRange range = [bufText rangeOfString:@" "];
        NSString *name;
        
        if (range.location != NSNotFound)
        {
            name = [[[[NSString alloc] initWithFormat:@"%@", bufText] substringToIndex:range.location] stringByReplacingOccurrencesOfString:@":" withString:@""];
            bufText = [[[NSString stringWithFormat:@"%@", bufText] substringFromIndex:range.location] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        }
        else
        {
            name = [[[[NSString alloc] initWithFormat:@"%@", bufText] substringToIndex:bufText.length] stringByReplacingOccurrencesOfString:@":" withString:@""];
            bufText = @"";
        }
        
        if ((![replyArray containsObject:name]) && (![name isEqualToString:[[NSString alloc] initWithFormat:@"@%@", userTwitterName]]))
            [replyArray addObject:name];
        
        rangeUser = [bufText rangeOfString:@"@"];
    }
    
    return replyArray;
}

- (TweetInfo *)parseTweetInfo:(NSDictionary *)tweetData
{
    TweetInfo *tweet = [[TweetInfo alloc] init];
      
    if ([tweetData objectForKey:@"retweeted_status"])
    {
        //this is a retweet
        NSDictionary *retweetInfo = [tweetData objectForKey:@"retweeted_status"];
     
        tweet.tweetID = [retweetInfo objectForKey:@"id_str"];
        tweet.tweetText = [self replaceURLsInText:[[retweetInfo objectForKey:@"text"] stringByDecodingHTMLEntities] withURLs:[[retweetInfo objectForKey:@"entities"] objectForKey:@"urls"]];        
        tweet.userTwitterName = [[NSString alloc] initWithFormat:@"@%@", [[retweetInfo objectForKey:@"user"] objectForKey:@"screen_name"]];        
        tweet.userName = [[retweetInfo objectForKey:@"user"] objectForKey:@"name"];
        
        NSString *avaURL = [[[retweetInfo objectForKey:@"user"] objectForKey:@"profile_image_url"] stringByReplacingOccurrencesOfString:@"normal" withString:@"bigger"];
        if (avaURL.length)
            tweet.userAvaURL = [[NSURL alloc] initWithString:avaURL];
        else
            tweet.userAvaURL = [[NSURL alloc] initWithString:[[retweetInfo objectForKey:@"user"] objectForKey:@"profile_image_url"]];
        
        tweet.media = [self parseEntities:[retweetInfo objectForKey:@"entities"]];
        tweet.links = [self linksMapping:[[retweetInfo objectForKey:@"entities"] objectForKey:@"urls"]];
        tweet.repliedToTweetID = [retweetInfo objectForKey:@"in_reply_to_status_id_str"];
        tweet.retwittedByUser = [[tweetData objectForKey:@"user"] objectForKey:@"name"];
        tweet.retwittedByUserTwitterName = [[tweetData objectForKey:@"user"] objectForKey:@"screen_name"];
    }
    else
    {
        tweet.tweetID = [tweetData objectForKey:@"id_str"];
        tweet.tweetText = [self replaceURLsInText:[[tweetData objectForKey:@"text"] stringByDecodingHTMLEntities] withURLs:[[tweetData objectForKey:@"entities"] objectForKey:@"urls"]];
        tweet.userTwitterName = [[NSString alloc] initWithFormat:@"@%@", [[tweetData objectForKey:@"user"] objectForKey:@"screen_name"]];        
        tweet.userName = [[tweetData objectForKey:@"user"] objectForKey:@"name"];
        
        NSString *avaURL = [[[tweetData objectForKey:@"user"] objectForKey:@"profile_image_url"] stringByReplacingOccurrencesOfString:@"normal" withString:@"bigger"];
        if (avaURL.length)
            tweet.userAvaURL = [[NSURL alloc] initWithString:avaURL];
        else
            tweet.userAvaURL = [[NSURL alloc] initWithString:[[tweetData objectForKey:@"user"] objectForKey:@"profile_image_url"]];
        
        tweet.media = [self parseEntities:[tweetData objectForKey:@"entities"]];
        tweet.links = [self linksMapping:[[tweetData objectForKey:@"entities"] objectForKey:@"urls"]];
        tweet.repliedToTweetID = [tweetData objectForKey:@"in_reply_to_status_id_str"];
    }

    tweet.originalTweetID = [tweetData objectForKey:@"id_str"];
    tweet.createdDate = [tweetData objectForKey:@"created_at"];
    tweet.isFavorited = [[tweetData objectForKey:@"favorited"] boolValue];
   
    //TODO - maybe twitter_name instead !!!
    tweet.replyToUsers = [self parseReplyUsersInfoInText:tweet.tweetText forUser:tweet.userName];

    return tweet;
}

- (NSString *)replaceURLsInText:(NSString *)text withURLs:(NSArray *)urls
{
    if (!urls.count)
        return text;

    __block NSString *textWithFullURLs = nil;
    [urls enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSDictionary *url = (NSDictionary *)obj;
        
        textWithFullURLs = [text stringByReplacingOccurrencesOfString:[url objectForKey:@"url"] withString:[url objectForKey:@"display_url"]];
    }];
    
    return textWithFullURLs;
}

- (NSDictionary *)linksMapping:(NSArray *)urls
{
    if (!urls.count)
        return nil;
    
    __block NSMutableDictionary *mappedURLs = [[NSMutableDictionary alloc] init];
    [urls enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        [mappedURLs setObject:[obj objectForKey:@"expanded_url"]
                       forKey:[[obj objectForKey:@"display_url"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    }];
    
    return mappedURLs;
}

- (NSMutableArray *)parseTweetsFeed:(NSArray *)tweetsJSONData
{
    if (!tweetsJSONData.count || ![tweetsJSONData isKindOfClass:[NSArray class]])
        return nil;
    
    __block NSMutableArray *tweets = [[NSMutableArray alloc] init];
    
    [tweetsJSONData enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSDictionary *tweetData = (NSDictionary *)obj;
        [tweets addObject:[self parseTweetInfo:tweetData]];
    }];
    
    return tweets;
}

@end
