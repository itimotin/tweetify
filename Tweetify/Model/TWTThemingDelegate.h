//
//  TWTThemingDelegate.h
//  Tweetify
//
//  Created by Yuriy Berdnikov on 10/3/13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataProxy.h"

@protocol TWTThemingDelegate <NSObject>

- (void)themeWasChanged:(AppThemeType)theme;

@end
