//
//  ThemeAppearance.h
//  Tweetify
//
//  Created by Yuriy Berdnikov on 10/3/13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#ifndef Tweetify_ThemeAppearance_h
#define Tweetify_ThemeAppearance_h

//Light theme

#define LIGHT_VIEWS_BACKGROUND_COLOR [UIColor colorWithRed:245.0f / 255.0f green:246.0f / 255.0f blue:250.0f / 255.0f alpha:1.0f]
#define LIGHT_VIEWS_TITLE_COLOR [UIColor colorWithRed:0.0f green:31.0f / 255.0f blue:32.0f / 255.0f alpha:1.0f]
#define LIGHT_NEW_TWEETS_BADGE_TEXT_COLOR [UIColor whiteColor]
#define LIGHT_NEW_TWEETS_BADGE_BG_COLOR [UIColor colorWithRed:0 / 255.0 green:153 / 255.0 blue:203 / 255.0 alpha:1.0]

#define LIGHT_TWEET_USERNAME_COLOR [UIColor colorWithRed:51.0f / 255.0f green:51.0f / 255.0f blue:51.0f / 255.0f alpha:1.0f]
#define LIGHT_TWEET_USET_TWITTER_NAME_COLOR [UIColor colorWithRed:120.0f / 255.0f green:120.0f / 255.0f blue:120.0f / 255.0f alpha:1.0f]
#define LIGHT_TWEET_DATE_COLOR [UIColor colorWithRed:120.0f / 255.0f green:120.0f / 255.0f blue:120.0f / 255.0f alpha:1.0f]
#define LIGHT_TWEET_TEXT_COLOR [UIColor colorWithRed:95.0f / 255.0f green:95.0f / 255.0f blue:95.0f / 255.0f alpha:1.0f]
#define LIGHT_TWEET_TEXT_LINK_COLOR [UIColor colorWithRed:0.0f green:153.0f / 255.0f blue:203.0f / 255.0f alpha:1.0f]
#define LIGHT_TWEET_ACTION_BUTTON_COLOR [UIColor colorWithRed:0 / 255.0 green:153 / 255.0 blue:203 / 255.0 alpha:1.0]
#define LIGHT_TWEET_DELETE_BUTTON_COLOR [UIColor colorWithRed:196 / 255.0 green:49 / 255.0 blue:66 / 255.0 alpha:1.0]
#define LIGHT_TWEET_RETWEETED_BY_NORMAL_COLOR [UIColor colorWithRed:0 / 255.0 green:153 / 255.0 blue:203 / 255.0 alpha:1.0]
#define LIGHT_TWEET_RETWEETED_BY_SELECTED_COLOR [UIColor colorWithRed:0 / 255.0f green:153 / 255.0f blue:203 / 255.0f alpha:1.0f]

//new tweet button
#define LIGHT_NEW_TWEET_BUTTON_BG_IMAGE [UIImage imageNamed:@"new tweet button bg white.png"]
#define LIGHT_NEW_TWEET_BUTTON_COMPOSE_IMAGE [UIImage imageNamed:@"new tweet button white.png"]
#define LIGHT_NEW_TWEET_BUTTON_LOADING_TYPE UIActivityIndicatorViewStyleGray

//tweet composer
#define LIGHT_TWEET_COMPOSER_BG_COLOR [UIColor colorWithRed:245 / 255.0f green:246 / 255.0f blue:250 / 255.0f alpha:1.0f]
#define LIGHT_TWEET_COMPOSER_TWEET_FIELD_COLOR [UIColor colorWithRed:245 / 255.0f green:246 / 255.0f blue:250 / 255.0f alpha:1.0f]
#define LIGHT_TWEET_COMPOSER_TWEET_TEXT_COLOR [UIColor colorWithRed:8 / 255.0f green:32 / 255.0f blue:31 / 255.0f alpha:1.0f]
#define LIGHT_TWEET_COMPOSER_TWEET_EXTRA_TEXT_COLOUR [UIColor colorWithRed:8 / 255.0f green:31 / 255.0f blue:250 / 255.0f alpha:1.0f]
#define LIGHT_TWEET_COMPOSER_COUNTER_COLOR [UIColor colorWithRed:95 / 255.0f green:95 / 255.0f blue:95 / 255.0f alpha:1.0f]
#define LIGHT_TWEET_COMPOSER_SUGGESTION_TEXT_COLOR [UIColor colorWithRed:0 / 255.0f green:153 / 255.0f blue:203 / 255.0f alpha:1.0f]
#define LIGHT_TWEET_COMPOSER_DELETE_BUTTON_IMAGE [UIImage imageNamed:@"delete 1.png"]
#define LIGHT_TWEET_COMPOSER_IMAGE_SELECTION_BUTTON_IMAGE [UIImage imageNamed:@"picture_button.png"]
#define LIGHT_TWEET_COMPOSER_POST_BUTTON_IMAGE [UIImage imageNamed:@"send 1.png"]

//settings
#define LIGHT_SETTINGS_TABLE_TEXT_COLOR [UIColor blackColor]
#define LIGHT_SETTINGS_ABOUT_TEXT_COLOR [UIColor blackColor]

//profile
#define LIGHT_PROFILE_USER_ICON_STROKE_IMAGE [UIImage imageNamed:@"user_icon_stroke.png"]
#define LIGHT_PROFILE_BACKGROUND_IMAGE [UIImage imageNamed:@"bg black fill.png"]
#define LIGHT_PROFILE_STATISTICS_BACKGROUND_IMAGE [UIImage imageNamed:@"table.png"]


//Dark theme

#define DARK_VIEWS_BACKGROUND_COLOR [UIColor blackColor]
#define DARK_VIEWS_TITLE_COLOR [UIColor whiteColor]
#define DARK_NEW_TWEETS_BADGE_TEXT_COLOR [UIColor whiteColor]
#define DARK_NEW_TWEETS_BADGE_BG_COLOR [UIColor colorWithRed:0 / 255.0 green:153 / 255.0 blue:203 / 255.0 alpha:1.0]

#define DARK_TWEET_USERNAME_COLOR [UIColor whiteColor]
#define DARK_TWEET_USET_TWITTER_NAME_COLOR [UIColor darkGrayColor]
#define DARK_TWEET_DATE_COLOR [UIColor darkGrayColor]
#define DARK_TWEET_TEXT_COLOR [UIColor whiteColor]
#define DARK_TWEET_TEXT_LINK_COLOR [UIColor colorWithRed:0 / 255.0 green:153 / 255.0 blue:203 / 255.0 alpha:1.0]
#define DARK_TWEET_ACTION_BUTTON_COLOR [UIColor colorWithRed:0 / 255.0 green:153 / 255.0 blue:203 / 255.0 alpha:1.0]
#define DARK_TWEET_DELETE_BUTTON_COLOR [UIColor colorWithRed:196 / 255.0 green:49 / 255.0 blue:66 / 255.0 alpha:1.0]
#define DARK_TWEET_RETWEETED_BY_NORMAL_COLOR [UIColor colorWithRed:0 / 255.0 green:153 / 255.0 blue:203 / 255.0 alpha:1.0]
#define DARK_TWEET_RETWEETED_BY_SELECTED_COLOR [UIColor colorWithRed:82 / 255.0f green:82 / 255.0f blue:82 / 255.0f alpha:1.0f]

//new tweet button
#define DARK_NEW_TWEET_BUTTON_BG_IMAGE [UIImage imageNamed:@"new tweet button bg black.png"]
#define DARK_NEW_TWEET_BUTTON_COMPOSE_IMAGE [UIImage imageNamed:@"new tweet button black.png"]
#define DARK_NEW_TWEET_BUTTON_LOADING_TYPE UIActivityIndicatorViewStyleWhite


//tweet composer
#define DARK_TWEET_COMPOSER_BG_COLOR [UIColor colorWithRed:20 / 255.0f green:20 / 255.0f blue:20 / 255.0f alpha:1.0f]
#define DARK_TWEET_COMPOSER_TWEET_FIELD_COLOR [UIColor colorWithRed:20 / 255.0f green:20 / 255.0f blue:20 / 255.0f alpha:1.0f]
#define DARK_TWEET_COMPOSER_TWEET_EXTRA_TEXT_COLOUR  [UIColor colorWithRed:250 / 255.0f green:8 / 255.0f blue:31 / 255.0f alpha:1.0f]
#define DARK_TWEET_COMPOSER_COUNTER_COLOR [UIColor whiteColor]
#define DARK_TWEET_COMPOSER_TWEET_TEXT_COLOR [UIColor whiteColor]
#define DARK_TWEET_COMPOSER_SUGGESTION_TEXT_COLOR [UIColor whiteColor]
#define DARK_TWEET_COMPOSER_DELETE_BUTTON_IMAGE [UIImage imageNamed:@"delete 1.png"]
#define DARK_TWEET_COMPOSER_IMAGE_SELECTION_BUTTON_IMAGE [UIImage imageNamed:@"picture_button black.png"]
#define DARK_TWEET_COMPOSER_POST_BUTTON_IMAGE [UIImage imageNamed:@"send 1.png"]

//settings
#define DARK_SETTINGS_TABLE_TEXT_COLOR [UIColor whiteColor]
#define DARK_SETTINGS_ABOUT_TEXT_COLOR [UIColor whiteColor]

//profile
#define DARK_PROFILE_USER_ICON_STROKE_IMAGE [UIImage imageNamed:@"user_icon_stroke.png"]
#define DARK_PROFILE_BACKGROUND_IMAGE [UIImage imageNamed:@"bg black fill.png"]
#define DARK_PROFILE_STATISTICS_BACKGROUND_IMAGE [UIImage imageNamed:@"table.png"]

#endif
