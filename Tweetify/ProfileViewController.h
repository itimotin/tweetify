//
//  ProfileViewController.h
//  Tweetify
//
//  Created by smr on 24.02.13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Twitter/Twitter.h>
#import <Accounts/Accounts.h>
#import "BaseViewController.h"

@interface ProfileViewController : BaseViewController

@property (nonatomic, strong) NSMutableArray *mrUserTweets;
@property (nonatomic, strong) NSString *currentUserName;

@end
