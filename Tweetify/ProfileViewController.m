//
//  ProfileViewController.m
//  Tweetify
//
//  Created by smr on 24.02.13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import "UIImageView+WebCache.h"
#import "TweetInfo+Additions.h"
#import "ProfileViewController.h"
#import "ProfileView.h"
#import "TweetViewCell.h"
#import "TweetInfo.h"
#import "JSONDataParser.h"
#import "TSMiniWebBrowser.h"
#import "TweetMediaInfo.h"
#import "AllAroundPullView.h"
#import "FollowersViewController.h"

#define BUTTON_TAG_ACTION_FOLLOW 1001
#define BUTTON_TAG_ACTION_UNFOLLOW 1002

@interface ProfileViewController () <UITableViewDataSource, UITableViewDelegate,
ProfileViewDelegate>

@property (nonatomic, strong) ProfileView *profileView;

@property (nonatomic, strong) AllAroundPullView *profileBottomPullView;
@end

@implementation ProfileViewController

@synthesize mrUserTweets;
@synthesize currentUserName;
@synthesize account;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {

    }
    return self;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    self.profileView = nil;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    self.profileView = nil;
}

- (void)loadView
{
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"ProfileView" owner:self options:nil];
    
    // assuming the view is the only top-level object in the nib file (besides File's Owner and First Responder)
    UIView *nibView = [nibObjects objectAtIndex:0];
    self.profileView = (ProfileView *)nibView;
    self.profileView.delegate = self;
    self.view = self.profileView;
    
    self.profileView.theme = [DataProxy sharedDataProxy].currentTheme;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UINib *customCellNib = [UINib nibWithNibName:@"TweetCellView" bundle:nil];
    [self.profileView.tweetsTable registerNib:customCellNib forCellReuseIdentifier:@"tweetCell"];
    self.profileView.tweetsTable.delegate = self;
    self.profileView.tweetsTable.dataSource = self;
    self.profileView.rightButton.hidden = YES;
    
    self.profileView.userTwitterName = self.currentUserName;
    
    [self applyThemeAppearance];

    [self mrGetTwDataUserTweets:self.currentUserName usePaging:NO];
    [self mrGetUserData:self.currentUserName];
}

- (AllAroundPullView *)profileBottomPullView
{
    if (!_profileBottomPullView)
    {
        _profileBottomPullView = [[AllAroundPullView alloc] initWithScrollView:self.profileView.tweetsTable
                                                                      position:AllAroundPullViewPositionBottom
                                                                        action:^(AllAroundPullView *view){
                                                                            [self mrGetTwDataUserTweets:self.currentUserName usePaging:YES];
                                                                        }];
        _profileBottomPullView.activityView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
    }
    return _profileBottomPullView;
}


- (void)gotoProfile:(NSString *)profileName
{
    if ([profileName isEqualToString:self.currentUserName])
        return;
    
    [super gotoProfile:profileName];
}

-(void)refreshData
{
    [self mrGetTwDataUserTweets:self.currentUserName usePaging:NO];
}

- (void)mrGetUserData:(NSString *)accountName
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSURL *userTwitterInfoURL = [[NSURL alloc] initWithString:[[NSString alloc] initWithFormat:@"https://api.twitter.com/1.1/users/show.json?screen_name=%@", accountName]];
    SLRequest *request  = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:TWRequestMethodGET URL:userTwitterInfoURL parameters:nil];
    [request setAccount:self.account];
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
     {
         if (responseData)
         {   
             NSError *error;
             NSDictionary *mrUserInfo = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
             if (error)
             {
                 NSAssert(NO, error.localizedDescription);
             }
             
             if (![self isResponceValid:mrUserInfo error:error])
                 return;
             
             dispatch_async(dispatch_get_main_queue(), ^{

                 self.profileView.username.text = [mrUserInfo objectForKey:@"name"];
                 
                 self.profileView.followCount.text = [[NSString alloc] initWithFormat:@"%@",
                                                      [mrUserInfo objectForKey:@"friends_count"]];
                 self.profileView.followersCount.text = [[NSString alloc] initWithFormat:@"%@",
                                                         [mrUserInfo objectForKey:@"followers_count"]];
                 self.profileView.tweetsCount.text = [[NSString alloc] initWithFormat:@"%@",
                                                      [mrUserInfo objectForKey:@"statuses_count"]];
                 
                 NSURL *imageURL = [[NSURL alloc] initWithString:[[mrUserInfo objectForKey:@"profile_image_url"] stringByReplacingOccurrencesOfString:@"_normal" withString:@""]];
                 [self.profileView.userIcon setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"profile_icon_placeholder.png"]];
                 
                 if ([[mrUserInfo objectForKey:@"following"] boolValue])
                 {
                     [self.profileView.rightButton setImage:[UIImage imageNamed:@"following.png"] forState:UIControlStateNormal];
                     self.profileView.rightButton.tag = BUTTON_TAG_ACTION_UNFOLLOW;
                 }
                 else
                 {
                     [self.profileView.rightButton setImage:[UIImage imageNamed:@"follow.png"] forState:UIControlStateNormal];
                     self.profileView.rightButton.tag = BUTTON_TAG_ACTION_FOLLOW;
                 }
                 
                 if ([self.currentUserName isEqualToString:self.account.username])
                     self.profileView.rightButton.hidden = YES;
                 else
                     self.profileView.rightButton.hidden = NO;
                 
                 [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                 
             });
         }
         else
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [self.navigationController popViewControllerAnimated:YES];
             });
         }
         
     }];
    
    NSURL *profileBannerURL = [[NSURL alloc] initWithString:[[NSString alloc] initWithFormat:@"https://api.twitter.com/1.1/users/profile_banner.json?screen_name=%@", accountName]];
    SLRequest *request2  = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:TWRequestMethodGET URL:profileBannerURL parameters:nil];
    [request2 setAccount:self.account];
    [request2 performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
     {
         if (responseData)
         {
             NSError *error;
             NSDictionary *mrUserInfo = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
             if (error)
             {
                 NSAssert(NO, error.localizedDescription);
                 return;
             }
             
             NSString *imageAddress = [[[mrUserInfo objectForKey:@"sizes"]objectForKey:@"mobile_retina"]objectForKey:@"url"];
             
             if (imageAddress.length)
             {
                 NSURL *imageURL = [[NSURL alloc] initWithString:imageAddress];
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [self.profileView.profileBackground setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"profile_banner_placeholder.png"]];
                 });
             }
         }
     }];
}

- (void)mrGetTwDataUserTweets:(NSString *)accountName usePaging:(BOOL)paging
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    NSString *requestAddress;
    if (paging)
    {
        TweetInfo *oldestTweet = [self.mrUserTweets lastObject];
        requestAddress = [[NSString alloc] initWithFormat:@"http://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=%@&max_id=%@", accountName, oldestTweet.originalTweetID];
    }
    else
    {
        requestAddress = [[NSString alloc] initWithFormat:@"http://api.twitter.com/1.1/statuses/user_timeline.json?screen_name=%@", accountName];
    }
    
    NSURL *userTimelineURL = [[NSURL alloc] initWithString:requestAddress];
    
    SLRequest *request  = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:TWRequestMethodGET URL:userTimelineURL parameters:nil];
    [request setAccount:self.account];
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
     {
         if (responseData)
         {
             NSError *error;
             NSArray *userTweetsItems = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
             if (error)
             {
                 NSAssert(NO, error.localizedDescription);
             }
             
             if (![self isResponceValid:userTweetsItems error:error])
                 return;
             
             NSMutableArray *parsedResult = [[JSONDataParser sharedJSONParser] parseTweetsFeed:userTweetsItems];
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 if (parsedResult.count)
                 {
                     if (paging)
                     {
                         TweetInfo *latestTweet = self.mrUserTweets.count ? [self.mrUserTweets lastObject] : nil;
                         
                         if (latestTweet)
                             [parsedResult removeObject:latestTweet];
                         
                         [self.mrUserTweets addObjectsFromArray:parsedResult];
                     }
                     else
                         self.mrUserTweets = parsedResult;
                     
                     [self.profileView.tweetsTable reloadData];
                     
                     if (![self.profileBottomPullView isDescendantOfView:self.profileView.tweetsTable])
                         [self.profileView.tweetsTable addSubview:self.profileBottomPullView];
                 }

                 [self.profileBottomPullView finishedLoading];
                 [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
             });
         }
     }];
}

#pragma mark - Table functions

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.mrUserTweets.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int rowIndex = indexPath.row;

    static NSString *CellIdentifier = @"tweetCell";
    TweetViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
        cell = [[TweetViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    
    cell.delegate = self;
    cell.currentTheme = [DataProxy sharedDataProxy].currentTheme;
    
    TweetInfo *tweet = [self.mrUserTweets objectAtIndex:rowIndex];

    if ([tweet.userTwitterName isEqualToString:[[NSString alloc] initWithFormat:@"@%@", self.account.username]])
        cell.showDeleteButton = YES;
    else
        cell.showDeleteButton = NO;
    
    cell.tweetInfo = tweet;
    cell.tweetType = kTweetTypeUsers;
    [cell.userIcon setImageWithURL:tweet.userAvaURL placeholderImage:[UIImage imageNamed:@"profile_icon_placeholder.png"]];
    cell.tweetTime.text = [self mrGetTimeStrFromDate:tweet.createdDate];
    cell.username.text = tweet.userName;
    cell.userTwitterName.text = tweet.userTwitterName;
    cell.isFavorited = tweet.isFavorited;
    cell.repliedTweetID = tweet.repliedToTweetID;
    cell.links = tweet.links;
    
    if ([tweet.retwittedByUserTwitterName isEqualToString:self.account.username] || tweet.isRetweeted)
    {
        cell.isRetweeted = YES;
        cell.retweetInfoText.text = NSLocalizedString(@"Retweeted by You", @"Retweeted by You");
    }
    else
    {
        cell.isRetweeted = NO;
        cell.retweetInfoText.text = tweet.retwittedByUser.length ? [[NSString alloc] initWithFormat:@"Retweeted by %@", tweet.retwittedByUser] : @"";
    }

    [cell.tweetImage cancelCurrentImageLoad];
    if (tweet.media.count)
    {
        //TODO: place only 1 image - add support more
        TweetMediaInfo *mediaInfo = (TweetMediaInfo *)[tweet.media lastObject];
        
        NSString *thumbAddress;
        if ([mediaInfo.mediaType isEqualToString:@"photo"])
        {
            thumbAddress = [[NSString alloc] initWithFormat:@"%@:thumb", mediaInfo.mediaURL];
            
            [cell.tweetImage setImageWithURL:[[NSURL alloc] initWithString:thumbAddress]
                            placeholderImage:[UIImage imageNamed:@"tweet_image_placeholder.png"]];
            cell.originalImageURL = [[NSURL alloc] initWithString:mediaInfo.mediaURL];
        }
        else if ([mediaInfo.mediaType isEqualToString:@"instagram"])
        {
            cell.tweetImage.image = [UIImage imageNamed:@"tweet_image_placeholder.png"];
            NSURLRequest *request = [NSURLRequest requestWithURL:[[NSURL alloc] initWithString:mediaInfo.mediaURL]
                                                     cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                 timeoutInterval:10.0];
            
            __weak UITableView *weakTableView = tableView;
            __weak TweetMediaInfo *weakMediaInfo = mediaInfo;
            [NSURLConnection sendAsynchronousRequest:request
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
             {
                 if (error)
                 {
                     NSAssert(NO, error.localizedDescription);
                     return;
                 }
                 
                 NSDictionary *imageData = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                 if (error)
                 {
                     NSAssert(NO, error.localizedDescription);
                     return;
                 }
                 
                 if ([[imageData objectForKey:@"url"] length])
                 {
                     weakMediaInfo.mediaURL = [imageData objectForKey:@"url"];
                     weakMediaInfo.mediaType = @"";
                     
                     TweetViewCell *cellToUpdate = (TweetViewCell *)[weakTableView cellForRowAtIndexPath:indexPath];
                     if (cellToUpdate)
                     {
                         [cellToUpdate.tweetImage setImageWithURL:[[NSURL alloc] initWithString:[imageData objectForKey:@"url"]]
                                                 placeholderImage:[UIImage imageNamed:@"tweet_image_placeholder.png"]];
                         cellToUpdate.originalImageURL = [[NSURL alloc] initWithString:[imageData objectForKey:@"url"]];
                     }
                 }
             }];
        }
        else
        {
            if (mediaInfo.mediaURL.length)
            {
                [cell.tweetImage setImageWithURL:[[NSURL alloc] initWithString:mediaInfo.mediaURL]
                                placeholderImage:[UIImage imageNamed:@"tweet_image_placeholder.png"]];
                cell.originalImageURL = [[NSURL alloc] initWithString:mediaInfo.mediaURL];
            }
        }
    }
    
    CGFloat height = [JSCoreTextView measureFrameHeightForText:tweet.tweetText
                                                      fontName:CELL_TWEET_TEXT_FONT.fontName
                                                      fontSize:CELL_TWEET_TEXT_FONT.pointSize
                                            constrainedToWidth:303
                                                    paddingTop:0
                                                   paddingLeft:0];
    
    CGRect tweetTextFrame = cell.tweetText.frame;
    tweetTextFrame.size.height = height;
    cell.tweetText.frame = tweetTextFrame;
    
    cell.tweetText.text = tweet.tweetText;

    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TweetInfo *tweet = [self.mrUserTweets objectAtIndex:indexPath.row];
    
    return [tweet optimalCellSizeForTweet];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView == self.profileView.tweetsTable)
        return 55.0f;
    
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView == self.profileView.tweetsTable)
        return self.profileView.tweetsTitleView;
    
    return [[UIView alloc] init];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.backgroundColor = [UIColor clearColor];
}

#pragma mark - TweetViewCellDelegate methods

- (void)replyToTweet:(NSString *)tweetID tweetType:(TweetType)type
{
    NSPredicate *pred = [NSPredicate predicateWithFormat:[[NSString alloc] initWithFormat:@"tweetID like '%@'", tweetID]];
    NSArray *tweets = [self.mrUserTweets filteredArrayUsingPredicate:pred];
    NSAssert(tweets.count == 1, @"Somethings wrong with this tweet");
    
    TweetInfo *tweetInfo = [tweets lastObject];
    
    [self replyToTweet:tweetInfo];
}

- (void)retweet:(NSString *)tweetID tweetType:(TweetType)tweetType
{
    TweetInfo *tweet = [[TweetInfo alloc] init];
    tweet.tweetID = tweetID;
    
    NSUInteger index = [self.mrUserTweets indexOfObject:tweet];
    TweetInfo *tweetToUpdate = [self.mrUserTweets objectAtIndex:index];
    
    [self.profileView.tweetsTable beginUpdates];
    TweetViewCell *cellToUpdate = (TweetViewCell *)[self.profileView.tweetsTable cellForRowAtIndexPath:
                                                    [NSIndexPath indexPathForRow:index inSection:0]];
    
    tweetToUpdate.isRetweeted = YES;
    cellToUpdate.retweetInfoText.text = @"Retweeted by You";
    
    [self.profileView.tweetsTable endUpdates];
}

- (void)unretweet:(NSString *)tweetID tweetType:(TweetType)tweetType
{
    TweetInfo *tweet = [[TweetInfo alloc] init];
    tweet.tweetID = tweetID;
    
    NSUInteger index = [self.mrUserTweets indexOfObject:tweet];
    TweetInfo *tweetToUpdate = [self.mrUserTweets objectAtIndex:index];
    
    [self.profileView.tweetsTable beginUpdates];
    TweetViewCell *cellToUpdate = (TweetViewCell *)[self.profileView.tweetsTable cellForRowAtIndexPath:
                                                    [NSIndexPath indexPathForRow:index inSection:0]];

    tweetToUpdate.isRetweeted = NO;
    tweetToUpdate.retweetedTweetID = nil;
    
    cellToUpdate.retweetInfoText.text = tweetToUpdate.retwittedByUser.length ? [[NSString alloc] initWithFormat:@"Retweeted by %@", tweetToUpdate.retwittedByUser] : @"";
    
    [self.profileView.tweetsTable endUpdates];
}

- (void)retweetStateChangedForTweet:(TweetInfo *)tweetInfo
                          tweetType:(TweetType)type
                        isRetweeted:(BOOL)isRetweeted
                          completed:(void(^)(void))completedBlock
{
//    NSUInteger index = [self.mrUserTweets indexOfObject:tweetInfo];
//    
//    [self.profileView.tweetsTable beginUpdates];
//    TweetViewCell *cellToUpdate = (TweetViewCell *)[self.profileView.tweetsTable cellForRowAtIndexPath:
//                                                    [NSIndexPath indexPathForRow:index inSection:0]];
//    if (!isRetweeted)
//    {
//        tweetInfo.isRetweeted = YES;
//        cellToUpdate.retweetInfoText.text = @"Retweeted by You";
//    }
//    else
//    {
//        tweetInfo.isRetweeted = NO;
//        cellToUpdate.retweetInfoText.text = tweetInfo.retwittedByUser.length ? [[NSString alloc] initWithFormat:@"Retweeted by %@", tweetInfo.retwittedByUser] : @"";
//    }
//    
//    completedBlock();
//    [self.profileView.tweetsTable endUpdates];    
//    
    void (^localBlock)(void); // declaration
    
    __weak ProfileViewController *weakSelf = self;
    localBlock = ^(void) {

        NSUInteger index = [weakSelf.mrUserTweets indexOfObject:tweetInfo];
        
        [weakSelf.profileView.tweetsTable beginUpdates];
        TweetViewCell *cellToUpdate = (TweetViewCell *)[weakSelf.profileView.tweetsTable cellForRowAtIndexPath:
                                                        [NSIndexPath indexPathForRow:index inSection:0]];
        if (!isRetweeted)
        {
            tweetInfo.isRetweeted = YES;
            cellToUpdate.retweetInfoText.text = @"Retweeted by You";
        }
        else
        {
            tweetInfo.isRetweeted = NO;
            cellToUpdate.retweetInfoText.text = tweetInfo.retwittedByUser.length ? [[NSString alloc] initWithFormat:@"Retweeted by %@", tweetInfo.retwittedByUser] : @"";
        }

        [weakSelf.profileView.tweetsTable endUpdates];
    };
    
    [super retweetStateChangedForTweet:tweetInfo tweetType:type isRetweeted:isRetweeted completed:localBlock];
}

#pragma mark - ProfileViewDelegate methods

- (void)leftButtonPressed:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)rightButtonPressed
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    if (self.profileView.rightButton.tag == BUTTON_TAG_ACTION_FOLLOW)
    {
        [self.profileView.rightButton setImage:[UIImage imageNamed:@"following.png"] forState:UIControlStateNormal];
        self.profileView.rightButton.tag = BUTTON_TAG_ACTION_UNFOLLOW;
        
        NSURL *url = [[NSURL alloc] initWithString:@"https://api.twitter.com/1.1/friendships/create.json"];
        SLRequest *request  = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:TWRequestMethodPOST URL:url parameters:@{@"screen_name" : self.currentUserName}];
        [request setAccount:self.account];
        [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
         {
             if (responseData)
             {
                 //NSError *error;
                 //NSArray *response = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
                 
                 [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
             }
         }];
    }
    else if (self.profileView.rightButton.tag == BUTTON_TAG_ACTION_UNFOLLOW)
    {
        [self.profileView.rightButton setImage:[UIImage imageNamed:@"follow.png"] forState:UIControlStateNormal];
        self.profileView.rightButton.tag = BUTTON_TAG_ACTION_FOLLOW;
        
        NSURL *url = [[NSURL alloc] initWithString:@"https://api.twitter.com/1.1/friendships/destroy.json"];
        SLRequest *request  = [SLRequest requestForServiceType:SLServiceTypeTwitter requestMethod:TWRequestMethodPOST URL:url parameters:@{@"screen_name" : self.currentUserName}];
        [request setAccount:self.account];
        [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
         {
             if (responseData)
             {
                 //NSError *error;
                 //NSArray *response = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&error];
                 
                 [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
             }
         }];
    }
    else
    {
        NSAssert(NO, @"Unknown button tag received");
    }
}

- (void)showFollowersForUser:(NSString *)username
{
    FollowersViewController *followersController = [[FollowersViewController alloc] initWithNibName:@"FollowersViewController" bundle:nil];
    followersController.account = self.account;
    followersController.username = username;
    
    [self.navigationController pushViewController:followersController animated:YES];
}

- (void)showFollowedUsersForUser:(NSString *)username
{
    FollowersViewController *followersController = [[FollowersViewController alloc] initWithNibName:@"FollowersViewController" bundle:nil];
    followersController.account = self.account;
    followersController.username = username;
    followersController.showFollowees = YES;
    
    [self.navigationController pushViewController:followersController animated:YES];
}

#pragma mark - UIScrollview delegate methods

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    NSIndexPath *index = [self.profileView.tweetsTable indexPathForSelectedRow];
    if (index)
        [self.profileView.tweetsTable deselectRowAtIndexPath:index animated:YES];
    
    if (sender == self.profileView.tweetsTable)
    {
        if (sender.contentOffset.y < 0) {
            [sender setContentOffset:CGPointMake(sender.contentOffset.x, 0)];
        }
    }
}

- (UITableView *)tableViewForDatasource:(NSMutableArray *)datasource
{
    return self.profileView.tweetsTable;
}

- (NSMutableArray *)datasourceForTweetType:(TweetType)tweetType
{
    return self.mrUserTweets;
}

- (void)applyThemeAppearance
{
    if ([DataProxy sharedDataProxy].currentTheme == AppThemeTypeBlack)
    {
        self.view.backgroundColor = DARK_VIEWS_BACKGROUND_COLOR;
    }
    else
    {
        self.view.backgroundColor = LIGHT_VIEWS_BACKGROUND_COLOR;
    }
}

@end
