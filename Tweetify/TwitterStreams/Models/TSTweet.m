//
//  TSTweet.m
//  TwitterStreams
//
//  Created by Stuart Hall on 6/03/12.
//  Copyright (c) 2012 Stuart Hall. All rights reserved.
//

#import "TSTweet.h"
#import "NSArray+Enumerable.h"

@interface TSTweet()
@property (nonatomic, retain) TSUser* cachedUser;
@property (nonatomic, retain) NSArray* cachedUserMentions;
@property (nonatomic, retain) NSArray* cachedUrls;
@property (nonatomic, retain) NSArray* cachedHashtags;

@end

@implementation TSTweet

@synthesize cachedUser=_cachedUser;
@synthesize cachedUserMentions=_cachedUserMentions;
@synthesize cachedUrls=_cachedUrls;
@synthesize cachedHashtags=_cachedHashtags;
@synthesize deleteTwID;

- (void)dealloc {
    self.cachedUser = nil;
    self.cachedUserMentions = nil;
    self.cachedUrls = nil;
    self.cachedHashtags = nil;
    self.deleteTwID = nil;
    
    [super dealloc];
}

- (NSString*)text {
    return [self.dictionary objectForKey:@"text"];
}

- (NSString*)created_at {
    return [self.dictionary objectForKey:@"created_at"];
}

- (NSString*)tweetID{
    return [self.dictionary objectForKey:@"id_str"];
}

- (NSDictionary*)rtwStatus{
    return [self.dictionary objectForKey:@"retweeted_status"];
}

- (NSString *)rtwTweetID
{
    return [self.rtwStatus objectForKey:@"id_str"];
}

- (NSString *)repliedToID
{
    return self.rtwTweetID.length ? [self.rtwStatus objectForKey:@"in_reply_to_status_id_str"] : [self.dictionary objectForKey:@"in_reply_to_status_id_str"];
}

- (TSUser*)user {
    if (!self.cachedUser)
        self.cachedUser = [[[TSUser alloc] initWithDictionary:[self.dictionary objectForKey:@"user"]] autorelease];
    
    return self.cachedUser;
}

- (NSArray*)userMentions {
    if (!self.cachedUserMentions) {
        self.cachedUserMentions = [[self.dictionary valueForKeyPath:@"entities.user_mentions"] map:^id(NSDictionary* d) {
            return [[[TSUser alloc] initWithDictionary:d] autorelease];
        }];
    }
    
    return self.cachedUserMentions;
}

- (NSDictionary*)mappedURLs
{
    NSArray *urls;
    
    if ([self.dictionary objectForKey:@"retweeted_status"])
        urls = [[self.dictionary objectForKey:@"retweeted_status"] valueForKeyPath:@"entities.urls"];
    else
        urls = [self.dictionary valueForKeyPath:@"entities.urls"];
    

    if (!urls.count)
        return nil;
    
    __block NSMutableDictionary *mappedURLs = [[NSMutableDictionary alloc] init];
    [urls enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        [mappedURLs setObject:[obj objectForKey:@"expanded_url"]
                       forKey:[[obj objectForKey:@"display_url"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    }];
    
    return mappedURLs;
}

- (NSArray*)urls {
    if (!self.cachedUrls) {
        self.cachedUrls = [[self.dictionary valueForKeyPath:@"entities.urls"] map:^id(NSDictionary* d) {
            return [[[TSUrl alloc] initWithDictionary:d] autorelease];
        }];
    }
    
    return self.cachedUrls;
}

- (NSArray*)hashtags {
    if (!self.cachedHashtags) {
        self.cachedHashtags = [[self.dictionary valueForKeyPath:@"entities.hashtags"] map:^id(NSDictionary* d) {
            return [[[TSHashtag alloc] initWithDictionary:d] autorelease];
        }];
    }
    
    return self.cachedHashtags;
}

- (NSArray *)mediaInfo
{
    NSArray *media;
    
    if (self.rtwTweetID.length)
        media = [self.rtwStatus valueForKeyPath:@"entities.media"];
    else
        media = [self.dictionary valueForKeyPath:@"entities.media"];
    
    return media.count ? media : nil;
}

- (NSArray *)urlsInfo
{
    NSArray *urls;
    
    if (self.rtwTweetID.length)
        urls = [self.rtwStatus valueForKeyPath:@"entities.urls"];
    else
        urls = [self.dictionary valueForKeyPath:@"entities.urls"];
    
    return urls.count ? urls : nil;
}

- (BOOL)isFavorited
{
    return [[self.dictionary objectForKey:@"favorited"] boolValue];
}

- (BOOL)isRetweeted
{
    return [[self.rtwStatus objectForKey:@"retweeted"] boolValue];
}

@end
