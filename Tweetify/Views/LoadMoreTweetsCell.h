//
//  LoadMoreTweetsCell.h
//  Tweetify
//
//  Created by Yuriy Berdnikov on 6/12/13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadMoreTweetsCell : UITableViewCell

@property (nonatomic, copy) void (^onLoadMorePressed)(void);

@end
