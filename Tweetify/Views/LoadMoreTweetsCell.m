//
//  LoadMoreTweetsCell.m
//  Tweetify
//
//  Created by Yuriy Berdnikov on 6/12/13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import "LoadMoreTweetsCell.h"

@interface LoadMoreTweetsCell ()

@property (weak, nonatomic) IBOutlet UIButton *loadMoreButton;

@end

@implementation LoadMoreTweetsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)loadMoreTweetsButtonPressed:(UIButton *)sender
{
    if (self.onLoadMorePressed)
        self.onLoadMorePressed();
}

@end
