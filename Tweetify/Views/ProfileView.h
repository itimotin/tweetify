//
//  ProfileView.h
//  Tweetify
//
//  Created by Yuriy Berdnikov on 08.03.13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import "THLabel.h"
#import "DataProxy.h"

@protocol ProfileViewDelegate <NSObject>
- (void)showFollowersForUser:(NSString *)username;
- (void)showFollowedUsersForUser:(NSString *)username;
@optional
- (void)leftButtonPressed:(UIButton *)sender;
- (void)rightButtonPressed;
@end


@interface ProfileView : UIView

@property (nonatomic, weak) id<ProfileViewDelegate> delegate;

@property (nonatomic, weak) IBOutlet UIImageView *profileBackground;
@property (nonatomic, weak) IBOutlet UIImageView *userIcon;
@property (nonatomic, weak) IBOutlet THLabel *username;

@property (nonatomic, weak) IBOutlet THLabel *tweetsCount;
@property (nonatomic, weak) IBOutlet THLabel *tweetsTitle;
@property (nonatomic, weak) IBOutlet THLabel *followCount;
@property (nonatomic, weak) IBOutlet THLabel *followTitle;
@property (nonatomic, weak) IBOutlet THLabel *followersCount;
@property (nonatomic, weak) IBOutlet THLabel *followersTitle;

@property (nonatomic, weak) IBOutlet UIButton *rightButton;
@property (nonatomic, weak) IBOutlet UIButton *leftButton;
@property (nonatomic, weak) IBOutlet UITableView *tweetsTable;

@property (nonatomic, copy) NSString *userTwitterName;
@property (strong, nonatomic) IBOutlet UIButton *tweetsTitleButton;
@property (strong, nonatomic) IBOutlet UIView *tweetsTitleView;

//settings
@property (nonatomic, assign) BOOL showSettingsButton;
@property (nonatomic, copy) void(^onSettingsButtonPressed)(void);

@property (nonatomic, assign) AppThemeType theme;

@end
