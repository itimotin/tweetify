//
//  ProfileView.m
//  Tweetify
//
//  Created by Yuriy Berdnikov on 08.03.13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "ProfileView.h"
#import "ThemeAppearance.h"

// Цвет обводки
#define STROKE_COLOR [UIColor colorWithWhite:119.0f / 255.0f alpha:1.0f];
#define STROKE_WIDTH 0.0f

@interface ProfileView ()
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIButton *settingsButton;
@property (weak, nonatomic) IBOutlet UIImageView *userProfileStrokeImage;
@property (weak, nonatomic) IBOutlet UIImageView *userProfileBackgroundImage;
@property (weak, nonatomic) IBOutlet UIImageView *userStatisticsBackgroundImage;

- (IBAction)leftButtonPressed:(UIButton *)sender;
- (IBAction)rightButtonPressed:(UIButton *)sender;

@end

@implementation ProfileView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
    }
    return self;
}


- (void)awakeFromNib
{
    self.userIcon.layer.cornerRadius = 25.0f;
    self.userIcon.layer.masksToBounds = YES;
    
    self.username.strokeSize = STROKE_WIDTH;
    self.username.strokeColor = STROKE_COLOR;
    
    self.tweetsCount.strokeSize = STROKE_WIDTH;
    self.tweetsCount.strokeColor = STROKE_COLOR;
    
    self.tweetsTitle.strokeSize = STROKE_WIDTH;
    self.tweetsTitle.strokeColor = STROKE_COLOR;
    
    self.followCount.strokeSize = STROKE_WIDTH;
    self.followCount.strokeColor = STROKE_COLOR;
    
    self.followTitle.strokeSize = STROKE_WIDTH;
    self.followTitle.strokeColor = STROKE_COLOR;
    
    self.followersCount.strokeSize = STROKE_WIDTH;
    self.followersCount.strokeColor = STROKE_COLOR;
    
    self.followersTitle.strokeSize = STROKE_WIDTH;
    self.followersTitle.strokeColor = STROKE_COLOR;
    
    self.tweetsTable.tableHeaderView = self.headerView;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)setShowSettingsButton:(BOOL)showSettingsButton
{
    _showSettingsButton = showSettingsButton;
    
    self.settingsButton.hidden = !showSettingsButton;
}

- (IBAction)settingsButtonPressed:(UIButton *)sender
{
    if (self.onSettingsButtonPressed)
        self.onSettingsButtonPressed();
}

- (IBAction)scrollToTop:(UITapGestureRecognizer *)sender
{
    [self.tweetsTable scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (IBAction)leftButtonPressed:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(leftButtonPressed:)])
        [self.delegate leftButtonPressed:sender];
}

- (IBAction)rightButtonPressed:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(rightButtonPressed)])
        [self.delegate rightButtonPressed];
}

- (IBAction)followersButtonPressed:(UIButton *)sender
{
    [self.delegate showFollowersForUser:self.userTwitterName];
}

- (IBAction)showFollowedUsersButtonPressed:(UIButton *)sender
{
    [self.delegate showFollowedUsersForUser:self.userTwitterName];
}

- (IBAction)tweetsLabelPressed:(UIButton *)sender
{
    [self.tweetsTable setContentOffset:CGPointMake(0, 0) animated:YES];
}

- (void)setTheme:(AppThemeType)theme
{
    _theme = theme;
    
    if (theme == AppThemeTypeBlack)
    {
        self.tweetsTitleView.backgroundColor = DARK_VIEWS_BACKGROUND_COLOR;
        [self.tweetsTitleButton setTitleColor:DARK_VIEWS_TITLE_COLOR forState:UIControlStateNormal];
        self.userProfileStrokeImage.image = DARK_PROFILE_USER_ICON_STROKE_IMAGE;
        self.userProfileBackgroundImage.image = DARK_PROFILE_BACKGROUND_IMAGE;
        self.userStatisticsBackgroundImage.image = DARK_PROFILE_STATISTICS_BACKGROUND_IMAGE;
    }
    else
    {
        self.tweetsTitleView.backgroundColor = LIGHT_VIEWS_BACKGROUND_COLOR;
        [self.tweetsTitleButton setTitleColor:LIGHT_VIEWS_TITLE_COLOR forState:UIControlStateNormal];
        self.userProfileStrokeImage.image = LIGHT_PROFILE_USER_ICON_STROKE_IMAGE;
        self.userProfileBackgroundImage.image = LIGHT_PROFILE_BACKGROUND_IMAGE;
        self.userStatisticsBackgroundImage.image = LIGHT_PROFILE_STATISTICS_BACKGROUND_IMAGE;
    }
}

@end
