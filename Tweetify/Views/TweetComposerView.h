//
//  TweetComposerView.h
//  Tweetify
//
//  Created by Yuriy Berdnikov on 3/14/13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#define TWEET_MAX_LENGTH 140

@class TweetComposerView;

@protocol TweetComposerViewDelegate <NSObject>
- (void)postNewTweet:(TweetComposerView *)composerView;
- (void)deleteNewTweet:(TweetComposerView *)composerView;
- (void)addImageToTweet:(TweetComposerView *)composerView;
- (void)dismissComposer:(TweetComposerView *)composerView;
- (void)suggestedTextWasUsed:(NSString *)text;
@end

@interface TweetComposerView : UIView

@property (nonatomic, weak) id <TweetComposerViewDelegate> delegate;

@property (nonatomic, copy) NSString *inReplyToID;

@property (weak, nonatomic) IBOutlet UIView *tweetComposerView;
@property (weak, nonatomic) IBOutlet UITextView *tweetComposerTextView;

@property (weak, nonatomic) IBOutlet UIButton *postNewTweetButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteNewTweetButton;
@property (weak, nonatomic) IBOutlet UIButton *addImageToTweetButton;
@property (weak, nonatomic) IBOutlet UILabel *tweetLengthCounterLabel;
@property (nonatomic, assign) NSUInteger twitterImageURLLength;

@property (nonatomic, copy) NSArray *suggestions;

- (void)applyThemeAppearance;

@end
