//
//  TweetComposerView.m
//  Tweetify
//
//  Created by Yuriy Berdnikov on 3/14/13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import "TweetComposerView.h"
#import "DataProxy.h"
#import "ThemeAppearance.h"

@interface TweetComposerView ()

@property (weak, nonatomic) IBOutlet UIScrollView *suggestionsScrollView;

@end

@implementation TweetComposerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.tweetComposerTextView.font = [UIFont fontWithName:@"HelveticaNeue" size:17.0f];
    
    [self applyThemeAppearance];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (IBAction)postNewTweetButtonPressed:(UIButton *)sender
{
    [self.delegate postNewTweet:self];
}

- (IBAction)deleteNewTweetButtonPressed:(UIButton *)sender
{
    self.suggestions = nil;
    [self.delegate deleteNewTweet:self];
}

- (IBAction)addImageToTweetButtonPressed:(id)sender
{
    [self.delegate addImageToTweet:self];
}

- (IBAction)dismissNewTweetComposer:(UIButton *)sender
{
    [self.delegate dismissComposer:self];
}

#pragma mark - Suggestions related

- (void)setSuggestions:(NSArray *)suggestions
{
    _suggestions = suggestions;
    
    NSLog(@"suggestions - %@", suggestions);
    
    [self displaySuggestions];
}

- (void)displaySuggestions
{
    [self.suggestionsScrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    if (!self.suggestions.count)
        return;
    
    UIColor *buttonTextColor = ([DataProxy sharedDataProxy].currentTheme == AppThemeTypeBlack) ? DARK_TWEET_COMPOSER_SUGGESTION_TEXT_COLOR : LIGHT_TWEET_COMPOSER_SUGGESTION_TEXT_COLOR;
    
    __block NSUInteger maxWidth = 0;
    [self.suggestions enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        UIButton *button = [[UIButton alloc] init];
        button.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:17.0];
        
        //suggestion colors
        
        [button setTitleColor:buttonTextColor forState:UIControlStateNormal];
        [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        
        [button setTitle:[[NSString alloc] initWithFormat:@"@%@", obj] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(suggestionTapped:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = idx;
        [button sizeToFit];
        
        CGRect buttonFrame = button.frame;
        buttonFrame.origin.x = maxWidth + 10.0f;
        button.frame = buttonFrame;
        
        [self.suggestionsScrollView addSubview:button];
        maxWidth = CGRectGetMaxX(buttonFrame);
    }];
    
    self.suggestionsScrollView.contentSize = CGSizeMake(maxWidth + 10, CGRectGetHeight(self.suggestionsScrollView.bounds));
}

- (void)suggestionTapped:(UIButton *)sender
{
    NSString *suggestedText = [self.suggestions objectAtIndex:sender.tag];
    [self.delegate suggestedTextWasUsed:suggestedText];
}

- (void)applyThemeAppearance
{
    if ([DataProxy sharedDataProxy].currentTheme == AppThemeTypeBlack)
    {
        self.tweetComposerView.backgroundColor = DARK_TWEET_COMPOSER_BG_COLOR;
        self.tweetLengthCounterLabel.textColor = DARK_TWEET_COMPOSER_COUNTER_COLOR;
        self.tweetComposerTextView.backgroundColor = DARK_TWEET_COMPOSER_TWEET_FIELD_COLOR;
        self.tweetComposerTextView.textColor = DARK_TWEET_COMPOSER_TWEET_TEXT_COLOR;
        self.tweetComposerTextView.keyboardAppearance = UIKeyboardAppearanceDark;
        [self.deleteNewTweetButton setImage:DARK_TWEET_COMPOSER_DELETE_BUTTON_IMAGE forState:UIControlStateNormal];
        [self.addImageToTweetButton setImage:DARK_TWEET_COMPOSER_IMAGE_SELECTION_BUTTON_IMAGE forState:UIControlStateNormal];
        [self.postNewTweetButton setImage:DARK_TWEET_COMPOSER_POST_BUTTON_IMAGE forState:UIControlStateNormal];
    }
    else
    {
        self.tweetComposerView.backgroundColor = LIGHT_TWEET_COMPOSER_BG_COLOR;
        self.tweetLengthCounterLabel.textColor = LIGHT_TWEET_COMPOSER_COUNTER_COLOR;
        self.tweetComposerTextView.backgroundColor = LIGHT_TWEET_COMPOSER_TWEET_FIELD_COLOR;
        self.tweetComposerTextView.textColor = LIGHT_TWEET_COMPOSER_TWEET_TEXT_COLOR;
        self.tweetComposerTextView.keyboardAppearance = UIKeyboardAppearanceLight;
        [self.deleteNewTweetButton setImage:LIGHT_TWEET_COMPOSER_DELETE_BUTTON_IMAGE forState:UIControlStateNormal];
        [self.addImageToTweetButton setImage:LIGHT_TWEET_COMPOSER_IMAGE_SELECTION_BUTTON_IMAGE forState:UIControlStateNormal];
        [self.postNewTweetButton setImage:LIGHT_TWEET_COMPOSER_POST_BUTTON_IMAGE forState:UIControlStateNormal];
    }
}

@end
