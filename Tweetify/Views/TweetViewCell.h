//
//  TweetViewCell.h
//  Tweetify
//
//  Created by Yuriy Berdnikov on 3/4/13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import "JSTwitterCoreTextView.h"
#import "TweetInfo.h"
#import "DataProxy.h"

#define CELL_RETWEET_INFO_TEXT_FONT [UIFont fontWithName:@"HelveticaNeue" size:15.0f]
#define CELL_TWEET_TEXT_FONT [UIFont fontWithName:@"Helvetica-Light" size:15.0f]
#define CELL_TWEET_TIME_TEXT_FONT [UIFont fontWithName:@"HelveticaNeue" size:15.0f]

typedef enum {
    kTweetTypeTimeline,
    kTweetTypeMentions,
    kTweetTypeUsers,
} TweetType;

@protocol TweetViewCellDelegate <NSObject>
- (void)showProfileForUser:(NSString *)username;
- (void)openURL:(NSString *)link;
- (void)deleteTweet:(NSString *)tweetID;
- (void)replyToTweet:(NSString *)tweetID tweetType:(TweetType)type;
- (void)favoriteStateChangedForTweet:(TweetInfo *)tweetInfo tweetType:(TweetType)type isFavorited:(BOOL)isFavorited completed:(void(^)(void))completedBlock;
- (void)retweetStateChangedForTweet:(TweetInfo *)tweetInfo tweetType:(TweetType)type isRetweeted:(BOOL)isRetweeted completed:(void(^)(void))completedBlock;
- (void)quoteTweet:(TweetInfo *)tweetInfo;

- (void)showFullscreenImage:(NSURL *)imageURL;
- (void)showConversationForTweet:(NSString *)tweetID username:(NSString *)username tweetType:(TweetType)type;
@end

@interface TweetViewCell : UITableViewCell

@property(nonatomic, weak) id<TweetViewCellDelegate> delegate;

@property (nonatomic, assign) AppThemeType currentTheme;

@property(nonatomic, strong) TweetInfo *tweetInfo;

@property(nonatomic, assign) TweetType tweetType;
@property(nonatomic, assign) BOOL isFavorited;
@property(nonatomic, assign) BOOL isRetweeted;
@property(nonatomic, copy) NSString *repliedTweetID;

@property(nonatomic, assign) BOOL showDeleteButton;

@property(nonatomic, weak) IBOutlet UIView *tweetView;

@property(nonatomic, weak) IBOutlet UILabel *username;
@property(nonatomic, weak) IBOutlet UILabel *userTwitterName;
@property(nonatomic, weak) IBOutlet UIImageView *userIcon;

@property(nonatomic, weak) IBOutlet UILabel *retweetInfoText;
@property(nonatomic, weak) IBOutlet JSTwitterCoreTextView *tweetText;
@property(nonatomic, weak) IBOutlet UILabel *tweetTime;

//TODO: should bec some grid view - if more then 1 pic
@property(nonatomic, weak) IBOutlet UIImageView *tweetImage;
@property(nonatomic, strong) NSURL *originalImageURL;
@property(nonatomic, strong) NSDictionary *links;

@property(nonatomic, weak) IBOutlet UIButton *replyButton;
@property(nonatomic, weak) IBOutlet UIButton *retweetButton;
@property(nonatomic, weak) IBOutlet UIButton *starTweetButton;
@property(nonatomic, weak) IBOutlet UIButton *deleteButton;

@property(nonatomic, assign) BOOL silenceUpdate;
@property(nonatomic, assign) BOOL hideConversationIndicator;

- (IBAction)openProfilePage:(UIButton *)sender;
- (IBAction)deleteButtonPressed:(UIButton *)sender;
- (IBAction)replyButtonPressed:(UIButton *)sender;
- (IBAction)starTweetPressed:(UIButton *)sender;
- (IBAction)retweetPressed:(UIButton *)sender;

@end
