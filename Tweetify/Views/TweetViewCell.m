//
//  TweetViewCell.m
//  Tweetify
//
//  Created by Yuriy Berdnikov on 3/4/13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "TweetViewCell.h"
#import "ThemeAppearance.h"

#define CELL_USERNAME_TEXT_FONT [UIFont fontWithName:@"HelveticaNeue" size:22.0f]
#define CELL_CONTENT_SIDE_OFFSET 8.0f

@interface TweetViewCell () <JSCoreTextViewDelegate>
@property (nonatomic, assign) BOOL isExpanded;

@property (weak, nonatomic) IBOutlet UIImageView *conversationIndicator;
@property (weak, nonatomic) IBOutlet UIButton *conversationButton;

@end

@implementation TweetViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        // Initialization code
        self.isExpanded = NO;
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];

    self.userIcon.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.userIcon.layer.borderWidth = 1.0f;

    self.tweetText.delegate = self;
	[self.tweetText setFontName:CELL_TWEET_TEXT_FONT.fontName];
	[self.tweetText setFontSize:CELL_TWEET_TEXT_FONT.pointSize];
    self.tweetText.highlightedLinkColor = [UIColor darkGrayColor];
	[self.tweetText setHighlightColor:[UIColor clearColor]];
	[self.tweetText setBackgroundColor:[UIColor clearColor]];
       
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)];
    [self.tweetImage addGestureRecognizer:tapRecognizer];
    self.tweetImage.userInteractionEnabled = YES;
    
    UILongPressGestureRecognizer *longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(quoteTweetPressed:)];
    [self.retweetButton addGestureRecognizer:longPressRecognizer];
    
    UITapGestureRecognizer *doubleTapReconizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapDetected:)];
    doubleTapReconizer.numberOfTapsRequired = 2;
    [doubleTapReconizer setDelaysTouchesBegan:YES];
    [doubleTapReconizer setCancelsTouchesInView:YES];
    [self addGestureRecognizer:doubleTapReconizer];
    
    [self.starTweetButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self.retweetButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self.deleteButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self.replyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect viewForSlideFrame = self.tweetView.frame;
    viewForSlideFrame.size.height = CGRectGetMaxY(self.userIcon.frame);
    self.tweetView.frame = viewForSlideFrame;
    
    //adjust username size
    CGRect usernameFrame = self.username.frame;
    CGSize usernameSize = [self.username.text sizeWithFont:CELL_USERNAME_TEXT_FONT
                                         constrainedToSize:CGSizeMake(CGRectGetWidth(self.contentView.bounds) - CGRectGetMaxX(self.userIcon.frame), 23)];
//    CGrect userNameFrame = 
    
    usernameFrame.size.width = MIN(ceilf(usernameSize.width), CGRectGetWidth(self.contentView.frame) - usernameFrame.origin.x - 10.0f);
    self.username.frame = usernameFrame;
    
    //adjust user twitter name size
    CGRect userTwitterNameFrame = self.userTwitterName.frame;
    userTwitterNameFrame.origin.x = CGRectGetMaxX(self.username.frame) + CELL_CONTENT_SIDE_OFFSET;
    userTwitterNameFrame.size.width = CGRectGetWidth(self.frame) - userTwitterNameFrame.origin.x - 10.0f;
    userTwitterNameFrame.size.width = userTwitterNameFrame.size.width > 40.0f ? userTwitterNameFrame.size.width : 0;
    self.userTwitterName.frame = userTwitterNameFrame;
    
    CGRect tweetTextFrame = self.tweetText.frame;
    if (self.retweetInfoText.text.length)
    {
        //place tweet text to it's original position
        tweetTextFrame.origin = CGPointMake(tweetTextFrame.origin.x, CGRectGetMaxY(self.retweetInfoText.frame) + 2);
    }
    else
    {
        //place to Retweet info position
        tweetTextFrame.origin = CGPointMake(tweetTextFrame.origin.x, CGRectGetMinY(self.retweetInfoText.frame));
    }
    
    self.tweetText.frame = tweetTextFrame;
    
    //place image and time
    CGRect timeTextRect = self.tweetTime.frame;
    
    if (self.tweetImage.image)
    {
        CGRect tweetImageFrame = self.tweetImage.frame;
        tweetImageFrame.origin = CGPointMake(tweetImageFrame.origin.x, CGRectGetMaxY(self.tweetText.frame) + CELL_CONTENT_SIDE_OFFSET);
        self.tweetImage.frame = tweetImageFrame;

        timeTextRect.origin = CGPointMake(timeTextRect.origin.x, CGRectGetMaxY(self.tweetImage.frame) + CELL_CONTENT_SIDE_OFFSET);
    }
    else
    {
        timeTextRect.origin = CGPointMake(timeTextRect.origin.x, CGRectGetMaxY(self.tweetText.frame) + CELL_CONTENT_SIDE_OFFSET - 2);
    }
    
    self.tweetTime.frame = timeTextRect;
    
    if (![self.tweetInfo.repliedToTweetID isKindOfClass:[NSNull class]] && self.tweetInfo.repliedToTweetID.length && !self.hideConversationIndicator)
    {
        self.conversationIndicator.hidden = NO;
        self.conversationIndicator.center = CGPointMake(self.conversationIndicator.center.x, self.tweetTime.center.y);
        
        self.conversationButton.hidden = NO;
        self.conversationButton.center = CGPointMake(self.conversationIndicator.center.x, self.tweetTime.center.y);
    }
    else
    {
        self.conversationIndicator.hidden = YES;
        self.conversationIndicator.hidden = YES;
    }
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    [self restoreInitialState];
    
    self.tweetImage.image = nil;
    self.originalImageURL = nil;
    self.showDeleteButton = NO;
}

- (void)setIsFavorited:(BOOL)isFavorited
{
    _isFavorited = isFavorited;
    
    //Цвет кнопки Star
    
    if (isFavorited)
    {
        [self.starTweetButton setTitleColor:[UIColor colorWithRed:82 / 255.0f green:82 / 255.0f blue:82 / 255.0f alpha:1.0f] forState:UIControlStateNormal];
        [self.starTweetButton setTitle:NSLocalizedString(@"Starred", @"Starred button title") forState:UIControlStateNormal];
    }
    else
    {
        [self.starTweetButton setTitleColor:[UIColor colorWithRed:0 / 255.0f green:153 / 255.0f blue:203 / 255.0f alpha:1.0f] forState:UIControlStateNormal];
        [self.starTweetButton setTitle:NSLocalizedString(@"Star", @"Star button title") forState:UIControlStateNormal];
    }
}

- (void)setIsRetweeted:(BOOL)isRetweeted
{
    _isRetweeted = isRetweeted;
    
    //цвет кнопки Retweet
    
    if (isRetweeted)
    {
        if (self.currentTheme == AppThemeTypeBlack)
            [self.retweetButton setTitleColor:DARK_TWEET_RETWEETED_BY_SELECTED_COLOR forState:UIControlStateNormal];
        else
            [self.retweetButton setTitleColor:LIGHT_TWEET_RETWEETED_BY_SELECTED_COLOR forState:UIControlStateNormal];
        
        [self.retweetButton setTitle:NSLocalizedString(@"Retweeted", @"Retweeted button title") forState:UIControlStateNormal];

        if (CGRectGetMinX(self.retweetButton.frame) > 0)
        {
            CGRect starButtonFrame = self.starTweetButton.frame;
            starButtonFrame.origin = CGPointMake(CGRectGetMaxX(self.retweetButton.frame) + 19.0f, starButtonFrame.origin.y);
            self.starTweetButton.frame = starButtonFrame;
        }
    }
    else
    {
        if (self.currentTheme == AppThemeTypeBlack)
            [self.retweetButton setTitleColor:DARK_TWEET_RETWEETED_BY_NORMAL_COLOR forState:UIControlStateNormal];
        else
            [self.retweetButton setTitleColor:LIGHT_TWEET_RETWEETED_BY_NORMAL_COLOR forState:UIControlStateNormal];
        
        [self.retweetButton setTitle:NSLocalizedString(@"Retweet", @"Retweet button title") forState:UIControlStateNormal];
        
        if (CGRectGetMinX(self.retweetButton.frame) > 0)
        {
            CGRect starButtonFrame = self.starTweetButton.frame;
            starButtonFrame.origin = CGPointMake(CGRectGetMaxX(self.retweetButton.frame), starButtonFrame.origin.y);
            self.starTweetButton.frame = starButtonFrame;
        }
    }
}

- (void)setExpanded:(BOOL)expand
{
    if (expand)
    {
        [UIView animateWithDuration:0.15 animations:^{
            self.tweetView.center = CGPointMake(CGRectGetWidth(self.tweetView.frame) + CGRectGetWidth(self.tweetView.frame) / 2,
                                                self.tweetView.center.y);
        } completion:^(BOOL finished) {
            if (finished && self.selected)
            {
                if (self.showDeleteButton)
                {
                    [self presentDeleteButton];
                    return;
                }
                
                //show start button
                [UIView animateWithDuration:0.1 animations:^{
                    [self showButton:self.starTweetButton
                         withXOffset:CELL_CONTENT_SIDE_OFFSET +CGRectGetWidth(self.retweetButton.frame) +
                     CGRectGetWidth(self.replyButton.frame) + (self.isRetweeted ? 19.0f : 0.0f)];
                    
                } completion:^(BOOL finished) {
                    
                    if (finished && self.selected)
                    {
                        //show retweet button
                        [UIView animateWithDuration:0.125 animations:^{
                            [self showButton:self.retweetButton withXOffset:CGRectGetWidth(self.replyButton.frame) + CELL_CONTENT_SIDE_OFFSET];
                        } completion:^(BOOL finished) {
                            if (finished && self.selected)
                            {
                                //show reply button
                                [UIView animateWithDuration:0.13 animations:^{
                                    [self showButton:self.replyButton withXOffset:CELL_CONTENT_SIDE_OFFSET];
                                } completion:^(BOOL finished) {
                                    if (finished && self.selected)
                                        self.isExpanded = YES;
                                    else
                                        [self restoreInitialState];
                                }];
                            }
                            else
                                [self restoreInitialState];
                        }];
                    }
                    else
                        [self restoreInitialState];
                }];
            }
            else
                [self restoreInitialState];
        }];
    }
    else
    {
        if (self.showDeleteButton)
        {
            [self hideDeleteButtonWithCompletionHandler:^{
                //show tweet info view
                [UIView animateWithDuration:0.1 animations:^{
                    self.tweetView.center = CGPointMake(CGRectGetWidth(self.tweetView.frame) / 2,
                                                        self.tweetView.center.y);
                } completion:^(BOOL finished) {
                    if (finished)
                        self.isExpanded = NO;
                }];

            }];
            
            return;
        }
        
        //hide Reply
        [UIView animateWithDuration:0.1 animations:^{
            [self hideButton:self.replyButton];
        } completion:^(BOOL finished) {
            if (finished)
            {
                //hide retweet button
                [UIView animateWithDuration:0.15 animations:^{
                    [self hideButton:self.retweetButton];
                } completion:^(BOOL finished) {
                    if (finished)
                    {
                        //hide star button
                        [UIView animateWithDuration:0.125 animations:^{
                            [self hideButton:self.starTweetButton];
                        } completion:^(BOOL finished) {
                            if (finished)
                            {
                                //show tweet info view
                                [UIView animateWithDuration:0.1 animations:^{
                                    self.tweetView.center = CGPointMake(CGRectGetWidth(self.tweetView.frame) / 2,
                                                                        self.tweetView.center.y);
                                } completion:^(BOOL finished) {
                                    if (finished)
                                        self.isExpanded = NO;
                                    else
                                        [self restoreInitialState];
                                }];
                            }
                            else
                                [self restoreInitialState];
                        }];
                    }
                    else
                        [self restoreInitialState];
                }];
            }
            else
                [self restoreInitialState];
        }];
    }
}

- (void)forseSelection
{
    self.tweetView.center = CGPointMake(CGRectGetWidth(self.tweetView.frame) + CGRectGetWidth(self.tweetView.frame) / 2,
                                        self.tweetView.center.y);
    
    if (self.showDeleteButton)
    {
        [self showButton:self.deleteButton withXOffset:CELL_CONTENT_SIDE_OFFSET];
    }
    else
    {
        [self showButton:self.starTweetButton
             withXOffset:CELL_CONTENT_SIDE_OFFSET + CGRectGetWidth(self.retweetButton.frame) + CGRectGetWidth(self.replyButton.frame) + (self.isRetweeted ? 19.0f : 0.0f)];
        [self showButton:self.retweetButton withXOffset:CGRectGetWidth(self.replyButton.frame) + CELL_CONTENT_SIDE_OFFSET];
        [self showButton:self.replyButton withXOffset:CELL_CONTENT_SIDE_OFFSET];
    }
    
    self.isExpanded = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
    if (selected)
    {
        if (self.silenceUpdate)
        {
            [self forseSelection];
            self.silenceUpdate = NO;
            return;
        }
        
        if (!self.isExpanded)
            [self setExpanded:YES];
        else
            [self setExpanded:NO];
    }
    else
    {
        if (self.silenceUpdate)
        {
            [self restoreInitialState];
            self.silenceUpdate = NO;
            self.isExpanded = NO;
            return;
        }
        
        if (self.isExpanded)
        {
            [self setExpanded:NO];
        }
    }
}

- (IBAction)openProfilePage:(UIButton *)sender
{
    [self.delegate showProfileForUser:self.userTwitterName.text];
}

- (IBAction)deleteButtonPressed:(UIButton *)sender
{
    sender.enabled = NO;
    
    NSString *tweetID = self.tweetInfo.isRetweeted ? self.tweetInfo.retweetedTweetID : self.tweetInfo.originalTweetID;
    [self.delegate deleteTweet:tweetID];
}

- (IBAction)replyButtonPressed:(UIButton *)sender
{
    [self.delegate replyToTweet:self.tweetInfo.tweetID tweetType:self.tweetType];
}

- (IBAction)starTweetPressed:(UIButton *)sender
{
    __weak TweetViewCell *weakSelf = self;

    [self.delegate favoriteStateChangedForTweet:self.tweetInfo tweetType:self.tweetType isFavorited:self.isFavorited completed:^{
        weakSelf.isFavorited = !weakSelf.isFavorited;
    }];
}

- (IBAction)retweetPressed:(UIButton *)sender
{
    __weak TweetViewCell *weakSelf = self;

    [self.delegate retweetStateChangedForTweet:self.tweetInfo tweetType:self.tweetType isRetweeted:self.isRetweeted completed:^{
        weakSelf.isRetweeted = !weakSelf.isRetweeted;
        
        if (!weakSelf.isRetweeted)
            weakSelf.retweetInfoText.text = weakSelf.tweetInfo.retwittedByUser.length ? [[NSString alloc] initWithFormat:@"Retweeted by %@", weakSelf.tweetInfo.retwittedByUser] : @"";
    }];
}

#pragma mark - UITapGestureRecognizer selector

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isKindOfClass:[UIButton class]])
        return NO;
    
    return YES;
}

- (void)imageTapped:(UITapGestureRecognizer *)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateEnded)
    {
        if (!self.tweetImage.image || !self.originalImageURL)
            return;
        
        if ([self.delegate respondsToSelector:@selector(showFullscreenImage:)])
            [self.delegate showFullscreenImage:self.originalImageURL];
    }
}

#pragma mark - UITapGestureRecognizer selector

- (void)doubleTapDetected:(UITapGestureRecognizer *)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateEnded)
    {
        if ([self.repliedTweetID isKindOfClass:[NSNull class]] || !self.repliedTweetID.length)
            return;
        
        [self.delegate showConversationForTweet:self.tweetInfo.originalTweetID username:self.userTwitterName.text tweetType:self.tweetType];
    }
}

- (void)quoteTweetPressed:(UILongPressGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateRecognized)
        [self.delegate quoteTweet:self.tweetInfo];
}

#pragma mark - UIButton

- (IBAction)conversationButtonTapped:(UIButton *)sender
{
    if ([self.repliedTweetID isKindOfClass:[NSNull class]] || !self.repliedTweetID.length)
        return;
    
    [self.delegate showConversationForTweet:self.tweetInfo.originalTweetID username:self.userTwitterName.text tweetType:self.tweetType];
}

#pragma mark - Helpers

- (void)presentDeleteButton
{
    [UIView animateWithDuration:0.1 animations:^{
        [self showButton:self.deleteButton withXOffset:CELL_CONTENT_SIDE_OFFSET];
    } completion:^(BOOL finished) {
        self.isExpanded = YES;
    }];
}

- (void)hideDeleteButtonWithCompletionHandler:(void(^)(void))completedBlock
{
    [UIView animateWithDuration:0.125 animations:^{
        [self hideButton:self.deleteButton];
    } completion:^(BOOL finished) {
        if (finished)
        {
            completedBlock();
        }
    }];
}

- (void)restoreInitialState
{
    self.deleteButton.enabled = YES;
    [self hideButton:self.replyButton];
    [self hideButton:self.retweetButton];
    [self hideButton:self.starTweetButton];
    [self hideButton:self.deleteButton];
    self.tweetView.center = CGPointMake(CGRectGetWidth(self.tweetView.frame) / 2,
                                        self.tweetView.center.y);
    self.isExpanded = NO;
}

- (void)showButton:(UIButton *)button withXOffset:(CGFloat)xOffset
{
    CGRect frame = button.frame;
    frame.origin = CGPointMake(xOffset, frame.origin.y);
    button.frame = frame;
}

- (void)hideButton:(UIButton *)button
{
    button.center = CGPointMake(-CGRectGetWidth(button.frame), button.center.y);
}

#pragma mark - JSCoreTextViewDelegate methods

- (void)textView:(JSCoreTextView *)textView linkTapped:(AHMarkedHyperlink *)link
{
    if ([link.URL.absoluteString hasPrefix:@"@"])
        [self.delegate showProfileForUser:link.URL.absoluteString];
    else if ([link.URL.absoluteString hasPrefix:@"#"])
    {
        //do nothing for hashtag
    }
    else
    {
        NSString *fullURL = [self.links objectForKey:link.URL.absoluteString];
        [self.delegate openURL: fullURL.length ? fullURL : link.URL.absoluteString];
    }
}

#pragma mark - Theme related

- (void)setCurrentTheme:(AppThemeType)currentTheme
{
    if (currentTheme == AppThemeTypeBlack)
    {
        self.username.textColor = DARK_TWEET_USERNAME_COLOR;
        self.userTwitterName.textColor = DARK_TWEET_USET_TWITTER_NAME_COLOR;
        self.tweetTime.textColor = DARK_TWEET_DATE_COLOR;
        self.tweetText.textColor = DARK_TWEET_TEXT_COLOR;
        self.tweetText.linkColor = DARK_TWEET_TEXT_LINK_COLOR;
        
        [self.starTweetButton setTitleColor:DARK_TWEET_ACTION_BUTTON_COLOR forState:UIControlStateNormal];
        [self.retweetButton setTitleColor:DARK_TWEET_RETWEETED_BY_NORMAL_COLOR forState:UIControlStateNormal];
        [self.replyButton setTitleColor:DARK_TWEET_ACTION_BUTTON_COLOR forState:UIControlStateNormal];
        
        [self.deleteButton setTitleColor:DARK_TWEET_DELETE_BUTTON_COLOR forState:UIControlStateNormal];
    }
    else
    {
        self.username.textColor = LIGHT_TWEET_USERNAME_COLOR;
        self.userTwitterName.textColor = LIGHT_TWEET_USET_TWITTER_NAME_COLOR;
        self.tweetTime.textColor = LIGHT_TWEET_DATE_COLOR;
        self.tweetText.textColor = LIGHT_TWEET_TEXT_COLOR;
        self.tweetText.linkColor = LIGHT_TWEET_TEXT_LINK_COLOR;
        
        [self.starTweetButton setTitleColor:LIGHT_TWEET_ACTION_BUTTON_COLOR forState:UIControlStateNormal];
        [self.retweetButton setTitleColor:LIGHT_TWEET_RETWEETED_BY_NORMAL_COLOR forState:UIControlStateNormal];
        [self.replyButton setTitleColor:LIGHT_TWEET_ACTION_BUTTON_COLOR forState:UIControlStateNormal];
        
        [self.deleteButton setTitleColor:LIGHT_TWEET_DELETE_BUTTON_COLOR forState:UIControlStateNormal];
    }
}

@end
