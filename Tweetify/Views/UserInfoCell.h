//
//  UserInfoCell.h
//  Tweetify
//
//  Created by Yuriy Berdnikov on 17.04.13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserTwitterInfo.h"
#import "DataProxy.h"

@interface UserInfoCell : UITableViewCell

@property (nonatomic, strong) UserTwitterInfo *userInfo;

@property (nonatomic, assign) AppThemeType currentTheme;

@end
