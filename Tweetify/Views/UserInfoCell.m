//
//  UserInfoCell.m
//  Tweetify
//
//  Created by Yuriy Berdnikov on 17.04.13.
//  Copyright (c) 2013 smr. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "UIImageView+WebCache.h"
#import "UserInfoCell.h"
#import "GTMFadeTruncatingLabel.h"
#import "ThemeAppearance.h"

#define CELL_USERNAME_TEXT_FONT [UIFont fontWithName:@"HelveticaNeue" size:22.0f]
#define CELL_CONTENT_SIDE_OFFSET 8.0f

@interface UserInfoCell ()
@property (weak, nonatomic) IBOutlet UIImageView *userIcon;
@property (weak, nonatomic) IBOutlet GTMFadeTruncatingLabel *username;
@property (weak, nonatomic) IBOutlet GTMFadeTruncatingLabel *userTwitterName;
@end

@implementation UserInfoCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    self.userIcon.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.userIcon.layer.borderWidth = 1.0f;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    [self.userIcon cancelCurrentImageLoad];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    //adjust username size
    CGRect usernameFrame = self.username.frame;
    CGSize usernameSize = [self.username.text sizeWithFont:CELL_USERNAME_TEXT_FONT
                                         constrainedToSize:CGSizeMake(CGRectGetWidth(self.bounds) - CGRectGetMaxX(self.userIcon.frame), 23)];
    usernameFrame.size.width = ceil(MIN(usernameSize.width, CGRectGetWidth(self.frame) - usernameFrame.origin.x - 10.0f));
    self.username.frame = usernameFrame;
    
    //adjust user twitter name size
    CGRect userTwitterNameFrame = self.userTwitterName.frame;
    userTwitterNameFrame.origin.x = CGRectGetMaxX(self.username.frame) + CELL_CONTENT_SIDE_OFFSET;
    userTwitterNameFrame.size.width = CGRectGetWidth(self.frame) - userTwitterNameFrame.origin.x - 10.0f;
    userTwitterNameFrame.size.width = userTwitterNameFrame.size.width > 40.0f ? userTwitterNameFrame.size.width : 0;
    self.userTwitterName.frame = userTwitterNameFrame;
}

- (void)setUserInfo:(UserTwitterInfo *)userInfo
{
    _userInfo = userInfo;
    
    if (userInfo)
    {
        [self.userIcon setImageWithURL:userInfo.userAvaURL];
        self.username.text = userInfo.userName;
        self.userTwitterName.text = [[NSString alloc] initWithFormat:@"@%@", userInfo.userTwitterName];
    }
}

#pragma mark - Theme related

- (void)setCurrentTheme:(AppThemeType)currentTheme
{
    if (currentTheme == AppThemeTypeBlack)
    {
        self.username.textColor = DARK_TWEET_USERNAME_COLOR;
        self.userTwitterName.textColor = DARK_TWEET_USET_TWITTER_NAME_COLOR;
    }
    else
    {
        self.username.textColor = LIGHT_TWEET_USERNAME_COLOR;
        self.userTwitterName.textColor = LIGHT_TWEET_USET_TWITTER_NAME_COLOR;
    }
}

@end
